
angular.module('MyApp')
  .factory('User', function($http)
  {
    return {
      list: function()
      {
        return $http.get('/user/list');
      },
      listSingle: function(id)
      {
        return $http.get('/user/single/' + id);
      },
      add: function(data)
      {
        return $http.post('/user/add', data);
      },
      update: function(data)
      {
        return $http.post('/user/edit', data);
      },
      delete: function(data)
      {
        return $http.post('/user/delete', data);
      },
      ban: function(data)
      {
        return $http.post('/user/ban', data);
      },
      unban: function(data)
      {
        return $http.post('/user/unban', data);
      },
      makeAdmin: function(data)
      {
        return $http.post('/user/makeAdmin', data);
      },
      makeMember: function(data)
      {
        return $http.post('/user/makeMember', data);
      },
    };
  })
  .factory('Settings', function($http)
  {
    return {
      list: function()
      {
        return $http.get('/settings/list');
      },
      listSingle: function(id)
      {
        return $http.get('/settings/single/' + id);
      },
      update: function(data)
      {
        return $http.post('/settings/edit', data);
      },
    }
  })
  .factory('FAQ', function($http)
  {
    return {
      list: function()
      {
        return $http.get('/faq/list');
      },
      listSingle: function(id)
      {
        return $http.get('/faq/single/' + id);
      },
      add: function(data)
      {
        return $http.post('/faq/add', data);
      },
      update: function(data)
      {
        return $http.post('/faq/edit', data);
      },
      delete: function(data)
      {
        return $http.post('/faq/delete', data);
      }
    };
  })
  .factory('PLAN', function($http)
  {
    return {
      list: function()
      {
        return $http.get('/plan/list');
      },
      listSingle: function(id)
      {
        return $http.get('/plan/single/' + id);
      },
      add: function(data)
      {
        return $http.post('/plan/add', data);
      },
      update: function(data)
      {
        return $http.post('/plan/edit', data);
      },
      delete: function(data)
      {
        return $http.post('/plan/delete', data);
      }
    };
  })
  .factory('CMSPage', function($http)
  {
    return {
      list: function()
      {
        return $http.get('/cms_pages/list');
      },
      listSingle: function(id)
      {
        return $http.get('/cms_pages/single/' + id);
      },
      add: function(data)
      {
        return $http.post('/cms_pages/add', data);
      },
      update: function(data)
      {
        return $http.post('/cms_pages/edit', data);
      },
      delete: function(data)
      {
        return $http.post('/cms_pages/delete', data);
      }
    };
  })
  .factory('BlogPost', function($http)
  {
    return {
      list: function()
      {
        return $http.get('/api/posts/list');
      },
      listSingle: function(id)
      {
        return $http.get('/api/posts/single/' + id);
      },
      add: function(data)
      {
        return $http.post('/api/posts/add', data);
      },
      update: function(data)
      {
        return $http.post('/api/posts/edit', data);
      },
      delete: function(data)
      {
        return $http.post('/api/posts/delete', data);
      }
    };
  })
  .factory('Category', function($http)
  {
    return {
      list: function()
      {
        return $http.get('/api/category/list');
      },
      listSingle: function(id)
      {
        return $http.get('/api/category/single/' + id);
      },
      add: function(data)
      {
        return $http.post('/api/category/add', data);
      },
      update: function(data)
      {
        return $http.post('/api/category/edit', data);
      },
      delete: function(data)
      {
        return $http.post('/api/category/delete', data);
      }
    };
  })
  .factory('Invoice', function($http)
  {
    return {
      list: function(data)
      {
        if(data)
        {
          return $http.get('/invoices_of/'+data);
        }else{
          return $http.get('/invoices/all');
        }

      }
    };
  })
  .factory('Subject', function($http)
  {
    return {
      list: function()
      {
        return $http.get('/api/subject/list');
      },
      listSingle: function(id)
      {
        return $http.get('/api/subject/single/' + id);
      },
      add: function(data)
      {
        return $http.post('/api/subject/add', data);
      },
      update: function(data)
      {
        return $http.post('/api/subject/edit', data);
      },
      delete: function(data)
      {
        return $http.post('/api/subject/delete', data);
      }
    };
  })
  .factory('Topic', function ($http) {
    return {
      list: function () {
        return $http.get('/api/topic/list');
      },
      listSingle: function (id) {
        return $http.get('/api/topic/single/' + id);
      },
      add: function (data) {
        return $http.post('/api/topic/add', data);
      },
      update: function (data) {
        return $http.post('/api/topic/edit', data);
      },
      delete: function (data) {
        return $http.post('/api/topic/delete', data);
      }
    };
  })
  .factory('Course', function ($http) {
    return {
      list: function () {
        return $http.get('/api/courses/list');
      },
      listSingle: function (id) {
        return $http.get('/api/courses/single/' + id);
      },
      add: function (data) {
        return $http.post('/api/courses/add', data);
      },
      update: function (data) {
        return $http.post('/api/courses/edit', data);
      },
      delete: function (data) {
        return $http.post('/api/courses/delete', data);
      }
    };
  })
  .factory('Lesson', function ($http) {
    return {
      list: function (courseId) {
        return $http.get('/api/lessons/list/'+courseId);
      },
      listSingle: function (id) {
        return $http.get('/api/lessons/single/' + id);
      },
      add: function (data) {
        return $http.post('/api/lessons/add', data);
      },
      update: function (data) {
        return $http.post('/api/lessons/edit', data);
      },
      delete: function (data) {
        return $http.post('/api/lessons/delete', data);
      }
    };
  });
