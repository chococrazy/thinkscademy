angular.module('MyApp')
  .controller('ImageCtrl', function($scope, $location, $window, $auth, Image, SweetAlert)
  {
    $scope.images = [];
    $scope.show = false;

    $scope.list = function()
    {
      $scope.show = false;
      Image.list()
        .then(function(response)
        {
          // console.log(response)
          $scope.show = true;
          $scope.images = response.data.images;
          setTimeout(function()
          {
            $('#datatables')
              .DataTable(
              {
                "pagingType": "full_numbers",
                "order": [
                [0, 'desc']
              ],
                responsive: true,
              });
          }, 300);
        })
        .catch(function(response)
        {
          $scope.images = [];
          $scope.show = true;
          $scope.messages = {
            error: Array.isArray(response.data) ? response.data : [response.data]
          };
        });
    }
    $scope.list();

    $scope.requestToDelete = function(id)
    {
      SweetAlert.swal(
      {
        title: "Are you sure?",
        text: "Are you sure you want to delete this?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      }, function(e)
      {
        if (!e) return;

        Image.delete(
          {
            id: id
          })
          .then(function(response)
          {
            $scope.messages = {
              success: [response.data]
            };
            $scope.list();
          })
          .catch(function(response)
          {
            $scope.messages = {
              error: Array.isArray(response.data) ? response.data : [response.data]
            };
          });

      });
    }

  });

angular.module('MyApp')
  .controller('ImageAddCtrl', function($scope, $location, $http, $window, $auth, Image)
  {
    $scope.image = {
      title: '',
      description: ''
    }

        $scope.stateName = null;
        $scope.loading = false;


        $scope.loadTags = function(searchTerm) {
          return $http.get('/api/images/tags/filter?search=' + searchTerm);
        };

        $scope.autoCompleteOptions = {
          minimumChars: 1,
          data: function(searchTerm) {
            return $http.get('/api/images/categories/filter?search=' + searchTerm)
              .then(function(response) {

                $scope.loading = false;

                return response.data;
              });
          }
        };

    $scope.title = 'New Image';
    $scope.submitForm = function()
    {
      if ($scope.image.title == '' || $scope.image.description == '')
        return;
      Image.add($scope.image)
        .then(function(response)
        {
          $scope.messages = {
            success: [response.data]
          };
          $location.path('/images');
        })
        .catch(function(response)
        {
          $scope.messages = {
            error: Array.isArray(response.data) ? response.data : [response.data]
          };
        });
    }
  });

angular.module('MyApp')
  .controller('ImageEditCtrl', function($scope, $location, $http, $routeParams, $window, $auth, Image)
  {

            $scope.loadTags = function(searchTerm) {
              return $http.get('/api/images/tags/filter?search=' + searchTerm);
            };

            $scope.autoCompleteOptions = {
              minimumChars: 1,
              data: function(searchTerm) {
                return $http.get('/api/images/categories/filter?search=' + searchTerm)
                  .then(function(response) {

                    $scope.loading = false;

                    return response.data;
                  });
              }
            };
    $scope.image = {
      title: '',
      description: '',
      id: $routeParams.id
    }
    $scope.title = 'Edit Image';
    $scope.submitForm = function()
    {
      if ($scope.image.title == '' || $scope.image.description == '')
        return;
      Image.update($scope.image)
        .then(function(response)
        {
          $scope.messages = {
            success: [response.data]
          };
          $("html, body").animate({
            scrollTop: 0
          }, "slow");
        })
        .catch(function(response)
        {
          $scope.messages = {
            error: Array.isArray(response.data) ? response.data : [response.data]
          };
        });
    }

    $scope.fetchSingle = function()
    {
      Image.listSingle($routeParams.id)
        .then(function(response)
        {
          $scope.image = response.data.image;
          $scope.image.is_paid = $scope.image.is_paid ? "yes":"no";
           var t = $scope.image.tags.split(',');
          var m = [];
          for (var i = 0; i < t.length; i++) {
            m.push({
              text: t[i].replace('{', '').replace('}', '')
            });
          }
          $scope.image.tags = m;
        })
        .catch(function(response)
        {
          $scope.image = {
            title: '',
            description: '',
            id: $routeParams.id
          };
          $scope.messages = {
            error: Array.isArray(response.data) ? response.data : [response.data]
          };
        });
    }
    $scope.fetchSingle();
  });
