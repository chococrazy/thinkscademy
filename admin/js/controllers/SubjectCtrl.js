angular.module('MyApp')
  .controller('SubjectCtrl', function($scope, $location, $auth, Subject, SweetAlert) {
    $scope.show = false;

    $scope.items = [ ];
    $scope.ELEMENENT = 'Subject';

    $scope.list = function(){
      $scope.show = false;
      Subject.list()
      .then(function(response) {
        $scope.show = true;
          $scope.items = response.data.items;
          setTimeout(function(){
            $('#datatables').DataTable({
              "pagingType": "full_numbers","order": [[ 0, 'desc' ]],
              responsive: true,
            });
          }, 300);
      })
      .catch(function(response) {
        $scope.items = [ ];
        $scope.show = true;
        $scope.messages = {
          error: Array.isArray(response.data) ? response.data : [response.data]
        };
      });
    }
    $scope.list();

    $scope.requestToDelete = function(id)
    {
      SweetAlert.swal({
        title: "Are you sure?",
        text: "Are you sure you want to delete this?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },  function(e){
        if(!e) return;
        Subject.delete({id:id})
        .then(function(response) {
          if(typeof scrolltoTop !=='undefined') scrolltoTop();
          $scope.messages = {
            success: [response.data]
          };
          $scope.list();
        })
        .catch(function(response) {
          if(typeof scrolltoTop !=='undefined') scrolltoTop();
          $scope.messages = {
            error: Array.isArray(response.data) ? response.data : [response.data]
          };
        });
      });
    }
  });


angular.module('MyApp')
  .controller('SubjectAddCtrl', function($scope, $location, $auth, Subject) {
    $scope.item = { title: '', description: '', icon: 'fas fa-book-reader'};
    $scope.ELEMENENT = 'Subject';
    $scope.title = 'New Subject';
    $scope.submitForm = function(){
      if($scope.item.title == ''  )
        return;
      Subject.add($scope.item)
      .then(function(response) {
        if(typeof scrolltoTop !=='undefined') scrolltoTop();
        $scope.messages = {
          success: [response.data]
        };
        $location.path('/subjects');
      })
      .catch(function(response) {
        if(typeof scrolltoTop !=='undefined') scrolltoTop();
        $scope.messages = {
          error: Array.isArray(response.data) ? response.data : [response.data]
        };
      });
    }
  });




angular.module('MyApp')
  .controller('SubjectEditCtrl', function($scope, $location,$routeParams, $auth, Subject) {
    $scope.item = {title: '', id:$routeParams.id }
    $scope.title = 'Edit Subject';
    $scope.submitForm = function(){
      if($scope.item.title == ''  )
        return;
      Subject.update($scope.item)
      .then(function(response) {
        if(typeof scrolltoTop !=='undefined') scrolltoTop();
        $scope.messages = {
          success: [response.data]
        };
      })
      .catch(function(response) {
        if(typeof scrolltoTop !=='undefined') scrolltoTop();
        $scope.messages = {
          error: Array.isArray(response.data) ? response.data : [response.data]
        };
      });
    }
    $scope.fetchSingle = function(){
      Subject.listSingle($routeParams.id)
      .then(function(response) {
        if(typeof scrolltoTop !=='undefined') scrolltoTop();
          $scope.item = response.data.item;
      })
      .catch(function(response) {
        if(typeof scrolltoTop !=='undefined') scrolltoTop();
        $scope.item = { title: '', id: $routeParams.id,  icon: 'fas fa-book-reader', description: '' };
        $scope.messages = {
          error: Array.isArray(response.data) ? response.data : [response.data]
        };
      });
    }
    $scope.fetchSingle();
  });
