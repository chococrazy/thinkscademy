angular.module('MyApp')
  .controller('VideoCtrl', function($scope, $location, $window, $auth, Video, SweetAlert) {
    $scope.videos = [];
    $scope.show = false;

    $scope.list = function() {
      $scope.show = false;
      Video.list()
        .then(function(response) {
          $scope.show = true;
          $scope.videos = response.data.videos;
          setTimeout(function() {
            $('#datatables')
              .DataTable({
                "pagingType": "full_numbers",
                "order": [
                  [0, 'desc']
                ],
                responsive: true,
              });
          }, 300);
        })
        .catch(function(response) {
          $scope.videos = [];
          $scope.show = true;
          $scope.messages = {
            error: Array.isArray(response.data) ? response.data : [response.data]
          };
        });
    }
    $scope.list();

    $scope.requestToDelete = function(id) {
      SweetAlert.swal({
        title: "Are you sure?",
        text: "Are you sure you want to delete this?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      }, function(e) {
        if (!e) return;

        Video.delete({
            id: id
          })
          .then(function(response) {
            $scope.messages = {
              success: [response.data]
            };
            $scope.list();
          })
          .catch(function(response) {
            $scope.messages = {
              error: Array.isArray(response.data) ? response.data : [response.data]
            };
          });

      });
    }

  });

angular.module('MyApp')
  .controller('VideoAddCtrl', function($scope, $http, $location, $window, $auth, Video) {
    $scope.video = {
      title: '',
      description: ''
    }

    $scope.stateName = null;
    $scope.loading = false;


    $scope.loadTags = function(searchTerm) {
      return $http.get('/api/videos/tags/filter?search=' + searchTerm);
    };

    $scope.autoCompleteOptions = {
      minimumChars: 1,
      data: function(searchTerm) {
        return $http.get('/api/videos/categories/filter?search=' + searchTerm)
          .then(function(response) {

            $scope.loading = false;

            return response.data;
          });
      }
    };
    $scope.title = 'New Video';
    $scope.submitForm = function() {
      if ($scope.video.title == '' || $scope.video.description == '')
        return;
      Video.add($scope.video)
        .then(function(response) {
          $scope.messages = {
            success: [response.data]
          };
          $location.path('/videos');
        })
        .catch(function(response) {
          $scope.messages = {
            error: Array.isArray(response.data) ? response.data : [response.data]
          };
        });
    }
  });

angular.module('MyApp')
  .controller('VideoEditCtrl', function($scope, $http, $location, $routeParams, $window, $auth, Video) {
    $scope.video = {
      title: '',
      description: '',
      id: $routeParams.id
    }
    $scope.title = 'Edit Video';


    $scope.loadTags = function(searchTerm) {
      return $http.get('/api/videos/tags/filter?search=' + searchTerm);
    };

    $scope.autoCompleteOptions = {
      minimumChars: 1,
      data: function(searchTerm) {
        return $http.get('/api/videos/categories/filter?search=' + searchTerm)
          .then(function(response) {

            $scope.loading = false;

            return response.data;
          });
      }
    };


    $scope.submitForm = function() {
      if ($scope.video.title == '' || $scope.video.description == '')
        return;
      Video.update($scope.video)
        .then(function(response) {
          $scope.messages = {
            success: [response.data]
          };
          $("html, body").animate({
            scrollTop: 0
          }, "slow");
          // $location.path('/video');
        })
        .catch(function(response) {
          $scope.messages = {
            error: Array.isArray(response.data) ? response.data : [response.data]
          };
        });
    }

    $scope.fetchSingle = function() {
      Video.listSingle($routeParams.id)
        .then(function(response) {
          $scope.video = response.data.video;
          $scope.video.is_paid = $scope.video.is_paid ? "yes" : "no";
          var t = $scope.video.tags.split(',');
          var m = [];
          for (var i = 0; i < t.length; i++) {
            m.push({
              text: t[i].replace('{', '').replace('}', '')
            });
          }
          $scope.video.tags = m;
          // console.log($scope.video)
        })
        .catch(function(response) {
          $scope.video = {
            title: '',
            description: '',
            id: $routeParams.id
          };
          $scope.messages = {
            error: Array.isArray(response.data) ? response.data : [response.data]
          };
        });
    }
    $scope.fetchSingle();
  });
