angular.module('MyApp')
  .controller('CourseCtrl', function ($scope, $location, $auth, Course, SweetAlert) {
    $scope.show = false;
    $scope.items = [];
    $scope.ELEMENENT = 'Course';
    $scope.list = function () {
      $scope.show = false;
      Course.list()
        .then(function (response) {
          $scope.show = true;
          $scope.items = response.data.items;
          setTimeout(function () {
            $('#datatables')
              .DataTable({
                "pagingType": "full_numbers",
                "order": [[0, 'desc']],
                responsive: true,
              });
          }, 300);
        })
        .catch(function (response) {
          $scope.items = [];
          $scope.show = true;
          $scope.messages = {
            error: Array.isArray(response.data) ? response.data : [response.data]
          };
        });
    }
    $scope.list();
    $scope.requestToDelete = function (id) {
      SweetAlert.swal({
        title: "Are you sure?",
        text: "Are you sure you want to delete this?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      }, function (e) {
        if (!e) return;
        Course.delete({
            id: id
          })
          .then(function (response) {
            $scope.messages = {
              success: [response.data]
            };
            $scope.list();
          })
          .catch(function (response) {
            $scope.messages = {
              error: Array.isArray(response.data) ? response.data : [response.data]
            };
          });
      });
    }
  });
angular.module('MyApp')
  .controller('CourseAddCtrl', function ($scope, $location, $auth, $http, Course) {
    $scope.item = {
      title: '',
      description: ''
    };
    $scope.is_new = true;
    $scope.ELEMENENT = 'Course';
    $scope.title = 'New Course';
    $scope.loading = false;
    $scope.autoCompleteOptions = {
      minimumChars: 1,
      data: function (searchTerm) {
        return $http.get('/api/subject/filter?search=' + searchTerm)
          .then(function (response) {
            $scope.loading = false;
            return response.data;
          });
      }
    };
    $scope.autoCompleteOptionsTopic = {
      minimumChars: 1,
      data: function (searchTerm) {
        return $http.get('/api/topic/filter?search=' + searchTerm + '&subject=' + $scope.item.subject)
          .then(function (response) {
            $scope.loading = false;
            return response.data;
          });
      }
    };
    $scope.submitForm = function () {
      if ($scope.item.title == '')
        return;
      Course.add($scope.item)
        .then(function (response) {
          if(typeof scrolltoTop !=='undefined') scrolltoTop();
          $scope.messages = {
            success: [response.data]
          };
          $location.path('/courses');
        })
        .catch(function (response) {
          if(typeof scrolltoTop !=='undefined') scrolltoTop();
          $scope.messages = {
            error: Array.isArray(response.data) ? response.data : [response.data]
          };
        });
    }
  });
angular.module('MyApp')
  .controller('CourseEditCtrl', function ($scope, $location, $routeParams, $http, $auth, Course) {
    $scope.item = {
      title: '',
      id: $routeParams.id
    }
    $scope.is_new = false;
    $scope.title = 'Edit Course';
    $scope.loading = false;
    $scope.autoCompleteOptions = {
      minimumChars: 1,
      data: function (searchTerm) {
        return $http.get('/api/subject/filter?search=' + searchTerm)
          .then(function (response) {
            $scope.loading = false;
            return response.data;
          });
      }
    };
    $scope.autoCompleteOptionsTopic = {
      minimumChars: 1,
      data: function (searchTerm) {
        return $http.get('/api/topic/filter?search=' + searchTerm + '&subject=' + $scope.item.subject)
          .then(function (response) {
            $scope.loading = false;
            return response.data;
          });
      }
    };

    $scope.submitForm = function () {
      if ($scope.item.title == '')
        return;
      Course.update($scope.item)
        .then(function (response) {
          if(typeof scrolltoTop !=='undefined') scrolltoTop();
          $scope.messages = {
            success: [response.data]
          };
        })
        .catch(function (response) {
          if(typeof scrolltoTop !=='undefined') scrolltoTop();
          $scope.messages = {
            error: Array.isArray(response.data) ? response.data : [response.data]
          };
        });
    }
    $scope.fetchSingle = function () {
      Course.listSingle($routeParams.id)
        .then(function (response) {
          $scope.item = response.data.item;
        })
        .catch(function (response) {
          $scope.item = {
            title: '',
            id: $routeParams.id
          };
          $scope.messages = {
            error: Array.isArray(response.data) ? response.data : [response.data]
          };
        });
    }
    $scope.fetchSingle();
    $scope.removeImage = function () {
      $scope.item.image_url = '';
    }
    $scope.uploaded_images = [];
    $scope.file_upload_started = false;
    $scope.file_upload_failed = false;
    $scope.file_upload_completed = false;
    $scope.uploadedFile = function (element) {
      $scope.file_upload_started = true;
      $scope.file_upload_started = true;
      $scope.file_upload_failed = false;
      $scope.file_upload_completed = false;
      $scope.$apply(function ($scope) {
        $scope.files = element.files;
      });
      $scope.__uploadFile($scope.files);
    }
    $scope.__uploadFile = function (files) {
      var fd = new FormData();
      var url = '/fileUplaoder';
      angular.forEach(files, function (file) {
        fd.append('file', file);
      });
      var data = {
        id: 123
      };
      fd.append("data", JSON.stringify(data));
      $http.post(url, fd, {
          withCredentials: false,
          headers: {
            'Content-Type': undefined
          },
          transformRequest: angular.identity
        })
        .success(function (data) {
          $scope.file_upload_started = false;
          $scope.file_upload_failed = false;
          $scope.file_upload_completed = true;
          $scope.item.image_url = data.file;
          $scope.messages = {
            success: [data]
          };
        })
        .error(function (data) {
          $scope.file_upload_started = false;
          $scope.file_upload_failed = true;
          $scope.file_upload_completed = false;
          $scope.messages = {
            error: [data]
          };
        });
    };
  });
