angular.module('MyApp')
  .controller('TopicCtrl', function($scope, $location, $auth, Topic, SweetAlert) {
    $scope.show = false;

    $scope.items = [ ];
    $scope.ELEMENENT = 'Topic';

    $scope.list = function(){
      $scope.show = false;
      Topic.list()
      .then(function(response) {
        $scope.show = true;
          $scope.items = response.data.items;
          setTimeout(function(){
            $('#datatables').DataTable({
              "pagingType": "full_numbers","order": [[ 0, 'desc' ]],
              responsive: true,
            });
          }, 300);
      })
      .catch(function(response) {
        $scope.items = [ ];
        $scope.show = true;
        $scope.messages = {
          error: Array.isArray(response.data) ? response.data : [response.data]
        };
      });
    }
    $scope.list();

    $scope.requestToDelete = function(id)
    {
      SweetAlert.swal({
        title: "Are you sure?",
        text: "Are you sure you want to delete this?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },  function(e){
        if(!e) return;
        Topic.delete({id:id})
        .then(function(response) {
          $scope.messages = {
            success: [response.data]
          };
          $scope.list();
        })
        .catch(function(response) {
          $scope.messages = {
            error: Array.isArray(response.data) ? response.data : [response.data]
          };
        });
      });
    }
  });


angular.module('MyApp')
  .controller('TopicAddCtrl', function($scope, $location, $auth, $http, Topic) {
    $scope.item = {title: '', description : ''};
    $scope.ELEMENENT = 'Topic';
    $scope.title = 'New Topic';



    $scope.loading = false;
    $scope.autoCompleteOptions = {
      minimumChars: 1,
      itemSelected: function (e) {
        console.log('-----');
        console.log(e);
          // that.selectedColor = e.item;
      },
      data: function(searchTerm) {
        return $http.get('/api/subject/filter?give_id=yes&search=' + searchTerm)
          .then(function(response) {
            $scope.loading = false;
            return response.data;
          });
      }
    };

    $scope.submitForm = function(){
      if($scope.item.title == ''  )
        return;
      Topic.add($scope.item)
      .then(function(response) {
        if(typeof scrolltoTop !=='undefined') scrolltoTop();
        $scope.messages = {
          success: [response.data]
        };
        $location.path('/topics');
      })
      .catch(function(response) {
        if(typeof scrolltoTop !=='undefined') scrolltoTop();
        $scope.messages = {
          error: Array.isArray(response.data) ? response.data : [response.data]
        };
      });
    }
  });




angular.module('MyApp')
  .controller('TopicEditCtrl', function($scope, $location,$routeParams, $http, $auth, Topic) {
    $scope.item = {title: '', id:$routeParams.id }
    $scope.title = 'Edit Topic';


    $scope.loading = false;
    $scope.autoCompleteOptions = {
      minimumChars: 1,
      data: function(searchTerm) {
        return $http.get('/api/subject/filter?give_id=yes&search=' + searchTerm)
          .then(function(response) {
            $scope.loading = false;
            return response.data;
          });
      }
    };


    $scope.submitForm = function(){
      if($scope.item.title == ''  )
        return;
      Topic.update($scope.item)
      .then(function(response) {
        if(typeof scrolltoTop !=='undefined') scrolltoTop();
        $scope.messages = {
          success: [response.data]
        };
      })
      .catch(function(response) {
        if(typeof scrolltoTop !=='undefined') scrolltoTop();
        $scope.messages = {
          error: Array.isArray(response.data) ? response.data : [response.data]
        };
      });
    }
    $scope.fetchSingle = function(){
      Topic.listSingle($routeParams.id)
      .then(function(response) {
          $scope.item = response.data.item;
      })
      .catch(function(response) {
        $scope.item = {title: '', id:$routeParams.id };
        $scope.messages = {
          error: Array.isArray(response.data) ? response.data : [response.data]
        };
      });
    }
    $scope.fetchSingle();
  });
