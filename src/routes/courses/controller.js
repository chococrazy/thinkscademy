const Course = require('./Course');

exports.listMy = function(req, res, next) {
  let c = new Course();
  c = c.where('author_id', '=', req.user.id);
  c = c.orderBy('id', 'DESC');
  if (req.query.subject) {
    c = c.where('subject', 'LIKE', req.query.subject);
  }
  let p;
  if (req.query.paged && parseInt(req.query.paged) > 1) {
    p = parseInt(req.query.paged);
  } else {
    p = 1;
  }
  c.fetchPage({
    page: p,
    pageSize: 10,
    withRelated: [
      'author',
      {
        lessons(query) {
          query.select('title', 'description', 'course_id', 'id', 'order');
          query.orderBy('order', 'ASC'); // <--- works if previous line is commented
        }
      }
    ]
  })
    .then(function(items) {
      if (!items) {
        return res.status(200).send([]);
      }
      return res
        .status(200)
        .send({ok: true, items: items.toJSON(), pagination: items.pagination});
    })
    .catch(function(err) {
      console.log(err);
      return res.status(200).send([]);
    });
};

exports.list = function(req, res, next) {
  new Course()
    .orderBy('id', 'DESC')
    .fetchAll({
      withRelated: [
        'author',
        {
          lessons(query) {
            query.select('title', 'description', 'course_id', 'id', 'order');
            query.orderBy('order', 'ASC'); // <--- works if previous line is commented
          }
        }
      ]
    })
    .then(function(items) {
      if (!items) {
        return res.status(200).send([]);
      }
      return res.status(200).send({ok: true, items: items.toJSON()});
    })
    .catch(function(err) {
      console.log(err);
      return res.status(200).send([]);
    });
};

exports.listPaged = function(req, res, next) {
  let c = new Course();
  c = c.orderBy('id', 'DESC');
  if (req.query.subject) {
    c = c.where('subject', 'LIKE', req.query.subject);
  }
  let p;
  if (req.query.paged && parseInt(req.query.paged) > 1) {
    p = parseInt(req.query.paged);
  } else {
    p = 1;
  }
  c.fetchPage({
    page: p,
    pageSize: 10,
    withRelated: [
      'author',
      {
        lessons(query) {
          query.select('title', 'description', 'course_id', 'id', 'order');
          query.orderBy('order', 'ASC'); // <--- works if previous line is commented
        }
      }
    ]
  })
    .then(function(items) {
      if (!items) {
        return res.status(200).send([]);
      }
      return res
        .status(200)
        .send({ok: true, items: items.toJSON(), pagination: items.pagination});
    })
    .catch(function(err) {
      console.log(err);
      return res.status(200).send([]);
    });
};

exports.listPopular = function(req, res, next) {
  let c = new Course();
  c = c.orderBy('views', 'DESC');
  c.fetchPage({
    page: 1,
    pageSize: 6,
    withRelated: [
      'author',
      {
        lessons(query) {
          query.select('title', 'description', 'course_id', 'id', 'order');
          query.orderBy('order', 'ASC'); // <--- works if previous line is commented
        }
      }
    ]
  })
    .then(function(items) {
      if (!items) {
        return res.status(200).send([]);
      }
      return res
        .status(200)
        .send({ok: true, items: items.toJSON(), pagination: items.pagination});
    })
    .catch(function(err) {
      console.log(err);
      return res.status(200).send([]);
    });
};

exports.listSameAuthor = function(req, res, next) {
  let c = new Course();
  c = c.orderBy('views', 'DESC');
  c = c.where('author_id', '=', req.query.author_id);
  c.fetchPage({
    page: 1,
    pageSize: 6,
    withRelated: [
      'author',
      {
        lessons(query) {
          query.select('title', 'description', 'course_id', 'id', 'order');
          query.orderBy('order', 'ASC'); // <--- works if previous line is commented
        }
      }
    ]
  })
    .then(function(items) {
      if (!items) {
        return res.status(200).send([]);
      }
      return res
        .status(200)
        .send({ok: true, items: items.toJSON(), pagination: items.pagination});
    })
    .catch(function(err) {
      console.log(err);
      return res.status(200).send([]);
    });
};

exports.single = function(req, res, next) {
  new Course()
    .where('id', req.params.id)
    .fetch({
      withRelated: [
        'author',
        {
          lessons(query) {
            query.select('title', 'description', 'course_id', 'id', 'order');
            query.orderBy('order', 'ASC'); // <--- works if previous line is commented
          }
        }
      ]
    })
    .then(function(course) {
      if (!course) {
        return res
          .status(200)
          .send({id: req.params.id, title: '', description: ''});
      }
      if (
        req.query &&
        req.query.do_add_view &&
        req.query.do_add_view == 'yes'
      ) {
        course
          .save({views: course.get('views') + 1})
          .then(function() {})
          .catch(function(err) {});
      }
      course = course.toJSON();
      // course.learnings = course.learnings ? course.learnings.split(`\n`) : [];
      // course.requirements = course.requirements
      //   ? course.requirements.split(`\n`)
      //   : [];

      return res.status(200).send({ok: true, item: course});
    })
    .catch(function(err) {
      console.log(err);
      return res.status(400).send({
        id: req.params.id,
        title: '',
        description: '',
        msg: 'Failed to fetch from db'
      });
    });
};

exports.add = function(req, res, next) {
  req.assert('title', 'Title cannot be blank').notEmpty();
  req.assert('description', 'Description cannot be blank').notEmpty();
  //req.assert('slug'    , 'Fancy URL cannot be blank').notEmpty();
  const errors = req.validationErrors();
  if (errors) {
    return res.status(400).send(errors);
  }
  new Course({
    title: req.body.title,
    description: req.body.description,
    requirements: req.body.requirements,
    learnings: req.body.learnings,
    subject: req.body.subject,
    topic: req.body.topic,
    author_id: req.user ? req.user.id : 1
  })
    .save()
    .then(function(course) {
      res.send({ok: true, msg: 'New Course has been created successfully.'});
    })
    .catch(function(err) {
      console.log(err);
      return res
        .status(400)
        .send({msg: 'Something went wrong while created a new Course'});
    });
};

exports.update = function(req, res, next) {
  req.assert('title', 'Title cannot be blank').notEmpty();
  req.assert('description', 'Content cannot be blank').notEmpty();

  const errors = req.validationErrors();
  if (errors) {
    return res.status(400).send(errors);
  }

  const course = new Course({id: req.body.id});
  const obj = {
    title: req.body.title,
    description: req.body.description,
    requirements: req.body.requirements,
    learnings: req.body.learnings,
    subject: req.body.subject,
    topic: req.body.topic,
    author_id: req.user ? req.user.id : 1,
    image_url: req.body.image_url
  };

  if (req.body.remove_media) {
    obj.image_url = '';
  }
  course
    .save(obj)
    .then(function(blg) {
      blg
        .fetch()
        .then(function(bll) {
          res.send({course: bll.toJSON(), msg: 'Course has been updated.'});
        })
        .catch(function() {
          res
            .status(400)
            .send({msg: 'Something went wrong while updating the Course'});
        });
    })
    .catch(function() {
      res
        .status(400)
        .send({msg: 'Something went wrong while updating the Course'});
    });
};

exports.delete = function(req, res, next) {
  new Course({id: req.body.id})
    .destroy()
    .then(function(post) {
      res.send({msg: 'The Course Item has been successfully deleted.'});
    })
    .catch(function(err) {
      return res
        .status(400)
        .send({msg: 'Something went wrong while deleting the Course'});
    });
};
