const bookshelf = require('../../config/bookshelf');
const User = require('../../models/User');
const Lesson = require('../lessons/Lesson');

const Course = bookshelf.Model.extend({
  tableName: 'course',
  hasTimestamps: true,
  author: function() {
    return this.belongsTo(User, 'author_id');
  },
  lessons: function() {
    return this.hasMany(Lesson);
  },

  parse: function(response) {
    // console.log('....')
    if (response.image_url == '') {
      response.image_url = '/img/imgs/placeholder.png';
    }
    // if (response.learnings == '') {
    //   response.learnings = response.learnings.split(',');
    // }
    return response;
  }
  // format: function(response) {
  //   if (response.learnings == '') {
  //     response.learnings = response.learnings.split(`\n`);
  //   }
  //   if (response.requirements == '') {
  //     response.requirements = response.requirements.split(`\n`);
  //   }
  //   return response;
  // }
});

module.exports = Course;
