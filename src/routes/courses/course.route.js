const routes = require('express').Router();

const u_ctrl = require('../../controllers/user');
const ctrl = require('./controller.js');

const confirmIfAllowed = function(req, res, next) {
  if (req.user.role == 'teacher' || req.user.role == 'admin') {
    next();
  } else {
    res.status(400).send({ok: false, msg: 'Unauthorized'});
  }
};

module.exports = routes;
routes.get('/list', ctrl.list);
routes.get('/my', u_ctrl.ensureAuthenticated, confirmIfAllowed, ctrl.listMy);

routes.get('/listPaged', ctrl.listPaged);
routes.get('/listPopular', ctrl.listPopular);
routes.get('/listSameAuthor', ctrl.listSameAuthor);

routes.get('/single/:id', ctrl.single);

routes.post('/add', u_ctrl.ensureAuthenticated, confirmIfAllowed, ctrl.add);
routes.post('/edit', u_ctrl.ensureAuthenticated, confirmIfAllowed, ctrl.update);
routes.post(
  '/delete',
  u_ctrl.ensureAuthenticated,
  confirmIfAllowed,
  ctrl.delete
);
