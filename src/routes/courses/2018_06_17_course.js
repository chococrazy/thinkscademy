exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('course', function(table) {
      table.increments();
      table.string('title');
      table.string('subject');
      table.string('topic');
      table.string('image_url');
      table.text('learnings');
      table.text('requirements');
      table.text('description');
      table.integer('author_id').references('users.id');
      table.integer('views').defaultTo(0);
      table.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([knex.schema.dropTable('course')]);
};
