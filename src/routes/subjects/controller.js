var fs = require('fs');
var Subject = require('./Subject');

var txt = 'Subject';
exports.list = function(req, res, next) {
  new Subject()
  .orderBy('id', 'DESC')
  .fetchAll()
  .then(function(items) {
    if (!items) {
      return res.status(200).send([]);
    }
    return res.status(200).send({ok:true , items: items.toJSON()});
  })
  .catch(function(err) {
    return res.status(200).send([]);
  });
};

exports.filter = function(req, res, next) {
  let qb = Subject.query();
  var q = qb.distinct();
  q = q
    .whereRaw('LOWER(title) LIKE ?', '%' + req.query.search.toLowerCase() + '%');
  q.pluck('title').then(function(resp) {
    res.status(200).send(resp);
  }).
    catch(function (err) {
    console.log(err)
    res.status(200).send([]);
  })
};

exports.single = function(req, res, next)
{
  new Subject().where('id', req.params.id)
  .fetch()
  .then(function(item) {
    if (!item) {
      return res.status(200).send({id: req.params.id, title: ''});
    }
    return res.status(200).send({ok:true, item: item.toJSON()});
  })
  .catch(function(err) {
    return res.status(400).send({id : req.params.id, title: '', msg: 'Failed to fetch from db'});
  });
}


exports.add = function(req, res, next) {
  req.assert('title'   , 'Title cannot be blank').notEmpty();
  // req.assert('description' , 'Description cannot be blank').notEmpty();
  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).send(errors);
  }
  new Subject({
    title            : req.body.title,
    // description: req.body.description,
    icon: req.body.icon,
  }).save()
  .then(function(item) {
      res.send({ ok:true , msg: 'New '+txt+' has been created successfully.' });
  })
  .catch(function(err) {
    console.log(err)
       return res.status(400).send({ msg: 'Something went wrong while created a new '+txt+'' });
   });
};

exports.update = function(req, res, next) {
  req.assert('title'   , 'Title cannot be blank').notEmpty();
  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).send(errors);
  }

  var item = new Subject({ id: req.body.id });
  var obj = {
    title            : req.body.title,
    icon: req.body.icon,
  };
  item
  .save(obj)
  .then(function(blg){
    blg
    .fetch()
    .then(function(bll){
      res.send({ item : bll.toJSON(), msg: ''+txt+' has been updated.' });
    })
    .catch(function(err){
      res.status(400).send({ msg : 'Something went wrong while updating the '+txt+' : '+err.message });
    })
  })
  .catch(function(err1){
    res.status(400).send({ msg : 'Something went wrong while updating the '+txt+' : '+err1.message });
  })
};

exports.delete = function(req, res, next) {
  new Subject({ id: req.body.id }).destroy().then(function(post) {
    res.send({ msg: 'The '+txt+' has been successfully deleted.' });
  }).catch(function(err) {
    return res.status(400).send({ msg : 'Something went wrong while deleting the '+txt+'' });
  });
};
