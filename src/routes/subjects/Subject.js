var bookshelf = require('../../config/bookshelf');

var Subject = bookshelf.Model.extend({
  tableName: 'subject',
  hasTimestamps: false,
	//  category: function() {
  //   return this.belongsTo(Category);
  // }
});

module.exports = Subject;
