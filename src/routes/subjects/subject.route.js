const routes = require('express').Router();

var u_ctrl = require('../../controllers/user');
var ctrl = require('./controller.js');



module.exports = routes;

routes.get('/list'       , ctrl.list );
routes.get('/filter'       , ctrl.filter );
routes.get('/single/:id' , ctrl.single );
routes.post('/add'       , u_ctrl.ensureAuthenticated , ctrl.add );
routes.post('/edit'      , u_ctrl.ensureAuthenticated , ctrl.update );
routes.post('/delete'    , u_ctrl.ensureAuthenticated , ctrl.delete );
// Subject
