const Topic = require('./Topic');

const txt = 'Topic';
exports.list = function(req, res, next) {
  new Topic()
    .orderBy('id', 'DESC')
    .fetchAll()
    .then(function(items) {
      if (!items) {
        return res.status(200).send([]);
      }
      return res.status(200).send({ok: true, items: items.toJSON()});
    })
    .catch(function(err) {
      return res.status(200).send([]);
    });
};

exports.filter = function(req, res, next) {
  const qb = Topic.query();
  let q = qb.distinct();
  q = q.whereRaw(
    'LOWER(title) LIKE ?',
    '%' + req.query.search.toLowerCase() + '%'
  );
  q = q.whereRaw('subject LIKE ?', '%' + req.query.subject + '%');
  q.pluck('title')
    .then(function(resp) {
      res.status(200).send(resp);
    })
    .catch(function(err) {
      console.log(err);
      res.status(200).send([]);
    });
};

exports.single = function(req, res, next) {
  new Topic()
    .where('id', req.params.id)
    .fetch()
    .then(function(item) {
      if (!item) {
        return res.status(200).send({id: req.params.id, title: ''});
      }
      return res.status(200).send({ok: true, item: item.toJSON()});
    })
    .catch(function(err) {
      return res
        .status(400)
        .send({id: req.params.id, title: '', msg: 'Failed to fetch from db'});
    });
};

exports.add = function(req, res, next) {
  req.assert('title', 'Title cannot be blank').notEmpty();
  req.assert('subject', 'Subject cannot be blank').notEmpty();
  const errors = req.validationErrors();
  if (errors) {
    return res.status(400).send(errors);
  }
  new Topic({
    title: req.body.title,
    // description: req.body.description,
    subject: req.body.subject
  })
    .save()
    .then(function(item) {
      res.send({
        ok: true,
        msg: 'New ' + txt + ' has been created successfully.'
      });
    })
    .catch(function(err) {
      console.log(err);
      return res
        .status(400)
        .send({msg: 'Something went wrong while created a new ' + txt + ''});
    });
};

exports.update = function(req, res, next) {
  req.assert('title', 'Title cannot be blank').notEmpty();
  req.assert('subject', 'Subject cannot be blank').notEmpty();
  const errors = req.validationErrors();
  if (errors) {
    return res.status(400).send(errors);
  }

  const item = new Topic({id: req.body.id});
  const obj = {
    title: req.body.title,
    // description: req.body.description,
    subject: req.body.subject
  };
  item
    .save(obj)
    .then(function(blg) {
      blg
        .fetch()
        .then(function(bll) {
          res.send({item: bll.toJSON(), msg: '' + txt + ' has been updated.'});
        })
        .catch(function(err) {
          console.log(err);
          res
            .status(400)
            .send({msg: 'Something went wrong while updating the ' + txt + ''});
        });
    })
    .catch(function(err) {
      console.log(err);
      res
        .status(400)
        .send({msg: 'Something went wrong while updating the ' + txt + ''});
    });
};

exports.delete = function(req, res, next) {
  new Topic({id: req.body.id})
    .destroy()
    .then(function(post) {
      res.send({msg: 'The ' + txt + ' has been successfully deleted.'});
    })
    .catch(function(err) {
      return res
        .status(400)
        .send({msg: 'Something went wrong while deleting the ' + txt + ''});
    });
};
