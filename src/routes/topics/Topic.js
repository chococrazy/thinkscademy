const bookshelf = require('../../config/bookshelf');

const Topic = bookshelf.Model.extend({
  tableName: 'topic',
  hasTimestamps: false
  //  category: function() {
  //   return this.belongsTo(Category);
  // }
});

module.exports = Topic;
