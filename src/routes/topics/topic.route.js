const routes = require('express').Router();

const u_ctrl = require('../../controllers/user');
const ctrl = require('./controller.js');

module.exports = routes;

routes.get('/list', ctrl.list);
routes.get('/single/:id', ctrl.single);
routes.get('/filter', ctrl.filter);
routes.post('/add', u_ctrl.ensureAuthenticated, ctrl.add);
routes.post('/edit', u_ctrl.ensureAuthenticated, ctrl.update);
routes.post('/delete', u_ctrl.ensureAuthenticated, ctrl.delete);
// Subject
