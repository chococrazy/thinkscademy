exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('topic', function(table) {
      table.increments();
      table.string('title');
      table.text('description');
      table.string('subject');
      table.string('icon');
      table.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('topic')
  ])
};
