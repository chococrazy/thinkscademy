var Video = require('../models/Video');
var Image = require('../models/Image');


exports.home = function(req, res, next)
{
  new Video()
  .where({'is_paid' : false})
  .fetchPage({
    pageSize: 4,
    page: 1,
  })
  .then(function(free_vids){
    if(free_vids)
      free_vids = free_vids.toJSON();
    else{
      free_vids = [];
    }
    new Image()
    .where({'is_paid' : false})
    .fetchPage({
      pageSize: 4,
      page: 1,
    })
    .then(function(free_albums){
      if(free_albums)
        free_albums = free_albums.toJSON();
      else{
        free_albums = [];
      }
      if(1)
      {
        new Image()
        .where({'is_paid' : true})
        .fetchPage({
          pageSize: 4,
          page: 1,
        })
        .then(function(paid_albums)
        {
          if(paid_albums)
            paid_albums = paid_albums.toJSON();
          else{
            paid_albums = [];
          }
          new Video()
          .where({'is_paid' : true})
          .fetchPage({
            pageSize: 4,
            page: 1,
          })
          .then(function(paid_video){
            if(paid_video)
              paid_video = paid_video.toJSON();
            else{
              paid_video = [];
            }
            res.status(200).send({
              ok          : true,
              free_images : free_albums,
              free_videos : free_vids,
              paid_images : paid_albums,
              paid_videos : paid_video
            });
          })
          .catch(function(){
            res.status(200).send({
              ok          : true,
              free_images : free_albums,
              free_videos : free_vids,
              paid_images : paid_albums,
              paid_videos : []
            });
          })
        })
        .catch(function(){
          res.status(200).send({
            ok          : true,
            free_images : free_albums,
            free_videos : free_vids,
            paid_images : [],
            paid_videos : []
          });
        })
      }else{
        res.status(200).send({
          ok          : true,
          free_images : free_albums,
          free_videos : free_vids,
          paid_images : [],
          paid_videos : []
        });
      }

    })
    .catch(function(){
      res.status(200).send({
        ok          : true,
        free_images : [],
        free_videos : free_vids,
        paid_images : [],
        paid_videos : []
      })
    })
  })
  .catch(function(err){
    res.status(200).send({
      ok          : true,
      free_images : [],
      free_videos : [],
      paid_images : [],
      paid_videos : []
    });
  })

}
