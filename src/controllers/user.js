var fs         = require('fs');
var async      = require('async');
var crypto     = require('crypto');
var jwt        = require('jsonwebtoken');
var moment     = require('moment');
var request    = require('request');
var qs         = require('querystring');
var User       = require('../models/User');
var Plan = require('../models/Plan');
var Progress = require('../models/Progress');
var Quiz = require('../models/Quiz');
var Lesson = require('../routes/lessons/Lesson');
var mailer     = require('./mailer');

function generateToken(user) {
  var payload = {
    iss: 'thinkscademy.com',
    sub: user.id,
    iat: moment().unix(),
    exp: moment().add(7, 'days').unix()
  };
  return jwt.sign(payload, process.env.TOKEN_SECRET);
}


function sendMailToActivate(req, user){
  var site_url = process.env.SITE_URL || 'http://thinkscademy.com';
  var link = site_url + '/activate/' + user.id + '/'+moment(user.created_at).unix();

  mailer.doMail(user.email, false, false, 'Verify your email', "Hi<br /><br />Please click on the link below to verify your email and activate your Thinkscademy account:  <br /><br />"+link+"<br /><br />Regards")

}



exports.doProgress = function (req, res, next) {

  let body = req.body;
  let answers = body.answers;
  let wrong = 0;
  for(let i = 0; i < answers.length; i++)
  {
    if (answers[i]['r'] == 'Incorrect' || !answers[i]['r'])
    {
      wrong++;
    }
  }
  if(wrong >  0)
  {

    new Lesson()
      .where('id', req.body.lesson_id)
      .fetch({withRelated: ['course_details']})
      .then(function(lesson) {



      if(!lesson)
      {
        res.status(400).send({ ok: false, msg: 'Unable to get lesson to update progress 56' });
        return;
      }
        res.status(200).send({ ok: true });

      lesson.save({
        start_count : lesson.get('start_count') +1
      }).then(function(){

      }).catch(function(err){
        console.log(err);
      });

    })
    .catch(function(err){
      console.log(err);
    })
  } else
  {
    new Lesson()
      .where('id', req.body.lesson_id)
      .fetch({withRelated: ['course_details']})
      .then(function(lesson) {

      if(!lesson)
      {
        res.status(400).send({ ok: false, msg: 'Unable to get lesson to update progress 33' });
        //res.status(200).send({ ok: true, msg: 'Tentatively Done' });
        return;
      }
      lesson.save({
        start_count : lesson.get('start_count') +1,
        complete_count : lesson.get('complete_count') +1
      }).then(function(){}).catch(function(err){
        console.log(err);
      });
      lesson = lesson.toJSON();
      // console.log(lesson);
      let order = lesson.order;
      new Progress()
      .where({'user_id' : req.user.id})
      .fetch()
      .then(function (progress) {
        if(!progress)
        {
          new Progress({
            user_id: req.user.id, course_progress: {
              [lesson.course_details.id]  : order

            }
          })
          .save()
            .then(function (prog) {
              res.status(200).send({ ok: true, msg: 'Progress Updated', progress: prog.toJSON() });
            }).catch(function (err) {
              console.log('4', err);
              res.status(400).send({ ok: false, msg: 'Unable to get lesson to update progress 4' });
            });
        } else {
          var course_details = progress.get('course_progress');
          if (!course_details[lesson.course_details.id]) {
            course_details[lesson.course_details.id] = order;
          } else if (course_details[lesson.course_details.id] < order)
          {
            course_details[lesson.course_details.id] = order;
          } else {
            // course_details[lesson.course_details.id] = order;
          }
          progress.save({
            course_progress: course_details
          }).then(function (prog) {
            res.status(200).send({ ok: true, msg: 'Progress Updated', progress: prog.toJSON() });
          }).catch(function (err) {
            console.log('3', err);
            res.status(400).send({ ok: false, msg: 'Unable to get lesson to update progress 6' });
          });
        }

      }).catch(function (err) {
        console.log('2', err);
        res.status(400).send({ ok: false, msg: 'Unable to get lesson to update progress 2' });
      })
    })
    .catch(function (err) {
      console.log('1', err);
      res.status(400).send({ ok: false, msg: 'Unable to get lesson to update progress 1' });
      // res.status(400).send({ ok: false, msg: 'Unable to get lesson to update progress' });
    })
  }

  new Quiz({
    lesson_id : body.lesson_id,
    correct_answers : answers.length - wrong,
    results : JSON.stringify(answers),
    user_id : req.user.id
  }).save().then(function(q){

  }).catch(function(err){
    console.log(err);
  })
}

exports.ensureAuthenticated = function(req, res, next) {
  if (req.isAuthenticated()) {
    next();
  } else {
    res.status(401).send({ msg: 'Unauthorized' });
  }
};
  /**
   * POST /login
   * Sign in with email and password
   */

exports.loginPost = function(req, res, next) {
    req.assert('email', 'Email is not valid').isEmail();
    req.assert('email', 'Email cannot be blank').notEmpty();
    req.assert('password', 'Password cannot be blank').notEmpty();
    // req.sanitize('email').normalizeEmail({ remove_dots: false });

    var errors = req.validationErrors();

    if (errors) {
      return res.status(400).send(errors);
    }

    new User({ email: req.body.email })
      .fetch({withRelated: ['progress']})
      .then(function(user) {
        if (!user) {
          return res.status(401).send({ msg: 'The email address ' + req.body.email + ' is not associated with any account. ' +
          'Double-check your email address and try again.'
          });
        }
        if(user.get('email_verified') == false)
        {
           res.status(401).send({ msg: 'You need to verify your email by clicking on the link we sent you. We have re-sent you the verification mail.'});

          sendMailToActivate(req, user.toJSON());
          return;
        }
        if (user.get('status') == false) {
          return res.status(401).send({ msg: 'Your Account is currently not active.' });
        }
        user.comparePassword(req.body.password, function(err, isMatch) {
          if (!isMatch) {
            return res.status(401).send({ msg: 'Invalid email or password' });
          }

          if(req.body.should_be)
          {
            if(user.get('role') == req.body.should_be)
            {
              res.send({ token: generateToken(user), user: user.toJSON() });
            }else{
              return res.status(401).send({ msg: 'You dont have permissions to perform this action.' });
            }
          } else {
            user = user.toJSON();
            let progress = user.progress;
            user.progress = null;
            res.send({ token: generateToken(user), user: user, progress:progress });
          }

        });
      }).catch(function(err){
        console.log(err)

      });
  };




exports.resend = function(req, res, next)
{
  var uid = req.query.uid;
  // console.log(uid);
  new User({ id: uid })
  .fetch()
  .then(function(user) {
    if(!user)
    {

      res.status(400).send({ok:false});
    }
    sendMailToActivate(req, user.toJSON())
    res.status(200).send({ok:true});
    return;
  });

}


exports.signupPost = function(req, res, next) {
  req.assert('first_name', 'First Name cannot be blank').notEmpty();
  req.assert('last_name', 'Last Name cannot be blank').notEmpty();
  req.assert('email', 'Email is not valid').isEmail();
  req.assert('email', 'Email cannot be blank').notEmpty();

  req.assert('password', 'Password must be at least 4 characters long').len(4);
  req.assert('password_confirm', 'Confirm Password must be at least 4 characters long').len(4);
  // req.sanitize('email').normalizeEmail({ remove_dots: false });

  var errors = req.validationErrors();

  if (errors) {
    return res.status(400).send(errors);
  }
  if (req.body.password != req.body.password_confirm)
  {
    res.status(400).send({ msg: 'Password and Confirm password do not match' });
    return;
  }

  new User({
    first_name : req.body.first_name,
    last_name  : req.body.last_name,
    email      : req.body.email,
    password   : req.body.password,
    role: req.body.signup_as == 'teacher' ? 'teacher' : 'student',
    status: req.body.signup_as == 'teacher' ? false  : true,
    plan_description : ( (req.body.selectedPlan) ? JSON.stringify({tent_plan_id : req.body.selectedPlan}) : '{}')
  }).save()
  .then(function(user) {
    res.send({ msg: 'Your Account has been successfully created. To Activate your account, please click on the activation link in the mail we have sent you.', user: user.toJSON()  });
      sendMailToActivate(req, user.toJSON())
      return;
  })
  .catch(function(err) {
    if (err.code === 'ER_DUP_ENTRY' || err.code === '23505') {
      return res.status(400).send({ msg: 'The email address you have entered is already associated with another account.' });
    }
  });
};



exports.doPlan = function(req, res, next)
{
  new Plan({ id: req.body.plan_id })
  .fetch()
  .then(function(plan) {
    if(plan)
    {
      new User({ id: req.user.id })
      .fetch()
      .then(function(user) {
        if(user)
        {
          plan = plan.toJSON();
          user.save({
            plan : 'paid',
            plan_description : {
              plan_id         : plan.id,
              plan_details    : plan,
              plan_started_on : moment().unix(),
            }
          }, { patch: true });
          res.status(200).send({ ok : true, m : { msg: 'Plan Started Successfully.'  }, user : user.toJSON()});

          mailer.doMail(user.get('email'), false, false, 'Membership Started', "Hello,<br /><br />" +  "This is a confirmation that your premium membership has just now started on Thinkscademy. You can now enjoy a variety of paid content.<br /><br />Regards");
          if(process.env.CONTACT_TO)

          mailer.doMail(process.env.CONTACT_TO, false, false, 'Membership Started For a User', "Hello,<br /><br />" +  "This is a confirmation that a user with email address "+user.get('email')+" has started a on Thinkscademy. The User  can now enjoy a variety of paid content.<br /><br />Regards");


        }else{
          res.status(400).send({ ok : false, msg: 'User doesnt exist' });
        }
      });
    }else{
      res.status(400).send({ ok : false, msg: 'Plan doesnt exist.' });
    }
  })
  .catch(function(err){
    console.log(err)
    res.status(400).send({ ok : false, msg: 'Some Error occoured' });
  })
}
exports.accountPut = function(req, res, next) {
  // console.log('in account put');
  if ('password' in req.body) {
    req.assert('password', 'Password must be at least 4 characters long').len(4);
    req.assert('confirm', 'Passwords must match').equals(req.body.password);
  } else {
    req.assert('email' , 'Email is not valid').isEmail();
    req.assert('email' , 'Email cannot be blank').notEmpty();
    // req.sanitize('email').normalizeEmail({ remove_dots: false });
  }

  var errors = req.validationErrors();

  if (errors) {
    return res.status(400).send(errors);
  }

  var user = new User({ id: req.user.id });
  if ('password' in req.body) {
    user.save({ password: req.body.password }, { patch: true });
  } else {
    user.save({
      email      : req.body.email,
      first_name : req.body.first_name,
      last_name  : req.body.last_name,
      // location   : req.body.location,
      // age        : req.body.age,
    }, { patch: true });
  }
  setTimeout(function(){


    user.fetch().then(function(user) {
      if ('password' in req.body) {
        res.send({
          m : {
            msg: 'Your password has been changed.'
          },
          user : user.toJSON()
        });
      } else {
        // user.dob   = new Date(user.dob);
        res.send({ user: user,  m : {msg: 'Your profile information has been updated.' }});
      }
    }).catch(function(err) {
      // console.log(err)
      if (err.code === 'ER_DUP_ENTRY') {
        res.status(409).send({ msg: 'The email address you have entered is already associated with another account.' });
      }
    });
  }, 500);
};

/**
 * DELETE /account
 */
exports.accountDelete = function(req, res, next) {
  new User({ id: req.user.id }).destroy().then(function(user) {
    res.send({ msg: 'Your account has been permanently deleted.' });
  });
};

/**
 * GET /unlink/:provider
 */
exports.unlink = function(req, res, next) {
  new User({ id: req.user.id })
    .fetch()
    .then(function(user) {
      switch (req.params.provider) {
        case 'facebook':
          user.set('facebook', null);
          break;
        case 'google':
          user.set('google', null);
          break;
        case 'twitter':
          user.set('twitter', null);
          break;
        case 'vk':
          user.set('vk', null);
          break;
        default:
        return res.status(400).send({ msg: 'Invalid OAuth Provider' });
      }
      user.save(user.changed, { patch: true }).then(function() {
      res.send({ msg: 'Your account has been unlinked.' });
      });
    });
};

/**
 * POST /forgot
 */
exports.forgotPost = function(req, res, next) {
  req.assert('email', 'Email is not valid').isEmail();
  req.assert('email', 'Email cannot be blank').notEmpty();
  // req.sanitize('email').normalizeEmail({ remove_dots: false });

  var errors = req.validationErrors();

  if (errors) {
    return res.status(400).send(errors);
  }

  async.waterfall([
    function(done) {
      crypto.randomBytes(16, function(err, buf) {
        var token = buf.toString('hex');
        done(err, token);
      });
    },
    function(token, done) {
      new User({ email: req.body.email })
        .fetch()
        .then(function(user) {
          if (!user) {
        return res.status(400).send({ msg: 'The email address ' + req.body.email + ' is not associated with any account.' });
          }
          user.set('passwordResetToken', token);
          user.set('passwordResetExpires', new Date(Date.now() + 3600000)); // expire in 1 hour
          user.save(user.changed, { patch: true }).then(function() {
            done(null, token, user.toJSON());
          });
        });
    },
    function(token, user, done) {
      res.send({ msg: 'An email has been sent to ' + user.email + ' with further instructions.' });
      var site_url = process.env.SITE_URL || 'http://thinkscademy.com';
      mailer.doMail(user.email, false, false, 'Reset your password', 'You are receiving this email because you (or someone else) have requested the reset of the password for your account.<br /><br />' +
      'Please click on the following link, or paste this into your browser to complete the process:<br /><br />' +
      site_url + '/reset/' + token + '<br /><br />' +
      'If you did not request this, please ignore this email and your password will remain unchanged.'+"<br /><br />Regards");

    }
  ]);
};

/**
 * POST /reset
 */
exports.resetPost = function(req, res, next) {
  req.assert('password', 'Password must be at least 4 characters long').len(4);
  req.assert('confirm', 'Passwords must match').equals(req.body.password);

  var errors = req.validationErrors();

  if (errors) {
      return res.status(400).send(errors);
  }

  async.waterfall([
    function(done) {
      new User({ passwordResetToken: req.params.token })
        .where('passwordResetExpires', '>', new Date())
        .fetch()
        .then(function(user) {
          if (!user) {
          return res.status(400).send({ msg: 'Password reset token is invalid or has expired.' });
          }
          user.set('password', req.body.password);
          user.set('passwordResetToken', null);
          user.set('passwordResetExpires', null);
          user.save(user.changed, { patch: true }).then(function() {
          done(null, user.toJSON());
          });
        });
    },
    function(user, done) {
      res.send({ msg: 'Your password has been changed successfully.' });

      var site_url = process.env.SITE_URL || 'http://thinkscademy.com';
      mailer.doMail(user.email, false, false, 'Password Changed', 'Hello,<br /><br />' +
      'This is a confirmation that the password for your account ' + user.email + ' has just been changed.'+"<br /><br />Regards");
    }
  ]);
};


exports.listUsers = function(req, res, next) {


 new User( )
 .orderBy('id', 'DESC')
  .fetchAll()
  .then(function(users) {
    if (!users) {
      return res.status(200).send([]);
    }
    return res.status(200).send({ok:true , users: users.toJSON()});
  })
  .catch(function(err) {
    return res.status(200).send([]);
  });
};


exports.deleteUser = function(req, res, next)
{
	new User({ id: req.body.id }).destroy().then(function(post) {
    res.send({ msg: 'The User has been successfully deleted.' });
  }).catch(function(err) {
    return res.status(400).send({ msg : 'Something went wrong while deleting the User' });
  });

}


exports.banUser = function(req, res, next)
{


  var user = new User({ id: req.body.id });

	if(user){
		user.save({
			status : false
		}, { patch: true }).then(function(){
      user.fetch().then(function(user) {
  			res.send({ user : user, msg: 'User has been updated.' });
  		}).catch(function(err) {
  			res.status(400).send({ msg : 'Something went wrong while updating the User' });
  		});

    }).catch(function(err) {
      res.status(400).send({ msg : 'Something went wrong while updating the User' });
    });
	}else{
		res.status(400).send({ msg : 'Something went wrong while updating the User' });
	}


}


exports.makeAdmin = function(req, res, next)
{


  var user = new User({ id: req.body.id });

	if(user){
		user.save({
			role : 'admin'
		}, { patch: true }).then(function(){
      user.fetch().then(function(user) {
  			res.send({ user : user, msg: 'User has been made admin.' });
  		}).catch(function(err) {
  			res.status(400).send({ msg : 'Something went wrong while updating the User' });
  		});

    }).catch(function(err) {
      res.status(400).send({ msg : 'Something went wrong while updating the User' });
    });
	}else{
		res.status(400).send({ msg : 'Something went wrong while updating the User' });
	}


}

exports.makeTeacher = function (req, res, next) {


  var user = new User({ id: req.body.id });

  if (user) {
    user.save({
      role: 'teacher'
    }, { patch: true }).then(function () {
      user.fetch().then(function (user) {
        res.send({ user: user, msg: 'User has been made teacher.' });
      }).catch(function (err) {
        res.status(400).send({ msg: 'Something went wrong while updating the User' });
      });

    }).catch(function (err) {
      res.status(400).send({ msg: 'Something went wrong while updating the User' });
    });
  } else {
    res.status(400).send({ msg: 'Something went wrong while updating the User' });
  }


}

exports.makeMember = function (req, res, next) {


  var user = new User({ id: req.body.id });

  if (user) {
    user.save({
      role: 'student'
    }, { patch: true }).then(function () {
      user.fetch().then(function (user) {
        res.send({ user: user, msg: 'User has been made student.' });
      }).catch(function (err) {
        res.status(400).send({ msg: 'Something went wrong while updating the User' });
      });

    }).catch(function (err) {
      res.status(400).send({ msg: 'Something went wrong while updating the User' });
    });
  } else {
    res.status(400).send({ msg: 'Something went wrong while updating the User' });
  }


}

exports.activateUser = function(req,res, next)
{
  // console.log('514');
  new User({id:  req.params.id}).fetch().then(function(usr){
    // console.log('516 ',req.params );
    if(usr)
    {
      // console.log('519');
      if( moment(usr.get('created_at')).unix() == req.params.timestamp )
      {
        // console.log('522');

        usr.save({ email_verified: true }, { patch: true }).then(function (usr) {
          if (usr.status == true) {
            var token = generateToken(usr);
            res.cookie('satellizer_token', token, { path: '/', maxAge: 90000000, httpOnly: false });
            res.cookie('token', token, { path: '/', maxAge: 90000000, httpOnly: false });
            res.redirect('/plans');
          } else {
            res.redirect('/login');
          }

        }).catch(function(err){
          // console.log(err)
          res.status(200).send('unable to save, please contact admin');
        })

      }else
      {
        res.status(200).send('invalid link');

      }

    }else{
      res.status(200).send('invalid user link');

    }
  }).catch(function(err){
    console.log(err)

    res.status(200).send('unable to fetch, please contact admin');
  })

}

exports.unbanUser = function(req, res, next)
{


  var user = new User({ id: req.body.id });

	if(user){
		user.save({
			status : true
		}, { patch: true }).then(function(){
      user.fetch().then(function(user) {
  			res.send({ user : user, msg: 'User has been updated.' });
  		}).catch(function(err) {
  			res.status(400).send({ msg : 'Something went wrong while updating the User' });
  		});

    }).catch(function(err) {
      res.status(400).send({ msg : 'Something went wrong while updating the User' });
    });
	}else{
		res.status(400).send({ msg : 'Something went wrong while updating the User' });
	}


}
