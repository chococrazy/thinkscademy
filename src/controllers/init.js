var fs = require('fs');
var Plan = require('../models/Plan');
var CMSPage = require('../models/CMSPage');
var Faq = require('../models/Faq');
var Subject = require('../routes/subjects/Subject');
var Course = require('../routes/courses/Course');


exports.init_courses = function (req, res, next) {
  var s =
    [
      {

        "title": "Visual and Grapich Design",
        "subject": "Hindi",
        "image_url": "",
        "learnings": 234324,
        "requirements": "",
        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
        "author_id": 1,
        "views": 0,
        "created_at": "",
        "updated_at": "2018-12-09 21:31:53.767+05:30",
        "topic": "Muhavre"
      },
      {

        "title": "Smart Education of English Grammer",
        "subject": "English",
        "image_url": "",
        "learnings": "Smart Learning Very Smart Very Very Smart",
        "requirements": "Smart Learning Very Smart Very Very Smart",
        "description": "The Smart Education has a first rate academic program, a fully qualified and very well educated teaching staff, bright and comfortable classrooms, continuous student counseling.",
        "author_id": 1,
        "views": 0,
        "created_at": "2018-12-09 21:38:56.964+05:30",
        "updated_at": "2018-12-09 23:03:06.719+05:30",
        "topic": "Tenses"
      }
    ];
  for (var i = 0; i < s.length; i++) {
    new Course(s[i]).save().then().catch(function (r) { console.log(r) });
  }
  res.status(200).send('okkkkk');
};
exports.init_subjects = function (req, res, next) {
  var s = [
    {

      "title": "English",
      "icon": "fas fa-book-reader"
    },
    {

      "title": "Hindi",
      "icon": "fas fa-book-reader"
    },
    {

      "title": "Maths",
      "icon": "fas fa-graduation-cap"
    },
    {

      "title": "Railway",
      "icon": "fas fa-book-reader"
    },
    {

      "title": "Police",
      "icon": "fas fa-book-reader"
    },
    {

      "title": "Backend",
      "icon": "fas fa-book-reader"
    },
    {

      "title": "Frontend",
      "icon": "fas fa-book-reader"
    },
    {

      "title": "Database",
      "icon": "fas fa-book-reader"
    }
  ];

  for (var i = 0; i < s.length; i++) {
    new Subject(s[i]).save().then().catch(function (r) { console.log(r)});
  }
  res.status(200).send('okkkkk');
};
exports.init_faqs = function (req, res, next) {
  var s = [
    {

      "title": "Minimize The Fear, Maximize The Selection",
      "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
      "category": "",
    },
    {

      "title": "Grab The Deftness",
      "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
      "category": "",
    },
    {

      "title": "Amplify The Performance",
      "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
      "category": "",
    },
    {

      "title": "Reach Out The Career Goal",
      "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
      "category": "",
    }
  ];

  for (var i = 0; i < s.length; i++) {
    new Faq(s[i]).save().then().catch(function (r) { console.log(r) });
  }
  res.status(200).send('okkkkk');
};
exports.init_pages = function (req, res, next) {
  var s = [
    {
      title: 'Privacy Policy',
      content: '',
      slug: 'privacy-policy',
      is_in_link: true,
    },

    {
      title: 'Cancellation Policy',
      content: '',
      slug: 'cancellation-policy',
      is_in_link: true,
    },

    {
      title: 'Terms & Conditions',
      content: '',
      slug: 'terms',
      is_in_link: true,
    }
  ];

  for (var i = 0; i < s.length; i++) {
    new CMSPage(s[i]).save().then().catch(function (r) { console.log(r) });
  }
  res.status(200).send('okkkkk');
};
exports.init_plans = function(req, res, next)
{
  var s = [
    {
      title :'Basic',
      cost:'5.99',
      duration:'1 Month',
      max_level_count : 1,
      gateway_plan_id : '',
    }, {
      title :'Standard',
      cost:'7.99',
      duration:'1 Month',
      max_level_count : 3,
      gateway_plan_id : '',
    }, {
      title :'Premium',
      cost:'9.99',
      duration:'1 Month',
      max_level_count : 5,
      gateway_plan_id : '',
    }, {
      title :'Ultra',
      cost:'9.99',
      duration:'1 Month',
      max_level_count : 20,
      gateway_plan_id : '',
    }
  ];

    for(var i =0 ; i < s.length ; i++)
    {
      new Plan(s[i]).save().then().catch(function (r) { console.log(r) });
    }
   res.status(200).send('okkkkk');
};
