var nodemailer  = require('nodemailer');
var fs          = require('fs');


var transporter = nodemailer.createTransport({

  service: 'SendGrid',
  auth: {
    user: process.env.MAIL_USERNAME,
    pass: process.env.MAIL_PASSWORD
  }
});


exports.doMail = function(to, from, from_name, subject, msg){

  if(from_name == false)
  {
    from_name = 'Thinkscademy';
  }
  if(from == false)
  {
    from = 'account@thinkscademy.com';
  }

  var site_url = process.env.SITE_URL || 'http://thinkscademy.com';

  var mailMsg = fs.readFileSync(__dirname+'/email.html', 'utf8');
  mailMsg = mailMsg.replace(new RegExp("{{SITE_URL}}", 'g'), site_url);
  mailMsg = mailMsg.replace(new RegExp("{{CONTENT}}", 'g'), msg+"<br />Thinkscademy");

  var mailOptions = {
    from    : from_name + ' ' + '<'+ from + '>',
    to      : to,
    subject : "Thinkscademy - "+ subject,
    is_html : true,
    html    : mailMsg
  };

  transporter.sendMail(mailOptions, function(err) {
    console.log(err);
  });
}
