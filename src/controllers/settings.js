var fs = require('fs');
var Settings = require('../models/Settings');


exports.deleteSettings = function(req, res, next) {
  new Settings({ id: req.body.id }).destroy().then(function(user) {
    res.send({ msg: 'The settings has been successfully deleted.' });
  }).catch(function(err) {
    return res.status(400).send({ msg : 'Something went wrong while deleting the QA item' });
  });
};


exports.create_SATOSHIS = function(req, res, next)
{


  var s = [

    {
      key      : 'home_heading_1',
      type     : 'text',
      label    : 'Home Heading - Main',
      content  : 'The world\'s largest selection of courses',
      page     : 'Home'
    },
    {
      key      : 'home_heading_2',
      type     : 'text',
      label    : 'Home Heading - Sub',
      content  : 'Skill up with our latest tech courses. 48 hours only!',
      page     : 'Home'
    },

    {
      key      : 'home_course_heading_1',
      type     : 'text',
      label    : 'Home Popular Courses - Main Heading',
      content  : 'Our Popular Courses',
      page     : 'Home'
    },

    {
      key      : 'home_course_heading_2',
      type     : 'text',
      label    : 'Home Popular Courses - Sub Heading',
      content  : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
      page     : 'Home'
    },


    {
      key      : 'home_faq_heading',
      type     : 'text',
      label    : 'Home FAQ Heading',
      content  : 'Why Choose Us?',
      page     : 'Home'
    },


{key : 'address', type:'text', label:'Address', content: '',
page :'Contact Details'},
{key : 'phone', type:'text', label:'Phone Number', content: '+91-999-999-9999', page :'Contact Details'},
{key : 'email', type:'email', label:'Email Address', content: 'abc@xyz.com', page :'Contact Details'},
{key : 'facebook_url', type:'url', label:'Facebook U.R.L.', content: 'http://facebook.com', page :'Contact Details'},
{key : 'twitter_url', type:'url', label:'Twitter U.R.L.', content: 'http://twitter.com', page :'Contact Details'},
{key : 'instagram_url', type:'url', label:'Instagram U.R.L.', content: 'http://instagram.com', page :'Contact Details'},
{key : 'google_plus_url', type:'url', label:'Google Plus U.R.L.', content: 'http://plus.google.com', page :'Contact Details'},


{key : 'contact__text', type:'textarea', label:'Contact Text', content: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', page :'Contact'},





    {
      key: 'stripe_live_publishable_key',
      type: 'text',
      label: 'Stripe Live Publishable Key',
      content: 'pk_live_',
      page: 'Stripe'
    },


    {
      key: 'stripe_test_publishable_key',
      type: 'text',
      label: 'Stripe Test Publishable Key',
      content: 'pk_test_',
      page: 'Stripe'
    },



    {
      key: 'stripe_mode',
      type: 'text',
      label: 'Stripe Mode ( test / live )',
      content: 'test',
      page: 'Stripe'
    },

    {
      key: 'plans_heading',
      type: 'textarea',
      label: 'Plans Page Heading',
      content: "Choose a plan that's right for you",
      page: 'Plans'
    },
    {
      key: 'plans_sub_heading',
      type: 'textarea',
      label: 'Plans Page Sub Heading',
      content: "Loreplans_sub_headingm Ipsum is simply dummy text.",
      page: 'Plans'
    },







  ];

    for(var i =0 ; i < s.length ; i++)
    {
      new Settings(s[i]).save().then().catch(function(r){ });
    }
   res.status(200).send('okkkkk');


}

exports.updateSettingsFile = function(req, res, next)
{

  // console.log(req.body.data)
  var data  = req.body.data;
  data = JSON.parse(data);
  if(req.file.path)
  {
    // console.log(req.file)
    var path = req.file.filename;
    var name = req.file.originalname;
    name     = name.split('.');
    name     = name[name.length-1];
    fs.renameSync('uploads/images/'+path, 'uploads/images/key_' + path + "."+name);

    var booking = new Settings().where(
    {
      key: data.key
    }).fetch().then(function(booking){
			if(!booking)
			{
				return res.status(400).send(
					{
						msg : 'Something went wrong while uploading File'
					});;
			}
			booking.save(
			{
				content : "key_" + path + "."+name,
			})
			.then(function(settings)
			{
				res.send(
				{
					settings : settings,
					msg     : 'Image uploaded successfully.'
				});
			}).catch(function(err)
			{
				res.status(400).send(
				{
					msg : 'Something went wrong while uploading File'
				});
			});
		}).catch(function(err)
			{
				res.status(400).send(
				{
					msg : 'Something went wrong while uploading File'
				});
			});
	}


}

exports.updateSettings = function(req, res, next) {
  req.assert('content'   , 'Content cannot be blank').notEmpty();

  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).send(errors);
  }

  var settings = new Settings({ id: req.body.id });

  settings.save({
    // title   : req.body.title,
    content        : req.body.content,
    content_hebrew : req.body.content_hebrew
  });

  settings.fetch().then(function(settings) {
    res.send({ settings : settings, msg: 'settings has been updated.' });
  }).catch(function(err) {
    res.status(400).send({ msg : 'Something went wrong while updating the settings' });
  });
};

exports.addSettings = function(req, res, next) {
  req.assert('title'   , 'Title cannot be blank').notEmpty();

  var errors = req.validationErrors();

  if (errors) {
    return res.status(400).send(errors);
  }

  new Settings({
    // title   : req.body.tit
    content        : req.body.content,
    content_hebrew : req.body.content_hebrew
  }).save()
  .then(function(settings) {
      res.send({ ok:true , msg: 'New settings has been created successfully.' });
  })
  .catch(function(err) {
    console.log(err)
       return res.status(400).send({ msg: 'Something went wrong while created a new settings' });
   });
};


exports.listSettings = function(req, res, next) {


 new Settings( )
 .orderBy('id', 'DESC')
  .fetchAll()
  .then(function(settings) {
    if (!settings) {
      return res.status(200).send([]);
    }
    return res.status(200).send({ok:true , settings: settings.toJSON()});
  })
  .catch(function(err) {
    return res.status(200).send([]);
  });
};

exports.listSettingsDefault = function(req, res, next)
{


   new Settings( )
   .orderBy('id', 'DESC')
   .where('autoload', true)
    .fetchAll()
    .then(function(settings) {
      if (!settings) {
        return res.status(200).send([]);
      }
      return res.status(200).send({ok:true , settings: settings.toJSON()});
    })
    .catch(function(err) {
      return res.status(200).send([]);
    });
}

exports.listSingleSettings = function(req, res, next)
{
  new Settings( ).where('id', req.params.id)
   .fetch()
   .then(function(settings) {
     if (!settings) {
       return res.status(200).send({id : req.params.id, title: '' });
     }
     return res.status(200).send({ok:true , settings: settings.toJSON()});
   })
   .catch(function(err) {
     return res.status(400).send({id : req.params.id, title: '', msg: 'failed to fetch from db'});
   });

}
