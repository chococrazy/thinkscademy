var Plan = require('../models/Plan');
const stripe = require('stripe')(process.env.STRIPE_KEY);
var User = require('../models/User');
var mailer     = require('./mailer');

function stripe_afterCustomerCreation(customer, req, res, user, plan) {

  let plan_stripe_id = plan.gateway_plan_id;

  stripe.subscriptions.create(
    {
      customer: customer.id,
      plan: plan_stripe_id,
      quantity: 1
    },
    function(err, subscription) {
      if (err) {
        console.log(err);
        return res
          .status(200)
          .send({ok: false, msg: 'Failed to start subscription.'});
      }
      const subs_obj = {
        subscription_id: subscription.id,
        current_period_end: subscription.current_period_end,
        current_period_start: subscription.current_period_start,
        start: subscription.start,
        status: subscription.status,
        customer: subscription.customer,
        quantity: subscription.quantity,
        plan_id: subscription.plan.id,
        trial_end: subscription.trial_end,
        subscription_obj: subscription

      };

      User.forge({id: req.user.id})
      .fetch()
      .then(function(user) {
        if (!user) {
          return res.status(400).send({ok: false, msg: 'Some Error #60'});
        }
        user
        .save({
          plan_description: subs_obj,
          plan_name: plan.id,
          stripe_customer_id : customer.id,
          max_levels_allowed : plan.max_level_count,
          plan: 'paid'
          }, { patch: true })
        .then(function() {
          res.status(200).send({
            ok: true,
            action: 'SUBSCRIPTION_ADDED',
            msg:
            'Your subscription to the '+plan.title+' plan was successful. You are being billed for duration of ' + plan.duration + '. We are redirecting you to the plans page.'
          });
            mailer.doMail(user.email, false, false, 'New Subscription', 'Hi<br /><br />Your subscription to the '+plan.title+' plan was successful. You are being billed for duration of ' + plan.duration + '.<br /><br />"+link+"<br /><br />Regards')
        })
        .catch(function(err) {
         console.log(err);
          res.status(400).send({
            ok: false,
            msg:
              'Subscription Created! ERROR fetching data, please contact support. ERR#54'
          });
        });
      });
    })

}

exports.charge_user = function(req, res, next) {
  if (!req.body.tokenObj) {
    return res.status(400).send({ok: false, msg: 'Invalid Stripe Token'});
  }
  if (!req.body.plan_id) {
    return res.status(400).send({ok: false, msg: 'Invalid Plan Id'});
  }
  const stripeTokenObj = req.body.tokenObj;
  const stripeToken = stripeTokenObj.id;

  Plan.forge({
    id: req.body.plan_id
  })
    .fetch()
    .then(function(plan) {
      if (!plan) {
        return res.status(400).send({
          ok: false,
          msg:
            'Plan Does Not Exist'
        });
      }
      plan = plan.toJSON();

      const user_stripe_cust_id = req.user.stripe_customer_id;
      // check if customer id created on stripe.
      if (user_stripe_cust_id && user_stripe_cust_id != '') {
        // customer exists!...
        stripe.customers.retrieve(user_stripe_cust_id, function(err, customer) {
          if (err) {
            console.log(error)
            res.status(400).send({ok: false, msg: 'Some error #127'});
            return;
          }
          stripe.customers.createSource(
            customer.id,
            {
              source: stripeToken
            },
            function(err, card) {
              if (err) {
                console.log(err);
                res.status(400).send({ok: false, msg: 'Unable to use this card for payment, please use another card.'});
                return;
              }
              stripe.customers.update(
                customer.id,
                {
                  default_source: card.id
                },
                function(err, customer) {
                  if (err) {
                    console.log(err)
                    res.status(400).send({ok: false, msg: 'Unable to use this card as a default payment method, please use another card.'});
                    return;
                  }
                  // all done, proceed with subscription.
                  return stripe_afterCustomerCreation(
                    customer,
                    req,
                    res,
                    req.user,
                    plan
                  );
                }
              );
            }
          );
        });
      } else {
        // create a customer
        stripe.customers.create(
          {
            description: 'Thinkscademy User Membership',
            email: req.user.email,
            source: stripeToken
          },
          function(err, customer) {
            if (err) {
              console.log(err);
              return res.status(400).send({
                ok: false,
                msg: 'We were unable to create a subscription for you. ERR #143'
              });
            }
            stripe_afterCustomerCreation(
              customer,
              req,
              res,
              req.user,
              plan
            );
            // userGrp
              // .save({stripe_cust_id: customer.id})
              // .then(function(userGrp) {
                //updated. customer Stripe ID to db, lets grab some money!

              // })
              // .catch(function(err) {
              //   console.log(err);
              //   // oh ho, customer id can not be saved?
              //   return res.status(400).send({
              //     ok: false,
              //     msg: 'We were unable to save a subscription for you. ERR#150'
              //   });
              // });
          }
        );
      }
    })
    .catch(function(err) {
      console.log(err)
      res
        .status(400)
        .send({ok: false, msg: 'Unable to fetch user details. ERR #158'});
    });
};

exports.charge_userChange = function (req, res, next)
{
  if (!req.body.plan_id) {
    return res.status(400).send({ ok: false, msg: 'Invalid Plan Id' });
  }
  Plan.forge({
    id: req.body.plan_id
  })
    .fetch()
    .then(function (plan) {
      if (!plan) {
        return res.status(400).send({
          ok: false,
          msg:
            'Plan Does Not Exist'
        });
      }
      plan = plan.toJSON();


      const user_stripe_cust_id = req.user.stripe_customer_id;
      let user_stripe_desc = req.user.plan_description;
      if (user_stripe_desc)
      {
        user_stripe_desc = JSON.parse(user_stripe_desc);
      } else {
        user_stripe_desc = {};

      }
      const subscription_id = user_stripe_desc.subscription_id;
      // check if customer id created on stripe.
      if (user_stripe_cust_id && user_stripe_cust_id != '' && subscription_id) {
        stripe.subscriptions.retrieve(subscription_id, function (err, subscription2) {
          // console.log(a);
          // console.log('---');
          // console.log(b);
          // return;

          if (err)
          {
            res.status(400).send({ ok: false, msg: 'Subscription details not found.' })
            return;
            }

        stripe.subscriptions.update(subscription_id, {
          cancel_at_period_end: false,
          items: [{
            id: subscription2.items.data[0].id,
            plan: plan.gateway_plan_id,
          }]
        },
        function (err, subscription) {
          // asynchronously called
          if(err)
          {
            console.log(err);
            res
              .status(400)
              .send({ ok: false, msg: 'Unable to change plan. ERR #243' });
            return;
          } else {

            res.status(200).send({
              ok: true,
              action: 'DOWNGRADED',
              msg:
                'Your subscription to the ' + plan.title + ' plan was successful. You are being billed for duration of ' + plan.duration + '. We are redirecting you to the plans page.'
            });
            mailer.doMail(user.email, false, false, 'Change in Subscription', 'Hi<br /><br />Your subscription to the '+plan.title+' plan was successful. You are being billed for duration of ' + plan.duration + '.<br /><br />"+link+"<br /><br />Regards')

            const subs_obj = {
              subscription_id: subscription.id,
              current_period_end: subscription.current_period_end,
              current_period_start: subscription.current_period_start,
              start: subscription.start,
              status: subscription.status,
              customer: subscription.customer,
              quantity: subscription.quantity,
              plan_id: subscription.plan.id,
              trial_end: subscription.trial_end,
              subscription_obj: subscription

            };

            User.forge({ id: req.user.id })
              .fetch()
              .then(function (user) {
                if (!user) {
                  return res.status(400).send({ ok: false, msg: 'Some Error #60' });
                }

                user
                  .save({
                    plan_description: subs_obj,
                    plan_name: plan.id,
                    // stripe_customer_id: customer.id,
                    max_levels_allowed: plan.max_level_count,
                    plan: 'paid'
                  }, { patch: true })
                  .then(function () {

                  })
                  .catch(function (err) {
                    console.log(err);

                  });
              }).catch(function(err){})
          }
          });

        });

      } else {
        res.status(400).send({ok:false, msg: 'Subscription details not found.'})
      }
    });

}


exports.deletePlan = function(req, res, next) {
  new Plan({ id: req.body.id }).destroy().then(function(user) {
    res.send({ msg: 'The QA Item has been successfully deleted.' });
  }).catch(function(err) {
    return res.status(400).send({ msg : 'Something went wrong while deleting the Plan' });
  });
};



exports.updatePlan = function(req, res, next) {
  req.assert('title'       , 'Title cannot be blank').notEmpty();
  req.assert('cost'        , 'Cost cannot be blank').notEmpty();
  req.assert('duration'    , 'Duration cannot be blank').notEmpty();
  req.assert('max_level_count' , 'Max Number of Levels cannot be blank').notEmpty();

  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).send(errors);
  }

  var plan = new Plan({ id: req.body.id });

  plan.save({
    title          : req.body.title,
    duration       : req.body.duration,
    cost           : req.body.cost,
    gateway_plan_id: req.body.gateway_plan_id,
    max_level_count: req.body.max_level_count,
    description    : req.body.description ? req.body.description : ''
  });

  plan.fetch().then(function(plan) {
    res.send({ plan : plan, msg: 'Plan has been updated.' });
  }).catch(function(err) {
    res.status(400).send({ msg : 'Something went wrong while updating the Plan' });
  });
};

exports.addPlan = function (req, res, next) {
  req.assert('title', 'Title cannot be blank').notEmpty();
  req.assert('cost', 'Cost cannot be blank').notEmpty();
  req.assert('duration', 'Duration cannot be blank').notEmpty();
  req.assert('max_level_count', 'Max Number of Levels cannot be blank').notEmpty();

  var errors = req.validationErrors();

  if (errors) {
    return res.status(400).send(errors);
  }

  new Plan({
    title          : req.body.title,
    duration       : req.body.duration,
    cost           : req.body.cost,
    gateway_plan_id: req.body.gateway_plan_id,
    max_level_count: req.body.max_level_count,
    description    : req.body.description ? req.body.description : ''
  }).save()
  .then(function(user) {
      res.send({ ok:true , msg: 'New Plan has been created successfully.' });
  })
  .catch(function(err) {
    console.log(err)
       return res.status(400).send({ msg: 'Something went wrong while created a new Plan' });
   });
};


exports.listPlan = function(req, res, next) {


 new Plan( )
 .orderBy('max_level_count', 'ASC')
  .fetchAll()
  .then(function(plans) {
    if (!plans) {
      return res.status(200).send([]);
    }
    return res.status(200).send({ok:true , plans: plans.toJSON()});
  })
  .catch(function(err) {
    return res.status(200).send([]);
  });
};


exports.listSinglePlan = function(req, res, next)
{
  new Plan( ).where('id', req.params.id)
   .fetch()
   .then(function(plan) {
     if (!plan) {
       return res.status(200).send({id : req.params.id, title: '',cost: ''});
     }
     return res.status(200).send({ok:true , plan: plan.toJSON()});
   })
   .catch(function(err) {
     return res.status(400).send({id : req.params.id, title: '',cost: '', msg: 'failed to fetch from db'});
   });

}
