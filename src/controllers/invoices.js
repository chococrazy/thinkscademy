var Plan = require('../models/Plan');
const stripe = require('stripe')(process.env.STRIPE_KEY);
var User = require('../models/User');

const invo = function(data, cb)
{
  stripe.invoices.list(
    data,
    cb
  );
}

exports.allInvoices = function(req, res, next)
{

  invo({
    limit:100,
    expand: ["data.customer"]
  }, function(err, data){
    if(err)
    {
      res.status(400).send({ok:false, data:[], msg:err});
      return;
    }
    res.status(200).send({ok:true,items: data.data});
  });
}



exports.allInvoicesOf = function(req, res, next)
{
  invo({limit:100, customer:req.params.id}, function(err, data){
    if(err)
    {
      res.status(400).send({ok:false, data:[], msg:err});
    }
    res.status(200).send({ok:true,items: data.data});
  });
}
