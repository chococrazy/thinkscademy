var mailer     = require('./mailer');

exports.contactGet = function(req, res) {
  res.render('contact', {
    title: 'Contact'
  });
};



exports.contactPost = function(req, res) {
  req.assert('name'      , 'Name cannot be blank').notEmpty();
  req.assert('last_name' , 'Last Name cannot be blank').notEmpty();
  req.assert('email'     , 'Email is not valid').isEmail();
  req.assert('email'     , 'Email cannot be blank').notEmpty();
   req.assert('message'   , 'Message cannot be blank').notEmpty();
   req.sanitize('email').normalizeEmail({ remove_dots: false });

  var errors = req.validationErrors();

  if (errors) {
    return res.status(400).send(errors);
  }

  res.send({ msg: 'Thank you! Your feedback has been submitted.' });

  mailer.doMail(process.env.CONTACT_TO, req.body.email, req.body.name +" "+req.body.last_name, 'Contact Form: '+req.body.email, "Hi\n\nA new Contact form has been submitted on Thinkscademy. <br />Message: "+req.body.message+"<br /><br />Regards")

};





//
// exports.listAll = function(req, res, next)
// {
//
//   new MachineRequest()
//     .orderBy('id', 'DESC')
//     .fetchAll( )
//     .then(function(bookings)
//     {
//       if (!bookings)
//       {
//         return res.status(200).send([]);
//       }
//       return res.status(200).send(
//       {
//         ok   : true,
//         rows : bookings.toJSON()
//       });
//     })
//     .catch(function(err)
//     {
//       return res.status(200).send({ok:true, rows: []});
//     });
// }
