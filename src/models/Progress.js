var bookshelf = require('../config/bookshelf');
// var User = require('../../models/User');

var Progress = bookshelf.Model.extend({
  tableName: 'progress',
  hasTimestamps: false,
	 user_id : function() {
    return this.belongsTo('User', 'user_id');
  },

  parse: function (response) {
    response.course_progress = response.course_progress ? JSON.parse(response.course_progress) : {}
    return response;
  },
  format: function (response){
    response.course_progress = response.course_progress ? JSON.stringify(response.course_progress) : '{}'
    return response;
  }

});

module.exports = Progress;
