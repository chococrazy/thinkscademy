var bookshelf = require('../config/bookshelf');
var User = require('./User');

var Quiz = bookshelf.Model.extend({
  tableName: 'quiz_results',
  hasTimestamps: true,
  users : function() {
   return this.belongsTo(User);
 },
});

module.exports = Quiz;
