
var bookshelf = require('../config/bookshelf');

var Plans = bookshelf.Model.extend({
  tableName: 'plans',
  hasTimestamps: true,
});

module.exports = Plans;
