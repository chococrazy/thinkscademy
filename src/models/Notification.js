var bookshelf = require('../config/bookshelf');

var Notification = bookshelf.Model.extend({
  tableName: 'notifications',
  hasTimestamps: true,
});

module.exports = Notification;
