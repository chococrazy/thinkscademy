
var bookshelf = require('../config/bookshelf');

var Settings = bookshelf.Model.extend({
  tableName: 'settings',
  hasTimestamps: true,
});

module.exports = Settings;
