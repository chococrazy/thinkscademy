var bookshelf = require('../config/bookshelf');
var Atm = require('../models/Atm');
var User = require('../models/User');

var Invoice = bookshelf.Model.extend(
{
  tableName: 'invoices',
  hasTimestamps: true,
  user: function()
  {
    return this.belongsTo(User);
  }

});

module.exports = Invoice;
