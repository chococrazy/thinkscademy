exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('lessons', function (table) {
      table.increments();
      table.string('title');
      table.text('description');
      table.text('quiz_data');
      table.integer('course_id').references('course.id');
      table.string('order');
      table.text('attachment_1');
      // table.string('attachemnt_2');
      // table.string('attachemnt_3');
      table.timestamps();
    }),
    knex.schema.createTable('lesson_usage', function (table) {
      table.increments();
      table.integer('lesson_id').references('lessons.id');;
      table.text('completed_users');
      table.text('completed_on');
      table.timestamps();
    })

  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    // knex.schema.dropTable('lessons'),
    // knex.schema.dropTable('lesson_usage'),
  ])
};
