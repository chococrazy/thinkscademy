exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('lessons', function (table) {
      table.integer('complete_count').defaultTo(0);
      table.integer('start_count').defaultTo(0);
    }),
    knex.schema.createTable('quiz_results', function(table) {
      table.increments();
      table.timestamps();
      table.integer('user_id').references('users.id');
      table.integer('lesson_id').references('lessons.id');
      table.integer('correct_answers');
      table.text('results');
    })
  ]);
};
exports.down = function(knex, Promise) {
  return Promise.all([
  ])
};
