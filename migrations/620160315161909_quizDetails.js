exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('progress', function (table) {
      table.increments();
      table.integer('user_id').references('course.id');
      table.text('course_progress');
      // table.timestamps();
    }),
    knex.schema.dropTable('lesson_usage')
  ]);
};
exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('progress')
  ])
};
