exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('users', function(table) {
      table.increments();
      table.string('first_name');
      table.string('last_name');
      table.string('username').unique();
      table.string('email').unique();
      table.string('password');
      table.string('gender');
      table.string('passwordResetToken');
      table.dateTime('passwordResetExpires');
      table.string('phone_number');
      table.string('address');
      table.string('city');
      table.string('zipcode');
      table.string('country');
      table.string('dob');
      table.string('role').defaultTo("student");
      table.string('plan').defaultTo('free');
      table.string('plan_name').defaultTo('free');
      table.integer('max_levels_allowed').defaultTo(0);

      table.string('stripe_customer_id');

      table.text('plan_description').defaultTo('{}');
      // table.text('stripe_customer_id');
      table.boolean('status').defaultTo(true);
      table.boolean('email_verified').defaultTo(false);
      table.timestamps();
    }),
    knex.schema.createTable('plans', function(table) {
      table.increments();
      table.string('title');
      table.string('cost');
      table.string('duration');
      table.integer('max_level_count');
      table.text('description');
      table.text('gateway_plan_id');
      table.timestamps();
    }),
    knex.schema.createTable('faqs', function(table) {
      table.increments();
      table.string('title');
      table.text('content');
      table.string('category');
      table.timestamps();
    }),
    knex.schema.createTable('cms_pages', function(table) {
      table.increments();
      table.string('title');
      table.text('content');
      table.string('slug');
      table.string('image_url');
      table.string('is_in_link').defaultTo('no');
      table.timestamps();
    }),
    knex.schema.createTable('notifications', function(table) {
      table.increments();
      table.string('description');
      table.integer('user_id');
      table.string('type');
      table.integer('object_id');
      table.timestamps();
    }),
    knex.schema.createTable('settings', function(table) {
      table.increments();
      table.string('key').unique();
      table.string('type');
      table.text('content');
      table.string('page');
      table.string('label');
      table.timestamps();
    }),
    knex.schema.createTable('subject', function (table) {
      table.increments();
      table.string('title');
      table.string('icon').defaultTo('fas fa-book-reader');
      // table.timestamps();
    }),
    knex.schema.createTable('topic', function (table) {
      table.increments();
      table.string('title');
      table.string('subject');
      // table.string('icon').defaultTo('fas fa-book-reader');
      // table.timestamps();
    }),
    knex.schema.createTable('category', function (table) {
      table.increments();
      table.string('title');
      // table.timestamps();
    }),
    knex.schema.createTable('blogpost', function (table) {
      table.increments();
      table.string('title');
      table.text('content');
      table.integer('category_id').references('category.id');
      table.string('slug');
      table.string('image_url');
      table.text('short_content');
      table.timestamps();
    }),
    knex.schema.createTable('course', function (table) {
      table.increments();
      table.string('title');
      table.string('subject');
      table.string('topic');
      table.string('image_url');
      table.text('learnings');
      table.text('requirements');
      table.text('description');
      table.integer('author_id').references('users.id');
      table.integer('views').defaultTo(0);
      table.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    // knex.schema.dropTable('users')
  ])
};
