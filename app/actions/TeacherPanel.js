export function addCourse(addedit, course, token, cb) {
  return (dispatch) => {
    dispatch({
      type: 'CLEAR_MESSAGES'
    });
    let api_path = addedit == 'edit' ? '/api/courses/edit/' : '/api/courses/add';
    return fetch(api_path, {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify(course)
    }).then((response) => {
      if (response.ok) {
        if(cb) cb();
        return response.json().then((json) => {
          dispatch({
            type: 'ADD_COURSE_SUCCESS',
            messages: Array.isArray(json) ? json : [json]
          });
        });
      } else {
        return response.json().then((json) => {
          jQuery('html, body').animate({scrollTop: 200});
          dispatch({
            type: 'ADD_COURSE_FAILURE',
            messages: Array.isArray(json) ? json : [json]
          });
        });
      }
    });
  };
}



export function addLesson(addedit, course, token, cb) {
  return (dispatch) => {
    dispatch({
      type: 'CLEAR_MESSAGES'
    });
    let api_path = addedit == 'edit' ? '/api/lessons/edit/' : '/api/lessons/add';
    return fetch(api_path, {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify(course)
    }).then((response) => {
      if (response.ok) {
        if(cb) cb();
        return response.json().then((json) => {
          dispatch({
            type: 'ADD_COURSE_SUCCESS',
            messages: Array.isArray(json) ? json : [json]
          });
        });
      } else {
        return response.json().then((json) => {
          jQuery('html, body').animate({scrollTop: 200});
          dispatch({
            type: 'ADD_COURSE_FAILURE',
            messages: Array.isArray(json) ? json : [json]
          });
        });
      }
    });
  };
}
