import moment from 'moment';
import cookie from 'react-cookie';
import { browserHistory } from 'react-router';


export function doPlan(id, token, cb)
{
  return (dispatch) => {
    dispatch({
      type: 'CLEAR_MESSAGES'
    });
    return fetch('/doPlan', {
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ plan_id : id })
    }).then((response) => {
      return response.json().then((json) => {
        if (response.ok) {
          window.location.reload();
          cb();
          dispatch({
            type: 'UPDATE_PROFILE_SUCCESS',
            messages: [json.m]
          });
          dispatch({
            type: 'PROFILE_UPDATE',
            user: json.user
          });
        } else {
          dispatch({
            type: 'CONTACT_FORM_FAILURE',
            messages: Array.isArray(json) ? json : [json]
          });
        }
      });
    });
  };
}
export function login(email, password) {
  return (dispatch) => {
    dispatch({
      type: 'CLEAR_MESSAGES'
    });
    return fetch('/login', {
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        email: email,
        password: password
      })
    }).then((response) => {
      if (response.ok) {
        return response.json().then((json) => {
          dispatch({
            type: 'LOGIN_SUCCESS',
            token: json.token,
            user: json.user,
            progress: json.progress
          });

          cookie.save('satellizer_token', json.token, { path: '/', expires: moment().add(1, 'month').toDate() });
          cookie.save('token', json.token, { path: '/', expires: moment().add(1, 'month').toDate() });
          if(json.user.plan == 'free' && json.user.role=='student')
          {
            setTimeout(function(){
              window.location.href = '/plans';
            }, 200);
          }else{
            setTimeout(function(){
              window.location.href = '/account';
            }, 200);
          }
        });
      } else {
        return response.json().then((json) => {
          dispatch({
            type: 'LOGIN_FAILURE',
            messages: Array.isArray(json) ? json : [json]
          });
        });
      }
    });
  };
}

export function signup(first_name, last_name, email, password, password_confirm, signup_as, selectedPlan) {
  return (dispatch) => {
    dispatch({
      type: 'CLEAR_MESSAGES'
    });
    return fetch('/signup', {
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ first_name: first_name, last_name: last_name, email: email, password: password, password_confirm: password_confirm, selectedPlan: selectedPlan, signup_as: signup_as })
    }).then((response) => {
      return response.json().then((json) => {
        jQuery('html, body').animate({ scrollTop: jQuery('.box_main_heading').offset().top });
        if (response.ok) {

          dispatch({
            type: 'SIGNUP_SUCCESS',
            messages: Array.isArray(json) ? json : [json]
          });
          // browserHistory.push('/');
          // localStorage.setItem('satellizer_token', json.token);
         //cookie.save('token', json.token, { path: '/', expires: moment().add(1, 'hour').toDate() });
        } else {
          dispatch({
            type: 'SIGNUP_FAILURE',
            messages: Array.isArray(json) ? json : [json]
          });
        }
      });
    });
  };
}

export function logout() {
  cookie.remove('token');
  return {
    type: 'LOGOUT_SUCCESS'
  };
  setTimeout(function(){
    window.location.href = '/';
  }, 1000);

}

export function forgotPassword(email) {
  return (dispatch) => {
    dispatch({
      type: 'CLEAR_MESSAGES'
    });
    return fetch('/forgot', {
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ email: email })
    }).then((response) => {
      if (response.ok) {
        return response.json().then((json) => {
          dispatch({
            type: 'FORGOT_PASSWORD_SUCCESS',
            messages: [json]
          });
        });
      } else {
        return response.json().then((json) => {
          dispatch({
            type: 'FORGOT_PASSWORD_FAILURE',
            messages: Array.isArray(json) ? json : [json]
          });
        });
      }
    });
  };
}

export function resetPassword(password, confirm, pathToken) {
  return (dispatch) => {
    dispatch({
      type: 'CLEAR_MESSAGES'
    });
    return fetch(`/reset/${pathToken}`, {
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        password: password,
        confirm: confirm
      })
    }).then((response) => {
      if (response.ok) {
        return response.json().then((json) => {
          browserHistory.push('/login');
          dispatch({
            type: 'RESET_PASSWORD_SUCCESS',
            messages: [json]
          });
        });
      } else {
        return response.json().then((json) => {
          dispatch({
            type: 'RESET_PASSWORD_FAILURE',
            messages: Array.isArray(json) ? json : [json]
          });
        });
      }
    });
  };
}

export function updateProfile(state, token, cb) {
  // console.log('...')
  return (dispatch) => {
    dispatch({
      type: 'CLEAR_MESSAGES'
    });
    return fetch('/account', {
      method: 'put',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify({
        email      : state.email,
        first_name : state.first_name,
        last_name  : state.last_name,
        // location   : state.location,
        // age        : state.age,
        // location   : state.location,
        // website    : state.website
      })
    }).then((response) => {
      if (response.ok) {
        cb();
        return response.json().then((json) => {
          dispatch({
            type: 'UPDATE_PROFILE_SUCCESS',
            messages: [json.m]
          });
          dispatch({
            type: 'PROFILE_UPDATE',
            user: json.user
          });
        });
      } else {
        return response.json().then((json) => {
          dispatch({
            type: 'UPDATE_PROFILE_FAILURE',
            messages: Array.isArray(json) ? json : [json]
          });
        });
      }
    });
  };
}

export function changePassword(password, confirm, token) {
  return (dispatch) => {
    dispatch({
      type: 'CLEAR_MESSAGES'
    });
    return fetch('/account', {
      method: 'put',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify({
        password: password,
        confirm: confirm
      })
    }).then((response) => {
      if (response.ok) {
        return response.json().then((json) => {
          dispatch({
            type: 'CHANGE_PASSWORD_SUCCESS',
            messages: [json]
          });
        });
      } else {
        return response.json().then((json) => {
          dispatch({
            type: 'CHANGE_PASSWORD_FAILURE',
            messages: Array.isArray(json) ? json : [json]
          });
        });
      }
    });
  };
}

export function deleteAccount(token) {
  return (dispatch) => {
    dispatch({
      type: 'CLEAR_MESSAGES'
    });
    return fetch('/account', {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
    }).then((response) => {
      if (response.ok) {
        return response.json().then((json) => {
          dispatch(logout());
          dispatch({
            type: 'DELETE_ACCOUNT_SUCCESS',
            messages: [json]
          });
        });
      }
    });
  };
}
