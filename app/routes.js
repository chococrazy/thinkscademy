import React                 from 'react';
import cookie                from 'react-cookie';
import { IndexRoute, Route } from 'react-router';
import App                   from './components/App';
import Home                  from './components/Home';
import About                 from './components/About';
import CMSPage               from './components/CMSPage';
import Contact               from './components/Contact';
import NotFound              from './components/NotFound';
import Login                 from './components/Account/Login';
import Signup                from './components/Account/Signup';
import Posts                 from './components/Blog/Posts';
import SinglePost            from './components/Blog/SinglePost';
import CourseList            from './components/Courses/CourseList';
import CourseSingle from './components/Courses/CourseSingle';
import Lesson from './components/Lesson/SingleLesson';
import Quiz from './components/Lesson/Quiz';
import MyCourses from './components/Dashboard/MyCourses';


import QuizResults from './components/Dashboard/QuizResults';

import LessonEdit from './components/Dashboard/LessonEdit';
import LessonAdd from './components/Dashboard/LessonAdd';
import LessonList from './components/Dashboard/LessonList';
import EditCourse from './components/Dashboard/EditCourse';
import AddCourse from './components/Dashboard/AddCourse';



import Plan from './components/Plan';
import Buy                  from './components/Account/Buy';
import Profile               from './components/Account/Profile';
import Forgot                from './components/Account/Forgot';
import Reset                 from './components/Account/Reset';

export default function getRoutes(store) {
  const ensureAuthenticated = (nextState, replace) => {
    if (!store.getState().auth.token) {
      replace('/login');
    }
  };

  const ensureAuthenticatedTeacher = (nextState, replace) => {
    if (!store.getState().auth.token) {
      replace('/login');
    }
    // console.log(store.getState().auth.user.role)
    if (store.getState().auth.user.role != 'teacher') {
      replace('/account')
    }
  }
  const skipIfAuthenticated = (nextState, replace) => {
    if (store.getState().auth.token) {
      replace('/');
    }
  };
  const checkAge = (nextState, replace) =>{
  }
  const clearMessages = () => {
    store.dispatch({
      type: 'CLEAR_MESSAGES'
    });
  };
  return (
    <Route path="/" component={App}>
      <IndexRoute  onEnter={checkAge} component={Home} onLeave={clearMessages}/>
      <Route path="/contact" component={Contact} onLeave={clearMessages}/>
      <Route path="/about" component={About} onLeave={clearMessages}/>
      <Route path="/login/:successVerify" component={Login} onEnter={skipIfAuthenticated} onLeave={clearMessages}/>
      <Route path="/login" component={Login} onEnter={skipIfAuthenticated} onLeave={clearMessages}/>
      <Route path="/signup/:selectedPlan" component={Signup} onEnter={skipIfAuthenticated} onLeave={clearMessages}/>
      <Route path="/signup" component={Signup} onEnter={skipIfAuthenticated} onLeave={clearMessages}/>
      <Route path="/account" component={Profile} onEnter={ensureAuthenticated} onLeave={clearMessages}/>
      <Route path="/forgot" component={Forgot} onEnter={skipIfAuthenticated} onLeave={clearMessages}/>
       <Route path='/reset/:token' component={Reset} onEnter={skipIfAuthenticated} onLeave={clearMessages}/>

      <Route path='/plans' component={Plan} onLeave={clearMessages} />

      <Route path='/buy/:plan_id/:plan_name' component={Buy} />

      <Route path="/dashboard/course/lessons/:course_id/quiz/:lesson_id/results" component={QuizResults} onEnter={ensureAuthenticatedTeacher} onLeave={clearMessages} />


      <Route path="/dashboard/course/lessons/:course_id/edit/:lesson_id" component={LessonEdit} onEnter={ensureAuthenticatedTeacher} onLeave={clearMessages} />


      <Route path="/dashboard/course/lessons/:course_id/add" component={LessonAdd} onEnter={ensureAuthenticatedTeacher} onLeave={clearMessages} />
      <Route path="/dashboard/course/lessons/:course_id" component={LessonList} onEnter={ensureAuthenticatedTeacher} onLeave={clearMessages} />
      <Route path="/dashboard/course/edit/:id" component={EditCourse} onEnter={ensureAuthenticatedTeacher} onLeave={clearMessages} />
      <Route path="/dashboard/course/add" component={AddCourse} onEnter={ensureAuthenticatedTeacher} onLeave={clearMessages} />
      <Route path="/dashboard" component={MyCourses} onEnter={ensureAuthenticatedTeacher}  onLeave={clearMessages} />

      <Route path='/blog/tag/:tag_slug' component={Posts}  onEnter={checkAge} onLeave={clearMessages}/>
      <Route path='/blog/post/:id/:slug' component={SinglePost}  onEnter={checkAge} onLeave={clearMessages}/>
      <Route path='/blog/post/:id' component={SinglePost} onEnter={checkAge} onLeave={clearMessages} />


      <Route path='/blog/:category_slug' component={Posts} onEnter={checkAge} onLeave={clearMessages} />
      <Route path='/blog/:category_slug/p/:paged' component={Posts} onEnter={checkAge} onLeave={clearMessages} />

      <Route path='/blog/p/:paged' component={Posts} />
      <Route path='/blog' component={Posts} />

      <Route path='/lesson/:id/:slug' onEnter={ensureAuthenticated} component={Lesson} />
      <Route path='/quiz/:id/:slug' onEnter={ensureAuthenticated} component={Quiz} />

      <Route path='/courses/:subject_slug' component={CourseList} />
      <Route path='/courses' component={CourseList} />


      <Route path='/courses/:subject_slug/p/:paged' component={CourseList} />
      <Route path='/courses/p/:paged' component={CourseList} />
      <Route path='/courses/' component={CourseList} />

      <Route path='/course/:id/:slug' component={CourseSingle} />

      <Route path = "/p/:slug" component={CMSPage} />
      <Route path = "*" component={NotFound} onLeave={clearMessages}/>
    </Route>
  );
}
