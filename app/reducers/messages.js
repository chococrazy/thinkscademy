export default function messages(state = {}, action) {
  switch (action.type) {
    case 'DOWNGRADE_FAILURE':
    case 'PRICING_FAILURE':
      return {
        error: action.messages,
        status: action.status
      };
    case 'DOWNGRADE_SUCCESS':
    case 'PRICING_SUCCESS':
      return {
        success: action.messages,
        status: action.status
      };
    case 'LOGIN_FAILURE':
    case 'SIGNUP_FAILURE':
    case 'UPDATE_PROFILE_FAILURE':
    case 'CHANGE_PASSWORD_FAILURE':
    case 'FORGOT_PASSWORD_FAILURE':
    case 'RESET_PASSWORD_FAILURE':
    case 'CONTACT_FORM_FAILURE':
    case 'OAUTH_FAILURE':
    case 'UNLINK_FAILURE':
    case 'LINK_FAILURE':
    case 'ADD_COURSE_FAILURE':
    case 'ADD_LESSON_FAILURE':
    case 'EDIT_LESSON_FAILURE':
    case 'EDIT_COURSE_FAILURE':
      return {
        error: action.messages
      };
    case 'ADD_COURSE_SUCCESS':
    case 'ADD_LESSON_SUCCESS':
    case 'EDIT_COURSE_SUCCESS':
    case 'EDIT_LESSON_SUCCESS':
    case 'UPDATE_PROFILE_SUCCESS':
    case 'CHANGE_PASSWORD_SUCCESS':
    case 'RESET_PASSWORD_SUCCESS':
    case 'CONTACT_FORM_SUCCESS':
    case 'SIGNUP_SUCCESS':
      return {
        success: action.messages
      };
    case 'FORGOT_PASSWORD_SUCCESS':
    case 'DELETE_ACCOUNT_SUCCESS':
    case 'UNLINK_SUCCESS':
      return {
        info: action.messages
      };
    case 'CLEAR_MESSAGES':
      return {};
    default:
      return state;
  }
}
