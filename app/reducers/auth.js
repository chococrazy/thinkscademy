const initialState = {
  token: null,
  user: {
    
  },
  progress: {
    
  }
};

export default function auth(state = initialState, action) {
  if (!state.hydrated) {
    state = Object.assign({}, initialState, state, { hydrated: true });
  }
  switch (action.type) {
    case 'LOGIN_SUCCESS':
    case 'SIGNUP_SUCCESS':
    case 'OAUTH_SUCCESS':
      return Object.assign({}, state, {
        token: action.token,
        user: action.user, 
        progress : action.progress
      });
      break;
    case 'PROFILE_UPDATE':
      return Object.assign({}, state, {
        user: action.user
      });
    case 'UPDATE_PROGRESS':
      return Object.assign({}, state, {
        progress: action.progress
      });

    case 'LOGOUT_SUCCESS':
      return initialState;
    default:
      return state;
  }
}
