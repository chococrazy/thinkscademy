import { combineReducers } from 'redux';
import messages from './messages';
import auth from './auth';
import settings from './settings';
import subjects from './subjects';

export default combineReducers({
  messages,
  auth,
  settings,
  subjects
});
