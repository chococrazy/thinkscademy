import React from 'react';
import { connect } from 'react-redux'
// import { resetPassword } from '../../actions/auth';
// import Messages from 'Messages';

import Header from './Header';
import Footer from './Footer';
import NotFound from './NotFound'


class CMSPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { page_loaded : false, is_page:true,title:'', content: ''  };
  }

  componentWillUnmount() {
    this.serverRequest.abort();
  }

  componentDidMount(){
    this.serverRequest = $.get('/cms_pages/slug/'+this.props.params.slug, function (result) {
  //    console.log(result);
      if(result.ok){
        var obj = {
          is_page   : true,
          is_loaded : true,
          title     : result.cms_page.title,
          content   : result.cms_page.content
        }
      }else{
        var obj = {
          is_page   : false,
          is_loaded : true,
        }
      }
      this.setState(obj);
    }.bind(this));
  }

  render() {
    if(this.state.is_loaded && !this.state.is_page)
    {
      return (
        <NotFound />
      )
    }
    return (
      <div className="wrapp-content">
        <Header title={this.state.title} />
        <main className="content-row">
          <div className="content-box-01 padding-top-100 padding-bottom-100">
            <div className="container">
              <div className="row">
                <div className="col-sm-12 col-md-12 col-lg-12">
                  <div dangerouslySetInnerHTML={{__html: this.state.content}}></div>
                </div>
              </div>
            </div>
          </div>
        </main>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return state;
};

export default connect(mapStateToProps)(CMSPage);
