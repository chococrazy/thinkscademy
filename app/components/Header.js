import React from 'react';
import { IndexLink, Link } from 'react-router';
import { connect } from 'react-redux'
import { logout } from '../actions/auth';

class Header extends React.Component {
  handleLogout(event) {
    event.preventDefault();
    this.setState({logout_clicked :  true});
    this.props.dispatch(logout());
  }
  constructor(props) {
    super(props);
    this.state = { categories : [] , albumcategories : [], logout_clicked: false};
  }
  componentWillReceiveProps(newProps){
    if(newProps.token !=this.props.token){
      this.setState({logout_clicked:false});
    }
  }

  componentWillUnmount() {
    // this.serverRequest && this.serverRequest.abort();
    // this.serverRequest && this.serverRequest2.abort();
  }

  runQuery(){
    return;
    this.serverRequest = $.get('/api/videos/categories/filter', function (result) {
      // console.log(result);
            if(result.length){
        var obj = {
          categories   : result
        }
        this.setState(obj);
      }


      this.serverRequest2 = $.get('/api/images/categories/filter', function (result2) {

        if(result2.length){
          var obj2 = {
            albumcategories   : result2
          }
          setTimeout(()=>{
            this.setState(obj2);
          }, 1000)
        }
      }.bind(this));

    }.bind(this));
  }
  componentDidMount(){
    this.runQuery();
  }
  componentWillReceiveProps(newProps)
  {
    this.runQuery();


  }

  render() {
    return (
      <header className={"wrapp-header "+(this.props.padding_less ? "padding_less" : "")} >
        <div className="main-nav">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <Link to="/" className="logo">
                  <img src="/img/logo_white.png" alt="" />
                </Link>
                <div className="main-nav__btn">
                  <div className="icon-left"></div>
                  <div className="icon-right"></div>
                </div>
                <ul className="main-nav__list">
                  <li>
                    <Link to='/'>Home</Link>
                  </li>
                  <li>
                    <Link to='/about'>About Us</Link>
                  </li>
                  <li>
                    <Link to='/courses'>Course List</Link>
                    <ul>
                    {
                      this.props.subjects['0'].map((o, i)=>{
                        return (

                          <li  key={i}>
                            <Link to={'/courses/'+o.title}>{o.title}</Link>
                          </li>
                        )
                      })
                    }


                    </ul>
                  </li>
                  <li>
                    <Link to='/blog'>Blog</Link>
                  </li>
                  <li>
                    <Link to='/contact'>Contact Us</Link>
                  </li>
                  <li>
                    <Link to='/plans'>Plans</Link>
                  </li>

                  {
                    this.props.token ? (
 
                        ((this.props.user.role == 'teacher') ? (
                          <li >
                            <Link to="/dashboard" className="menu_bttn">Dashboard</Link>
                          </li>
                        ) : (
                          <li>
                            <Link to="/account" className="menu_bttn">My Account</Link>
                          </li>
                        ))
 
                    ) : (
                      <li>
                        <Link to="/login" className="menu_bttn">Log in</Link>
                      </li>
                    )
                  }

                  {
                    this.props.token ? (
                      <li>
                        <a href="#" onClick={this.handleLogout.bind(this)} className="menu_bttn">Logout</a>
                      </li>
                    ) : (
                      <li>
                        <Link to="/signup" className="menu_bttn">Sign up</Link>
                      </li>
                    )
                  }
                </ul>
              </div>
            </div>
            {
              (this.props.title) ?
              (
                <div className="row">
                  <div className="col-lg-12 text-center">
                    <div className="page_titles"><h2>{this.props.title}</h2></div>
                  </div>
                </div>
              ) : false
            }

          </div>
        </div>
      </header>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    user: state.auth.user,
    subjects: state.subjects
  };
};

export default connect(mapStateToProps)(Header);
