import React from 'react';

import Header from './Header';
import Footer from './Footer';


const NotFound = (props) => {
  return (
    <div className="wrapp-content">
      <Header title="404 Not Found" />
      <main className="content-row">
  <div className="content-box-01 padding-top-100 padding-bottom-100">
    <div className="container">
      <div className="row">
        <div className="col-sm-12 col-md-12 col-lg-12">
        <p>The page you are looking for does no exist.
        </p>

        </div>

      </div>
    </div>
  </div>

</main>

      <Footer />
    </div>

  );
};

export default NotFound;
