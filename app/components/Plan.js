import React from 'react';
import { connect } from 'react-redux'
import { submitContactForm } from '../actions/contact';
import Messages from './Messages';


import { chargeChange} from '../actions/stripe';


import Header from './Header';
import Footer from './Footer';
import PlanSingle from './PlanSingle';
class Contact extends React.Component {


  constructor(props) {
    super(props);
    this.state = {
      plans : [ ],
      user     : this.props.user
    };
  }

  doPlan(id, cbid)
  {
    event.preventDefault();
    window.location.href= '/buy/'+id+'/'+cbid;
  }
  doPlanChange(id, cbid, event)
  {
    // console.log(event.target)
    if (event && event.target)
    {
      jQuery(event.target).html('please wait..').attr('disabled', 'disabled');
    }

    event.preventDefault();
    // return;
    // window.location.href= '/buy/'+id+'/'+cbid;
    this.props.dispatch(
        chargeChange(
           id,
          this.props.token, () => {
            this.setState({ clicked: false });
          }
        )
      );
      
  }

  componentWillUnmount() {
    this.serverRequest.abort();
  }

  componentDidMount(){
    this.runQuery(this.props);
  } 
  qryStr = [];

  runQuery(prps){
    this.serverRequest = $.get('/plan/list', function (result) {
      if(result.ok){
        var obj = {
          plans : result.plans,
          user  : this.props.user
        }

      }else{
        var obj = {
          plans : [],
          user  : this.props.user
        }
      }
      this.setState(obj);
    }.bind(this));
  }

  render() {
    return (
      <div className="wrapp-content">
        <Header padding_less={true} />
        <main className="content-row">
          <div className="content-box-01 single-post">
            <div className="container">
              <div className="row">
                <div className="content-box-01 padding-top-60 padding-bottom-60">
                  <div className="container">
                    <div className="row">
                      <div className="col-lg-12 text-left">
                        <h3 className="title-02 font-less">
                        {this.props.settings.plans_heading}
                        </h3>
                        <p className="subtitle-01">{this.props.settings.plans_sub_heading}</p>
                      </div>
                    </div>


                    <Messages messages={this.props.messages} />

                    <div className="row">
                      <div className="col-md-5">
                        <div className="top_blank_space"></div>
                        <div className="plan_main_text">Price</div>
                        <div className="plan_main_text">Duration</div>
                        <div className="plan_main_text">Levels Allowed</div>
                      </div>
                      <div className="col-md-7">
                        <div className="row plan_head">
                          {
                          this.state.plans.map((plan, i)=>{
                          return( <PlanSingle u={this.state.user} isLast={((i+1) == this.state.plans.length)} doPlanChange={this.doPlanChange.bind(this)} doPlan={this.doPlan.bind(this)} plan={plan} key={plan.id} /> )
                          })
                          }
                        </div>
                        <p><br /><small>In case of downgrade, the balance will be prorated and will be adjusted as a discount in next billing cycle.</small></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    messages: state.messages,
    settings: state.settings,
    token: state.auth.token,
    user: state.auth.user,
  };
};

export default connect(mapStateToProps)(Contact);
