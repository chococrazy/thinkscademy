import React from 'react';
import { connect } from 'react-redux'
import { submitContactForm } from '../actions/contact';
import Messages from './Messages';
import { IndexLink, Link } from 'react-router';

import Header from './Header';
import Footer from './Footer';
class Contact extends React.Component {
  constructor(props) {
    super(props);
    this.state = { name: '', email: '', message: '', last_name : '' };
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.dispatch(submitContactForm(this.state.name, this.state.last_name, this.state.email, this.state.message));
  }

  render() {
    return (
      <div className="wrapp-content">
        <Header title="Contact Us" />
        <main className="content-row">
          <div className="content-box-01 padding-top-100 padding-bottom-100 padding-lg-top-100">
            <div className="container">
              <div className="row">
                <div className="col-sm-6 col-md-6 col-lg-6">
                  <div className="contact-box">
                    <p>{this.props.settings.contact__text}</p>
                    <ul className="contact-box__list">
                      <li>
                        <span>Adress: </span>
                        {this.props.settings.address}
                      </li>
                      <li>
                        <span>Phones: </span>
                        {this.props.settings.phone}
                      </li>
                      <li>
                        <span>Email: </span>
                        <Link className="widget-contact__text-email" to={"mailto:"+this.props.settings.email}>{this.props.settings.email}</Link>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="col-sm-6 col-md-6 col-lg-6">
                  <div className="reply-form">
                    <div role="form" className="form-in-wrapp">
                      <div id="note">
                        <Messages messages={this.props.messages}/>
                      </div>
                      <div id="fields">
                        <form onSubmit={this.handleSubmit.bind(this)} novalidate="novalidate" className="reply-form__form">
                          <div className="reply-form__box-01 name">
                            <input value={this.state.name} onChange={this.handleChange.bind(this)} type="text" name="name" required="required" data-error="Firstname is required." className="reply-form__name form-in-wrapp-validates-as-required"  aria-required="true" aria-invalid="false" placeholder=" First Name " />
                          </div>
                          <div className="reply-form__box-01 name padding-left">
                            <input className="reply-form__name form-in-wrapp-validates-as-required" type="text" name="last_name" aria-required="true" aria-invalid="false" placeholder="Last Name" value={this.state.last_name} onChange={this.handleChange.bind(this)} />
                          </div>
                          <div className="fild_full your-email">
                            <input value={this.state.email} onChange={this.handleChange.bind(this)} className="reply-form__email form-in-wrapp-validates-as-required form-in-wrapp-validates-as-email" type="email" name="email" aria-required="true" aria-invalid="false" placeholder="Email *" />
                          </div>
                          <div className="reply-form__box-04 your-message">
                            <textarea value={this.state.message} onChange={this.handleChange.bind(this)} className="reply-form__message" name="message" aria-invalid="false" cols="30" rows="10" placeholder="Message..."></textarea>
                          </div>
                          <div className="reply-form__box-05">
                            <button className="btn-01" type="submit">Submit</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    messages: state.messages,
    settings: state.settings
  };
};

export default connect(mapStateToProps)(Contact);
