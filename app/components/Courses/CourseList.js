import React from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router';
import ReactPaginate from 'react-paginate';
import Header from '../Header';
import Footer from '../Footer';
import { browserHistory } from 'react-router'




class CourseList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      is_loaded: false,
      total_items : 0,
      total_pages: 0,
      current_page : 1,
      items: []
    };
  }


  componentWillUnmount() {
    this.serverRequest && this.serverRequest.abort();
  }

  componentDidMount() {
    this.runQuery(this.props);
  }

  componentWillReceiveProps(newProps) {
    this.runQuery(newProps);
  }

  qryStr = [];

  runQuery(prps) {
    var qry = [];
    this.qryStr = [];
    if (prps.params.subject_slug) {
      qry.push('subject=' + prps.params.subject_slug)
      this.qryStr.push(prps.params.subject_slug);
    }
    if (prps.params.paged) {
      qry.push('paged=' + prps.params.paged)
      this.qryStr.push(prps.params.paged);
    }

    qry = qry.join('&');
    this.serverRequest = $.get('/api/courses/listPaged?' + qry, function (result) {
      if (result.ok) {
        var obj = {
          items: result.items,
          is_loaded: true,
          total_items: result.pagination.rowCount,
          total_pages: result.pagination.pageCount,
          current_page: result.pagination.page
        }

      } else {
        var obj = {
          items: [],
          is_loaded: true,
          total_items: 0,
          total_pages: 0,
          current_page: 1,
        }
      }
      this.setState(obj);


      setTimeout(function () {
        if ($('.owl-option-02').size() > 0) {
          $('.owl-option-02').owlCarousel({
            loop: true,
            margin: 10,
            responsiveClass: true,
            dots: false,
            nav: true,
            responsive: {
              0: {
                items: 2
              },
              360: {
                items: 3
              },
              480: {
                items: 4
              },
              767: {
                items: 6
              },
              1024: {
                items: 8
              },
              1200: {
                items: 10
              }
            }
          });
        }
      }, 200);
    }.bind(this));
  }


  handlePageClick(i)
  {
  //  console.log(i)
    let page = i.selected + 1;
    var cat = this.props.params.subject_slug;
    if(cat)
    {
      browserHistory.push('/courses/' + cat + '/p/' + page);
    } else {
      browserHistory.push('/courses/p/' + page);
    }
  }


  render() {
    return (
      <div className="wrapp-content">
        <Header padding_less={true} />
        <main className="content-row">
          <div className="content-box-01 padding-top-40 padding-bottom-40 all_Cate_list">
            <div className="container-fluid">
              <div className="row owl_full">
                <div className="col-lg-12 text-center">
                  <div className="owl-carousel owl-option-02">
                    <Link to={'/courses/'} className={"cate_box" + ((!this.props.params.subject_slug ) ? ' active ' : '')}>
                      <div className="cate_title">
                        <i className="is_sub fas fa-chalkboard-teacher"></i>
                      </div>
                      <div className="cate_sub_head">All Subjects</div>
                    </Link>
                    {
                      this.props.subjects['0'].map((o, i) => {
                        return (
                          <Link to={'/courses/' + o.title} className={"cate_box" + ((this.props.params.subject_slug == o.title) ? ' active ' : '') }  key = { i }>
                            <div className="cate_title">
                              <i className={"is_sub " + o.icon}></i>
                            </div>
                            <div className="cate_sub_head">{o.title}</div>
                          </Link>
                        )
                      })
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="content-box-01 padding-top-40 padding-bottom-40">
            <div className="container">
              {this.state.is_loaded && this.state.items.length  < 1 ? (
                <p className='text-center'>No items matching your criteria</p>
              ): false}
              {this.state.items.map((item, i)=>{
                return (
                  <div className="row row_lines" key={i}>
                    <div className="col-md-3">
                      <div className="video_thumnail">
                        {
                          (item.image_url && item.image_url!='' && item.image_url!='/img/imgs/placeholder.png') ?
                            (<img src={'/downloads'+item.image_url} className="img-responsive" />) :
                            (<img src="/img/imgs/placeholder.png" className="img-responsive" />)
                        }
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="video_details">
                        <div className="vidoe_title">
                          <h2>
                            <Link to={"/course/"+item.id+'/'+convertToSlug(item.title)}>
                              {item.title}
                            </Link>
                          </h2>
                        </div>
                          {
                            (item.author) ? (
                              <div className="author_video">
                                Created by <Link className="authorName" to={"/courses/author/" + item.author_id}>{item.author.first_name} {item.author.last_name}</Link>
                              </div>
                            ) : false
                          }
                        <div className="video_text">
                          <p>
                            {item.description.substr(0, 255)}{'...'}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="duration_details">
                        <ul className="video_dur">
                          <li>
                            {/* {'LESSON_COUNT'}  */}
                            {item.lessons.length} lessons
                          </li>
                          <li>
                            <i className="fas fa-eye is_orange"></i> {item.views} views
                          </li>
                        </ul>

                        {/* <div className="red_play_icon">
                          <a href="#">
                            <img src="img/red_play_icon.png" />
                          </a>
                        </div> */}
                      </div>
                    </div>
                  </div>
                )
              })
              }

            </div>
          </div>

          <div className="row">
            <div className="col-lg-12">
              <div className="courses-pagination">




                {this.state.is_loaded  && this.state.items.length ? (
                  <ReactPaginate previousLabel={"<"}
                    nextLabel={">"}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    initialPage={this.state.current_page - 1}
                    disableInitialCallback={true}
                    pageCount={this.state.total_pages}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={(i) => { this.handlePageClick(i) }}
                    containerClassName={"pagination-list"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"} />
                ):false}



                {/* <ul className="pagination-list">
                  <li className="active">
                    <a href="#">1</a>
                  </li>
                  <li>
                    <a href="#">2</a>
                  </li>
                  <li>
                    <a href="#">3</a>
                  </li>
                  <li>
                    <a href="#">next</a>
                  </li>
                </ul> */}
              </div>
            </div>
          </div>

        </main>

        <Footer />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    user: state.auth.user,
    subjects: state.subjects
  };
};

export default connect(mapStateToProps)(CourseList);
