import React from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router';
import ReactPaginate from 'react-paginate';
import Header from '../Header';
import Footer from '../Footer';
import { browserHistory } from 'react-router'


class CourseBoxes extends React.Component {

  render() {
    const lnk = (typeof window !== 'undefined' ? window.location.href : '');
    return (
      <ul className="product-list">
        {this.props.data.map((item, i) => {
          let image = item.image_url;
          return(
          <li className="col-md-4" key={item.id}>
            <div className="single_course_video">
              <div className="video_course_box_p">
                <div className="course_image">
                  {
                    (image && image != '' && image!='img/imgs/placeholder.png'  && image!='/img/imgs/placeholder.png') ?
                      (<img src={'/downloads'+image} className="img-responsive" />) :
                      (<img src="/img/imgs/placeholder.png" className="img-responsive" />)
                  }
                </div>
                <div className="vedio_catpion">
                  <div className="video_by_p">By < Link className="authorName" to={"/courses/author/" + item.author_id} > {item.author.first_name} {item.author.last_name}</Link ></div>
                  <div className="profile_Vidoe_title"><Link to={"/course/" + item.id + '/' + convertToSlug(item.title)}>{item.title} </Link></div>
                </div>
                <div className="video_bottom">
                  <div className="video_left">
                    <ul className="video_dur_p">
                      <li>
                          {/* {'LESSON_COUNT'} */}
                          {item.lessons.length} lessons
                                </li>
                      <li>
                        <i className="fas fa-eye is_orange"></i> {item.views} views
                                </li>
                    </ul>
                  </div>
                  {/* <div className="video_icons">
                    <a href="#"><img src="img/profile_p_icon.png" /></a>
                  </div> */}
                </div>
              </div>
            </div>
            </li>
          )
        })}
      </ul>
    );
  }


}


export default CourseBoxes;
