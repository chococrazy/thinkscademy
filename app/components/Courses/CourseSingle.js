import React from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router';
import ReactPaginate from 'react-paginate';
import Header from '../Header';
import Footer from '../Footer';
import { browserHistory } from 'react-router'
import CourseBoxes from './CourseBoxes'



class CourseSingle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      is_loaded: false,
      item: {lessons : []},

      similar : []
    };
  }


  componentWillUnmount() {
    this.serverRequest && this.serverRequest.abort();
  }

  componentDidMount() {
    this.runQuery(this.props);
  }

  componentWillReceiveProps(newProps) {
    this.runQuery(newProps);
  }

  runQuery(prps) {
    if (this.state.item.id == prps.params.id)
    {
      return;
    }
    this.serverRequest = $.get('/api/courses/single/' + prps.params.id +'?do_add_view=yes', function (result) {
      if (result.ok) {
        var obj = {
          item: result.item,
          is_loaded: true,
        }

      } else {
        var obj = {
          item: {},
          is_loaded: true,
        }
      }
      this.setState(obj, () => {
        this.serverRequest2 = $.get('/api/courses/listSameAuthor?author_id='+obj.item.author_id, function (result) {
          // console.log(result);
          if (result.ok) {
            var obj = {
              similar: result.items,
            }
          } else {
            var obj = {
              similar: [],
            }
          }
          this.setState(obj);
        }.bind(this));
      });
    }.bind(this));
  }


  render() {
    let item = this.state.item;
    const lnk = (typeof window !== 'undefined' ? window.location.href : '');

    let progress = this.props.progress;
    if (!progress || !progress.course_progress)
    {
      progress = 1;
    } else {
      progress = progress.course_progress;
      progress = (typeof progress[this.state.item.id] === 'undefined') ? 1 : (
        parseInt(progress[this.state.item.id]) + 1
      );
    }

  //  console.log(progress);
    return (
      <div className="wrapp-content">
        <Header padding_less={true} />
        <main className="content-row">
          <div className="content-box-01 padding-top-40 padding-bottom-40 all_Cate_list">
            <div className="container">
              <div className="row">
                <div className="col-md-7 padding_remove">
                  <div className="profile_name">
                    {this.state.item.title}
                  </div>
                  <div className="profile_tagline">
                    {/* {Internet Advertising Trends You Won To Be Disappointed} */}
                  </div>
                  {
                    (this.state.item.author) ? (
                      <div className="single_course_created_by">
                        Created by <Link className="authorName"  to={"/courses/author/" + this.state.item.author_id}>{this.state.item.author.first_name} {this.state.item.author.last_name}</Link>
                        {/* <i className="fa fa-star" aria-hidden="true"></i> */}
                      </div>
                    ):false
                  }
                  <ul className="video_dur_single">
                    <li>
                      <img src="/img/gray_article.png" />
                      {item.lessons.length} Lessons
                    </li>
                    <li>
                      <i className="fas fa-eye is_orange"></i> {item.views} views
                    </li>
                  </ul>

                  <div className="share_box">
                    <span>Share Via: </span>
                    <a target="_blank" href={"https://www.linkedin.com/shareArticle?mini=true&url=" + lnk + '&title=&summary=&source='}><i className="fab fa-linkedin link_color"></i></a>
                    <a target="_blank" href={"whatsapp://send?text=" + lnk}>
                      <i className="fab fa-whatsapp wh_color"></i>
                    </a>
                    <a target="_blank" href={"https://twitter.com/home?status=" + lnk}><i className="fab fa-twitter twiter_color"></i></a>
                    <a target="_blank" href={"https://www.facebook.com/sharer/sharer.php?u=" + lnk}><i className="fab fa-facebook-square fb_color"></i></a>
                  </div>
                </div>
                <div className="col-md-5 padding-5-less">
                  <div className="single_course_video">
                    <div className="video_course_box_p single_post_video">
                      <div className="course_image">
                        {
                          (item.image_url && item.image_url!='' && item.image_url!='/img/imgs/placeholder.png') ?
                            (<img src={'/downloads'+item.image_url} className="img-responsive" />) :
                            (<img src="/img/imgs/placeholder.png" className="img-responsive" />)
                        }
                      </div>
                      <div className="vedio_catpion">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="content-box-01 padding-top-50 padding-bottom-50 gray_bg_profile">
            <div className="container">
              <div className="row">
                <div className="col-md-6">
                  <div className="vidoe_title small-title">
                    <h2>What Will I Learn?</h2>
                  </div>
                  <div className="will_list">
                    <ul>
                      {
                        item.learnings && item.learnings.split(`\n`).map((req, i) => {
                          return (
                            <li key={i}><i className="fa fa-check" aria-hidden="true"></i> {req}</li>
                          )
                        })
                      }
                    </ul>
                  </div>
                  <div className="vidoe_title small-title">
                    <h2>Requirments</h2>
                  </div>
                  <div className="will_list">
                    <ul>
                      {
                        item.requirements && item.requirements.split(`\n`).map((req, i) => {
                          return (
                            <li key={i}><i className="fa fa-circle" aria-hidden="true"></i> {req}</li>
                          )
                        })
                      }
                    </ul>
                  </div>
                  <div className="vidoe_title small-title">
                    <h2>Description</h2>
                  </div>
                  <div className="single_text">
                    <p>
                      {this.state.item.description}
                    </p>
                  </div>
                  <div className="single_comments">
                  </div>
                </div>
                <div className="col-md-6">
                  {
                    item.lessons && item.lessons.map((l, i) => {
                      let allowed = false;
                      let url;
                      let progress_issue;
                      let login_url;
                      let is_logged_in = (this.props.user) ? true : false;
                      if (is_logged_in) {
                        if (this.props.user.max_levels_allowed >= l.order) {

                          if (progress >= l.order) {
                            console.log('here')
                            allowed = true;
                            url = '/lesson/'+ l.id+'/'+convertToSlug(l.title)
                          } else {
                            allowed = false;
                            progress_issue = true;
                            url = '#'
                            // complete other levels first
                          }
                        } else {
                          url = '/plans'
                        }
                        // prev completed
                      } else {
                        login_url  = '/login'

                      }
                      // console.log(progress)
                      let not_allowed_msg = (progress_issue) ? 'Complete Previous Lessons first' : 'Upgrade Membership to see lesson'
                      var not_allowed_title = (allowed) ? false : (
                        <div className="overlap error" >
                          {not_allowed_msg}
                        </div>
                      )
                      return (
                        <Link  onClick={(e)=>{if(progress_issue) e.preventDefault(); }} to={"" + ((is_logged_in) ? url : login_url)} key={l.id} className={" is_lesson_link " + ((is_logged_in) ? "" : " is_not_logged_in ") + ((allowed) ? " allowed " : " not allowed") } >
                          {
                            (is_logged_in) ? not_allowed_title :
                              (
                                <div className="overlap error" >
                                login to see the lesson
                                </div>
                              )
                          }
                          <div className="lesstions_box" >
                            <div className="lesstion_pic">
                              <i className="fas fa-book-open"></i>
                            </div>
                            <div className="lession_descprtion">
                              <div className="lesstion_title">
                                {l.order}. {l.title}
                              </div>
                              <div className="lesstion_subtitle">
                                {l.description.replace(/<(?:.|\n)*?>/gm, '').split('&nbsp;').join(' ').substr(0,50)}...
                              </div>
                            </div>
                          </div>
                        </Link>
                      )
                    })
                  }
                </div>
              </div>
            </div>
          </div>


          <div className="content-box-01 padding-top-40 padding-bottom-40">


            <div className="container">

              <div className="row">

                <div className="col-md-12">
                  <div className="vidoe_title small-title">
                    <h2>More Courses by {item.author && item.author.first_name} {item.author && item.author.last_name}</h2>
                  </div>
                </div>
                <CourseBoxes data={this.state.similar} />


              </div>
            </div>
          </div>


        </main>
        <Footer />
      </div>
    );
  }


}



const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    user: state.auth.user,
    progress: state.auth.progress,
    // subjects: state.subjects
  };
};

export default connect(mapStateToProps)(CourseSingle);
