import React       from 'react';
import { Link }    from 'react-router';
import { connect } from 'react-redux'
import { login }   from '../../actions/auth';
import Messages    from '../Messages';
import Header from '../Header';
import Footer from '../Footer';


class Login extends React.Component {
  constructor(props) {
    super(props);

    if( this.props.params.successVerify =='successVerify')
    {
      this.state = { email: '', password: '', show_success: true };
    }else{
      this.state = { email: '', password: '' };
    }
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleLogin(event) {
    event.preventDefault();
    this.props.dispatch(login(this.state.email, this.state.password));
    this.setState({show_success: false});
  }


  render() {
    return (
      <div className="wrapp-content">
        <Header title="Login Your Account" />
        <main className="content-row">
          <div className="content-box-01 padding-top-93">
            <div className="container">
              <div className="row">
                <div className="col-lg-12 text-center">
                  <div className="login-box">

                    <form className="mgm_form" name="loginform" id="loginform" onSubmit={this.handleLogin.bind(this)}>
                      <div className="box_main_heading">
                        <h3>Sign In</h3>
                      </div>
                      {
                        this.state.show_success ? (
                          <div role="alert" className="alert alert-success">
                            Account Approved successfully.
                          </div>
                        ):false
                      }
                      <Messages messages={this.props.messages}/>
                      <div className="form-group">
                        <input placeholder="Email" value={this.state.email} onChange={this.handleChange.bind(this)} autoFocus type="text" name="email" id="user_login" className="input form-control"   />
                      </div>
                      <div className="form-group">
                        <input placeholder="Password" type="password" name="password" id="user_pass" className="input form-control" value={this.state.password} onChange={this.handleChange.bind(this)} />
                      </div>
                      <button type="submit" className="log-btn" >Log In</button>
                      <div className="bottom_links_login">
                        <Link id="lost-password" href="/forgot">Lost your password?</Link>
                        <Link id="sign-up" to="/signup">Sign up</Link>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    messages: state.messages
  };
};

export default connect(mapStateToProps)(Login);
