import React from 'react';
import { connect } from 'react-redux'
// import { submitContactForm } from '../actions/contact';
import Messages from '../Messages';

import Header from '../Header';
import Footer from '../Footer';

import {charge} from '../../actions/stripe';



class Buy extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      step: 3, // need not change this
      billing: {
        name_on_card: ''
      }
    };
  }

  handleChange(event) {
    if (this.state.billing[event.target.name] === undefined) {
      this.setState({[event.target.name]: event.target.value});
    } else {
      const billing = this.state.billing;
      billing[event.target.name] = event.target.value;
      this.setState({billing: billing});
    }
  }

  componentDidMount(){
    this.handleCheckoutStep2()
  }

  componentDidReceiveProps(){
    this.handleCheckoutStep2()
  }

  handleCheckoutStep2(){
    let stripe;
    stripe = Stripe(this.props.settings['stripe_'+this.props.settings.stripe_mode+'_publishable_key'] );
    const elements = stripe.elements();
    const style = {
      base: {
        fontSize: '16px',
        border:'2px solid #ddd',
        lineHeight: '24px'
      },
      invalid: {
        color: '#e5424d',
        ':focus': {
          color: '#303238'
        }
      }
    };
    const card = elements.create('card', {
      style: style,
      hidePostalCode: true,
      iconStyle: 'solid'
    });
    // ignore the lint here
    this.setState({cardObj: card, stripe: stripe});
    setTimeout(this.mountCard.bind(this), 500);
  }


  mountCard() {
    if(jQuery('#cardElement').length){
      this.state.cardObj.mount('#cardElement');
      this.state.cardObj.on('change', event => this.setOutcome(event));
    }
  }

  setOutcome(result) {
    const successElement = document.querySelector('.outcome_success');
    const errorElement = document.querySelector('.outcome_error');
    successElement.classList.remove('visible');
    errorElement.classList.remove('visible');

    if (result.token) {
      // successElement.querySelector('.token').textContent = result.token.id;
      // successElement.classList.add('visible');
      this.props.dispatch(
        charge(
          {
            // group_id: this.props.group.id,
            tokenObj: result.token,
            plan_id: this.props.params.plan_id,
            // amount: DO_AMOUNT_HERE
          },
          this.props.token, () => {
            this.setState({ clicked: false });
          }
        )
      );
      // 
    } else if (result.error) {
      errorElement.textContent = result.error.message;
      errorElement.classList.add('visible');
      this.setState({clicked: false});
    } else {
      errorElement.textContent = '';
      this.setState({clicked: false});
      errorElement.classList.remove('visible');
    }
  }

  handleCheckoutStep3(event) {
    //const tmp_this = this;
    this.setState({clicked: true});
    event.preventDefault();

    const extraDetails = {
      name: this.state.billing.name_on_card 
    };
    this.state.stripe
      .createToken(this.state.cardObj, extraDetails)
      .then(this.setOutcome.bind(this));
  }
 

  render() {
      // console.log('--', this.props.messages);
    if (this.props.user.plan != 'free') {
      return (
        <div className="wrapp-content">
          <Header padding_less={true} />
          <main className="content-row">
            <div className="content-box-01 single-post">
              <div className="container">
                <div className="row">
                  <h3>Buy Plan - {this.props.params.plan_name}</h3>
                  <div className="card">
                    <p>You are already a paying customer.</p>
                  </div>
                  </div>
                </div>
              </div>
            </main>
            <Footer />
          </div>
        );
    }


      const btn_label = this.state.clicked
        ? 'please wait...'
        : 'Complete purchase';
      const step_3_payment_method =
        this.state.step != 3 ? (
          false
        ) : (
          <div className="card-block">
            <form onSubmit={this.handleCheckoutStep3.bind(this)}>

              <legend>Checkout</legend>
              <Messages messages={this.props.messages} />
              <div className="form-group row m-t-2">
                <label
                  htmlFor="name_on_card"
                  className="col-sm-3 form-control-label"
                >
                  Name On Card
                </label>
                <div className="col-sm-7">
                  <input
                    type="text"
                    required
                    autoFocus
                    name="name_on_card"
                    id="name_on_card"
                    className="form-control"
                    value={this.state.billing.name_on_card}
                    onChange={this.handleChange.bind(this)}
                  />
                </div>
              </div>
              <div className="form-group row">
                <label
                  htmlFor="card_number"
                  className="col-sm-3 form-control-label"
                >
                  Card
                </label>
                <div className="col-sm-7">
                  <div id="card-errors" role="alert" />
                  <div id="cardElement" />
                </div>
              </div>
              <div className="form-group row">
                <div className="outcome">
                  <div className="col-sm-3" />
                  <div className="col-sm-7">
                    <div className="outcome_error" role="alert" />
                    <div className="outcome_success">
                      <div className="token" />
                    </div>
                  </div>
                </div>
              </div>
              <hr />
              <div className="row">
                <div className="col-sm-12 text-xs-center">
                </div>
              </div>
              <div className="form-group row">
                <div className="col-md-12 text-center">
                  <button
                    type="submit"
                    disabled={this.state.clicked}
                    className="btn-01"
                  >
                    {btn_label}
                  </button>
                </div>
              </div>
            </form>
          </div>
        );

      return (

        <div className="wrapp-content">
          <Header padding_less={true} />
          <main className="content-row">
            <div className="content-box-01 single-post">
              <div className="container">
                <div className="row">
                  <h3>Buy Plan - {this.props.params.plan_name}</h3>

                  <div className="card">
                    {step_3_payment_method}
                  </div>
                  </div>
                </div>
              </div>
            </main>
            <Footer />
          </div>
      );
    }



}

const mapStateToProps = (state) => {
  return {
    messages: state.messages,
    settings: state.settings,
    token: state.auth.token,
    user: state.auth.user,
  };
};

export default connect(mapStateToProps)(Buy);
