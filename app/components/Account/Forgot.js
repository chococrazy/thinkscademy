import React from 'react';
import { connect } from 'react-redux'
import { forgotPassword } from '../../actions/auth';
import Messages from '../Messages';
import Header from '../Header';
import Footer from '../Footer';

class Forgot extends React.Component {
  constructor(props) {
    super(props);
    this.state = { email: '' };
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleForgot(event) {
    event.preventDefault();
    this.props.dispatch(forgotPassword(this.state.email));
  }

  render() {
    return (
      <div className="wrapp-content">
        <Header title="Lost Password" />
        <main className="content-row">
          <div className="content-box-01 padding-top-93">
            <div className="container">
              <div className="row">
                <div className="col-lg-12 text-center">
                  <div className="login-box">

                    <form className="mgm_form" name="loginform" id="loginform" onSubmit={this.handleForgot.bind(this)}>
                      <div className="box_main_heading">
                        <h3>Lost Password</h3>
                      </div>

                      <Messages messages={this.props.messages}/>
                      Please enter your e-mail address. You will receive a password reset link via e-mail. <br /><br />
                      <div className="form-group">
                        <input type="text" name="email" id="email" className="input form-control" size="40"  value={this.state.email} onChange={this.handleChange.bind(this)} autoFocus />
                      </div>
                      <button type="submit" className="log-btn" >Reset Password</button>

                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
        <Footer />
      </div>


    );
  }
}

const mapStateToProps = (state) => {
  return {
    messages: state.messages
  };
};

export default connect(mapStateToProps)(Forgot);
