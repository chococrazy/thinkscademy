import React from 'react';
import { connect } from 'react-redux'
import { resetPassword } from '../../actions/auth';
import Messages from '../Messages';
import Header from '../Header';
import Footer from '../Footer';

class Reset extends React.Component {
  constructor(props) {
    super(props);
    this.state = { password: '', confirm: '' };
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleReset(event) {
    event.preventDefault();
    this.props.dispatch(resetPassword(this.state.password, this.state.confirm, this.props.params.token));
  }

  render() {
    return (
      <div className="wrapp-content">
        <Header title="Reset Password" />
        <main className="content-row">
          <div className="content-box-01 padding-top-93">
            <div className="container">
              <div className="row">
                <div className="col-lg-12 text-center">
                  <div className="login-box">

                    <form className="mgm_form" name="loginform" id="loginform" onSubmit={this.handleReset.bind(this)}>
                      <div className="box_main_heading">
                        <h3>Reset Password</h3>
                      </div>

                      <Messages messages={this.props.messages}/>
                      <div className="form-group">
                        <input placeholder="Password" onChange={this.handleChange.bind(this)} value={this.state.password} type="password" name="password" id="password" className="input form-control" />
                      </div>
                      <div className="form-group">
                        <input placeholder="Confirm Password" onChange={this.handleChange.bind(this)} value={this.state.confirm} type="password" name="confirm" id="confirm" className="input form-control" />
                      </div>
                      <button type="submit" className="log-btn" >Reset Password</button>

                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return state;
};

export default connect(mapStateToProps)(Reset);
