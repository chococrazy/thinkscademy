import React                              from 'react';
import { connect }                        from 'react-redux'
import { updateProfile , changePassword } from '../../actions/auth';
import { link          , unlink }         from '../../actions/oauth';
import Messages                           from '../Messages';
import { Link }                    from 'react-router';
import { logout }                         from '../../actions/auth';
var moment = require('moment');
import Header from '../Header';
import Footer from '../Footer';
class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email      : props.user.email,
      first_name : props.user.first_name ? props.user.first_name  :  "",
      last_name  : props.user.last_name ?  props.user.last_name : "",
      gravatar   : props.user.gravatar,
      password   : '',
      confirm    : '',
      logout_clicked : false,
      saved_user : props.user,
      invoices :[],
      is_loaded_invoices : false
    };
  }
  mm(s)
  {
    return moment.unix(s).format('llll');
  }

  handleLogout(event) {
    event.preventDefault();
    this.setState({logout_clicked :  true}, ()=>{
      this.props.dispatch(logout());
    });
  }

  componentWillReceiveProps(newProps)
  {
    this.setState({
      email      : newProps.user.email,
      first_name : newProps.user.first_name,
      last_name  : newProps.user.last_name,
      gravatar   : newProps.user.gravatar,
      saved_user : newProps.user
    });
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  componentDidMount(){
    this.runQuery()
  }

  handleProfileUpdate(event) {
    event.preventDefault();
    this.props.dispatch(updateProfile(this.state, this.props.token, ()=>{
      var t=  this.state.saved_user;
      t.first_name = this.state.first_name;
      t.last_name = this.state.last_name;
      // t.location = this.state.location;
      t.email = this.state.email;
      this.setState({
        saved_user  : t

      })
    }));

  }

  handleChangePassword(event) {
    event.preventDefault();
    this.props.dispatch(changePassword(this.state.password, this.state.confirm, this.props.token));
  }

  runQuery( ) {
    // alert();
    if(!this.props.user.stripe_customer_id)
    {
      this.setState({
        is_loaded_invoices: true
      })
      return;
    }
    this.serverRequest = $.get('/invoices_of/' + this.props.user.stripe_customer_id, function (result) {
      if (result.ok) {
        var obj = {
          invoices: result.items,
          is_loaded_invoices: true
        }

      } else {
        var obj = {
          invoices: [],
          is_loaded_invoices: true
        }
      }
      this.setState(obj);
    }.bind(this));
  }


  handleDeleteAccount(event) {
    event.preventDefault();
    this.props.dispatch(deleteAccount(this.props.token));
  }

  handleLink(provider) {
    this.props.dispatch(link(provider))
  }

  handleUnlink(provider) {
    this.props.dispatch(unlink(provider));
  }

  render() {
    // console.log(this.state.saved_user.first_name)
    const plan_details = (this.state.saved_user.plan != 'paid'  ) ?
    (
      <div className="alert alert-danger">You are using a free plan and are missing out of a lot of premium content. Click <Link to="/plans">here</Link> to buy a Membership Plan.</div>
    ):
    (
      <div className="alert alert-success">You are using a paid plan. See your plan details below.</div>
    )
    // console.log(this.state.saved_user );
    if(typeof this.state.saved_user != 'object')
    {
      var saved_user = JSON.stringify(this.state.saved_user);
      var saved_user = JSON.parse(saved_user);
      var saved_user = JSON.parse(saved_user);
    }else{
      var saved_user = this.state.saved_user;
    }
    if(!saved_user)
      saved_user = '';

    // console.log(saved_user.plan_description);
    if(!saved_user.plan_description)
      saved_user.plan_description = {plan_details : {}, plan_id : null, plan_started_on : ''};
    if(saved_user.plan_description && typeof saved_user.plan_description != 'object')
    {

       saved_user.plan_description = JSON.parse(saved_user.plan_description);
    }


    var pdd = (saved_user != '') ? saved_user.plan_description : {plan_details : {}, plan_id : null, plan_started_on : ''};
    if(!pdd)
    {
      pdd = {}

    }
    if(!pdd.plan_details)
    pdd.plan_details = {}


    var single = pdd.plan_details.description;

    if(single && single != '')
    {
      single = single.split("\n");
    }else{
      single = [];
    }
  var  plan_description = (this.state.saved_user.plan != 'paid') ? false : (
      <div>
      <p><strong>Plan Name :</strong> <span>{this.state.saved_user.plan_name}</span></p>
      <p><strong>Subscription Id :</strong> <span>{pdd.subscription_id}</span></p>
      <p><strong>Plan Started On :</strong> <span>{moment.unix(pdd.start).format('llll')}</span></p>
      <p><strong>Plan Cost/Duration :</strong> <span>${(pdd.subscription_obj.plan.amount / 100) + " / " + pdd.subscription_obj.plan.interval_count + ' ' + pdd.subscription_obj.plan.interval}</span></p>
        <br />
        <Link to="/p/cancellation-policy" >Cancellation Policy</Link>
      </div>
    )

    const inv_list = this.state.invoices.length
      ? (
        <table className='table table-striped'>
          <thead>
            <tr>
              <th>Inv Id</th>
              <th>Inv Amount</th>
              <th>Paid?</th>
              <th>Description</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody>
          {
            this.state.invoices.map((inv, i)=>{
              return (
                <tr key={i}>
                  <th>{inv.id}</th>
                  <th>${inv.amount_due/2}</th>
                  <th>{inv.paid ? 'PAID':"UN-PAID"}</th>
                  <th>{inv.lines.data[0].plan.nickname}</th>
                  <th>{this.mm(inv.date)}</th>
                </tr>
              )
            })
          }
          </tbody>

        </table>
      ):(
          <div className="alert alert-warning">No invoices found</div>
      )


    return (
      <div className="wrapp-content">
        <Header padding_less={true} />
        <main className="content-row">
          <div className="content-box-01 padding-top-40 padding-bottom-40">
            <div className="container">
            <div className="row">
              <div className="col-md-3 col-sm-3 col-xs-12 text-center">
                <div className="profile_pic">
                  <img className="editable img-responsive" src={this.state.saved_user.picture || this.state.saved_user.gravatar } />
                </div>
                <div className="user_name_p">
                  { (this.state.saved_user.first_name ? this.state.saved_user.first_name : "") +" "+(this.state.saved_user.last_name ? this.state.saved_user.last_name : "") }
                </div>
              </div>
              <div className="col-md-9 col-sm-9 col-xs-12">
                <ul className="nav nav-tabs">
                  <li className="active">
                    <a  href="#1" data-toggle="tab">Your Profile</a>
                  </li>
                  <li>
                    <a href="#2" data-toggle="tab">Profile Settings</a>
                  </li>
                  {
                    (this.props.user.role != 'teacher') ?
                    (
                      <li>
                        <a href="#3" data-toggle="tab">Plan Details</a>
                      </li>
                    ) : (
                      <li>
                        <Link to="/dashboard">Teacher Dashboard</Link>
                      </li>
                    )
                  }

                  {
                    (this.props.user.role != 'teacher') ?
                    (
                      <li>
                        <a href="#10" data-toggle="tab">Invoices</a>
                      </li>
                    ) : false
                  }


                  <li>
                  {
                    this.state.logout_clicked  ? (
                      <a href="#"  >please wait..</a>
                    ):
                    (
                      <a onClick={this.handleLogout.bind(this)} >Sign Out</a>
                    )
                  }


                  </li>
                </ul>
                <div className="tab-content ">
                  <div className="tab-pane active" id="1">
                    <div className="profile-user-info">
                      <div className="profile-info-row">
                        <div className="profile-info-name"> Username </div>
                        <div className="profile-info-value">
                          <span>
                            { (this.state.saved_user.first_name ? this.state.saved_user.first_name : "") +" "+(this.state.saved_user.last_name ? this.state.saved_user.last_name : "") }
                          </span>
                        </div>
                      </div>
                      <div className="profile-info-row">
                        <div className="profile-info-name"> Email </div>
                        <div className="profile-info-value">
                          <i className="fa fa-email light-orange bigger-110"></i>
                          <span> {this.state.saved_user.email}</span>
                        </div>
                      </div>
                      {/* <div className="profile-info-row">
                        <div className="profile-info-name"> Age </div>
                        <div className="profile-info-value">
                          <span>{this.state.saved_user.age}</span>
                        </div>
                      </div> */}
                      <div className="profile-info-row">
                        <div className="profile-info-name"> Joined </div>
                        <div className="profile-info-value">
                          <span>{moment(this.state.saved_user.created_at).format('llll')}</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="tab-pane" id="2">
                    <div className="edit_profile_box">
                      <Messages messages={this.props.messages}/>
                      <form onSubmit={this.handleProfileUpdate.bind(this)}>
                        <div className="form-group">
                          <label className="col-sm-2 control-label">First Name</label>
                          <div className="col-sm-4">
                            <input className="form-control" type="text" name="first_name" placeholder="First Name" value={this.state.first_name} onChange={this.handleChange.bind(this)}  />
                          </div>
                        </div>
                        <div className="form-group">
                          <label className="col-sm-2 control-label">Last Name</label>
                          <div className="col-sm-4">
                            <input className="form-control" type="text" name="last_name" placeholder="Last Name" value={this.state.last_name} onChange={this.handleChange.bind(this)}  />
                          </div>
                        </div>
                        <div className="form-group">
                          <label className="col-sm-2 control-label">Email</label>
                          <div className="col-sm-4">
                            <input className="form-control" type="text" name="email" placeholder="Email" value={this.state.email} onChange={this.handleChange.bind(this)} />
                          </div>
                        </div>

                        <div className="form-group">
                          <div className="col-sm-offset-2 col-sm-10">
                            <button type="submit" className="btn btn-primary">Update Profile</button>
                          </div>
                        </div>
                      </form>
                      <h4>Change Password</h4>
                      <form onSubmit={this.handleChangePassword.bind(this)}>
                        <div className="form-group">
                          <label className="col-sm-2 control-label">New Password</label>
                          <div className="col-sm-4">
                            <input className="form-control" type="password" name="password" id="password" value={this.state.password} onChange={this.handleChange.bind(this)} />
                          </div>
                        </div>
                        <div className="form-group">
                          <label className="col-sm-2 control-label">Confirm Password</label>
                          <div className="col-sm-4">
                            <input className="form-control" type="password" name="confirm" id="confirm" value={this.state.confirm} onChange={this.handleChange.bind(this)}/>
                          </div>
                        </div>
                        <div className="form-group">
                          <div className="col-sm-offset-2 col-sm-10">
                            <button className="btn btn-primary" >Change Password</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <div className="tab-pane" id="3">
                    <div className="plan_details">
                      {plan_details}
                      {plan_description}
                    </div>
                  </div>


                  <div className="tab-pane" id="10">

                  {
                    this.state.is_loaded_invoices?

                      inv_list
                     :false
                  }

                  </div>



                  <div className="tab-pane" id="4">
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
        </main>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    user: state.auth.user,
    messages: state.messages
  };
};

export default connect(mapStateToProps)(Profile);
