import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux'
import { signup } from '../../actions/auth';
import { facebookLogin, twitterLogin, googleLogin, vkLogin } from '../../actions/oauth';
import Messages from '../Messages';
import Header from '../Header';
import Footer from '../Footer';

class Signup extends React.Component {
  constructor(props) {
    super(props);
    this.state = { first_name: '', last_name : '', email: '', password: '', password_confirm:'', signup_as:'student' };
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleSignup(event) {
    event.preventDefault();
    var is_plan_selected = this.props.params.selectedPlan ? this.props.params.selectedPlan : false
    this.props.dispatch(signup(this.state.first_name, this.state.last_name, this.state.email, this.state.password, this.state.password_confirm, this.state.signup_as, is_plan_selected));
  }


  render() {
    return (
      <div className="wrapp-content">
        <Header title="Create Your Account" />
        <main className="content-row">
          <div className="content-box-01 padding-top-93">
            <div className="container">
              <div className="row">
                <div className="col-lg-12 text-center">
                  <div className="login-box">

                    <form className="mgm_form" name="loginform" id="loginform" onSubmit={this.handleSignup.bind(this)}>
                      <div className="box_main_heading">
                        <h3>Sign Up</h3>
                      </div>
                      <Messages messages={this.props.messages}/>
                      <div className="form-group">
                        <input placeholder="First Name" onChange={this.handleChange.bind(this)} value={this.state.first_name} type="text" name="first_name" id="first_name" className="input form-control" />
                      </div>
                      <div className="form-group">
                        <input placeholder="Last Name" onChange={this.handleChange.bind(this)} value={this.state.last_name} type="text" name="last_name" id="last_name" className="input form-control" />
                      </div>
                      <div className="form-group">
                        <input placeholder="Email" onChange={this.handleChange.bind(this)} value={this.state.email} type="text" name="email" id="email" className="input form-control" />
                      </div>
                      <div className="form-group">
                        <input placeholder="Password" onChange={this.handleChange.bind(this)} value={this.state.password} type="password" name="password" id="password" className="input form-control" />
                      </div>
                      <div className="form-group">
                        <input placeholder="Confirm Password" onChange={this.handleChange.bind(this)} value={this.state.password_confirm} type="password" name="password_confirm" id="password_confirm" className="input form-control" />
                      </div>
                      <div className="form-group signup_as clear">
                        <label className="pull-left">
                          <input  onChange={this.handleChange.bind(this)} type="checkbox" name="signup_as" value="student" checked={this.state.signup_as == 'student'} /> Student
                        </label>
                        <label className="pull-right">
                          <input onChange={this.handleChange.bind(this)}  type="checkbox" name="signup_as" value="teacher" checked={this.state.signup_as == 'teacher'} /> Teacher
                        </label>
                      </div>
                      {
                        (this.state.signup_as == 'teacher') ? (<p>Your account will be activated upon admin approval.</p>)  : false
                      }
                      <div className="form-group trems_user">
                        <input required name="" type="checkbox" value="yes" /> By signing up, you agree to our <a href="/p/terms">Terms of Use</a> and <a href="/p/policy" >Privacy Policy</a>.
                      </div>
                      <button type="submit" className="log-btn" >Sign Up</button>
                      <div className="bottom_links_login login_a">
                        Already have an account? <Link className="mgm-register-link" to="/login"><strong>Sign in</strong></Link><br /> <small>- or -</small><br /><Link className="mgm-lostpassword-link" to="/forgot" title=""><strong>Lost your password?</strong></Link>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    messages: state.messages
  };
};

export default connect(mapStateToProps)(Signup);
