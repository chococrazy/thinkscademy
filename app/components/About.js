import React from 'react';
import { connect } from 'react-redux'
import Messages from './Messages';

import { IndexLink, Link } from 'react-router';

import Header from './Header';
import Footer from './Footer';

class About extends React.Component {

  constructor(props) {
    super(props);
    this.state = {

    };
  }
  componentWillUnmount() {
    // this.serverRequest.abort();
  }

  componentDidMount(){
    this.runQuery(this.props);
  }

  componentWillReceiveProps(newProps)
  {
    this.runQuery(newProps);
  }

  runQuery(prps){
  
  }



  render() {
    // console.log(this.props.subjects);
    return (
      <div className="wrapp-content">
        <Header title="About Us" />
        <main className="content-row">

        <div className="content-box-01 padding-top-100 padding-lg-top-100">
				<div className="container">
					<div className="row">
						<div className="col-sm-6 col-md-6 col-lg-6">
							<h3 className="title-02">Start Your<span> Education Online</span></h3>
                            <blockquote className="margin-top-30 margin-bottom-15 margin-md-bottom-30">
								<p>Join millions of students and instructors in the world's largest online learning marketplace</p>
							</blockquote>
							<p className="padding-bottom-100">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>


						</div>
						<div className="col-sm-6 col-md-6 col-lg-6">
							<div className="nivoSlider about-slider">
								<img src="img/about-slider_03.jpg" alt='' />
							</div>
						</div>
					</div>
				</div>
			</div>


        <div className="content-box-02 padding-top-96 padding-bottom-55">
				<div className="container">
					<div className="row">
						<div className="col-sm-6 col-md-6 col-lg-6">
							<h3 className="process-title-01">Our
								<span>Mission</span>
							</h3>
							<p className="process-text-02">We offer many advantages to students, both international and domestic: a full curriculum taught by certified and knowledgeable teaching staff, on- and off-campus housing support and student meal plans, a specia- lized student services department
								that offers individualized assistance with applications.</p>
						</div>
						<div className="col-sm-6 col-md-6 col-lg-6">
							<h3 className="process-title-01">Our
								<span>Principles</span>
							</h3>
							<ul className="ul-list-02 process-list">
								<li>The Safe Educational Environment</li>
								<li>Fully-certified Teaching Staff</li>
								<li>Dedicated Student Services</li>
								<li>Comfortable Residence Living</li>
								<li>Perfect Programs for Studying</li>
							</ul>
						</div>

					</div>
				</div>
			</div>


            <div className="parallax parallax_03">
				<div className="container">
					<div className="row">
						<div className="col-lg-12">
							<div className="module-counter counter-02">
								<div className="shortcode_counter done">
									<div className="counter_wrapper">
										<div className="counter_content">
											<div className="stat_count_wrapper">
												<p className="stat_count" data-count="362">245</p>
												<p className="counter_title">Employee</p>
											</div>
											<div className="stat_temp" style={{width: "362px"}}></div>
										</div>
									</div>
								</div>
								<div className="shortcode_counter done">
									<div className="counter_wrapper">
										<div className="counter_content">
											<div className="stat_count_wrapper">
												<p className="stat_count" data-count="2458">4514</p>
												<p className="counter_title">Register Student</p>
											</div>
											<div className="stat_temp" style={{width: "2458px"}}></div>
										</div>
									</div>
								</div>
								<div className="shortcode_counter done">
									<div className="counter_wrapper">
										<div className="counter_content">
											<div className="stat_count_wrapper">
												<p className="stat_count" data-count="19">10</p>
												<p className="counter_title">Years of Experience</p>
											</div>
											<div className="stat_temp" style={{width: "19px"}}></div>
										</div>
									</div>
								</div>
								<div className="shortcode_counter done">
									<div className="counter_wrapper">
										<div className="counter_content">
											<div className="stat_count_wrapper">
												<p className="stat_count" data-count="35">425</p>
												<p className="counter_title">Courses</p>
											</div>
											<div className="stat_temp" style={{width: "35px"}}></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</main>

        <Footer />
      </div>

    );
  }
}

const mapStateToProps = (state) => {
  return {
    messages : state.messages,
    token    : state.auth.token,
    user     : state.auth.user,
    settings : state.settings,
    subjects : state.subjects
  };
};

export default connect(mapStateToProps)(About);
