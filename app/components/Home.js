import React from 'react';
import { connect } from 'react-redux'
import Messages from './Messages';

import { IndexLink, Link } from 'react-router';

import Header from './Header';
import Footer from './Footer';
import CourseBoxes from './Courses/CourseBoxes'
class Home extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      faqs: [],
      popular : []
    };
  }
  componentWillUnmount() {
    this.serverRequest && this.serverRequest.abort();
    this.serverRequest2 && this.serverRequest2.abort();
  }

  componentDidMount(){
    this.runQuery(this.props);
  }

  componentWillReceiveProps(newProps)
  {
    this.runQuery(newProps);
  }

  runQuery(prps){
    this.serverRequest = $.get('/faq/list', function (result) {
      // console.log(result);
      if (result.ok) {
        var obj = {
          faqs: result.faqs,
          // categories : this.getUniqueCategories(result.faqs)
        }
      } else {
        var obj = {
          faqs: [],
          // categories : [ ]
        }
      }
      this.setState(obj, () => {
        this.serverRequest2 = $.get('/api/courses/listPopular', function (result) {
        //  console.log(result);
          if (result.ok) {
            var obj = {
              popular: result.items,
            }
          } else {
            var obj = {
              popular: [],
            }
          }
          this.setState(obj);
        }.bind(this));

      });

      if ($('.accordion-01').size() > 0 || $('.module_toggle').size() > 0) {
    		$('.accordion-01__title').on('click', function() {
    			if (!$(this).hasClass('state-active')) {
    				$(this).parents('.accordion-01').find('.accordion-01__body').slideUp(300);
    				$(this).next().slideToggle(300);
    				$(this).parents('.accordion-01').find('.state-active').removeClass('state-active');
    				$(this).addClass('state-active');
    			}
    		});

    		$('.accordion-01__title.expanded_yes').each(function(index) {
    			$(this).next().slideDown(300);
    			$(this).addClass('state-active');
    		});
    	}

      if ($('.owl-option-02').size() > 0) {
        $('.owl-option-02').owlCarousel({ loop:true,margin:10,responsiveClass:true,dots: false,nav:true,responsive:{  0:{    items:2  },  360:{    items:3  },  480:{    items:4  },  767:{    items:5  }} });
      }



    }.bind(this));
  }



  render() {
    console.log(this.state);
    return (
      <div className="wrapp-content">
        <Header title="Thinkscademy" />
        <main className="content-row">
          <div className="content-box-02-gray">
            <div className="row">
              <div className="col-lg-12 text-center">
                <h3 className="title-02 title-02--mr-01">{this.props.settings.home_heading_1}
                </h3>
                <p className="subtitle-01">{this.props.settings.home_heading_2}</p>
              </div>
              <div className="col-lg-12 text-center">
                <div className="owl-carousel owl-option-02">
                {
                  this.props.subjects['0'].map((o, i)=>{
                    return (
                      <div className="item" key={i}>
                        <Link to={'/courses/'+o.title} className="owl-option-02__box-01">
                          <i className={"is_sub " + o.icon}></i>
                          <h3 className="owl-option-02__title">
                            {o.title}
                          </h3>
                        </Link>
                      </div>
                    )
                  })
                }

                </div>
              </div>
            </div>
          </div>
          <div className="content-box-01 padding-top-93">
            <div className="container">
              <div className="row">
                <div className="col-lg-12 text-center">
                  <h3 className="title-02 title-02--mr-01">{this.props.settings.home_course_heading_1}
                  </h3>
                  <p className="subtitle-01">{this.props.settings.home_course_heading_2}</p>
                </div>
              </div>
            </div>
          </div>
          <div className="content-box-01 padding-bottom-100">
            <div className="container">
              <div className="row">
                <div className="col-lg-12">
                  <CourseBoxes data={this.state.popular} />
                </div>
              </div>
              <div className="row">
                <div className="col-lg-12 text-center">
                  <Link to="/courses" className="btn-01">See more</Link>
                </div>
              </div>
            </div>
          </div>
          <div className="parallax parallax_01" data-type="background" data-speed="10">
            <div className="container">
              <div className="row">
                <div className="col-lg-12">
                  <div className="parallax-content-01">
                    <h3 className="parallax-title">Trusted by Over
                      <span>2M</span> Students
                    </h3>
                    <div className="parallax-text">
                      <p>A loving community of purposeful learning that focuses on the Competitor's Body, Mind and Soul </p>
                    </div>
                    <a href="#" className="parallax-btn">get started</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="content-box-02 content-box-02--ak">
            <div className="table-02">
              <div className="table-02__row">
                <div className="table-02__box-01">
                  <img className="table-02__img" src="img/img_02.jpg" alt="" />
                </div>
                <div className="table-02__box-02">
                  <div className="accordion-wrapp">
                    <h3 className="title-03">{this.props.settings.home_faq_heading}
                    </h3>
                    <div className="accordion-01">
                    {
                      this.state.faqs.map((faq, i)=>{
                        return (
                          <div key={i}>

                          <h6 data-count="1" className={"accordion-01__title "+((i == 0) ? "expanded_yes" : '')}>
                            {faq.title}
                          </h6>
                          <div className="accordion-01__body">
                            <div className="accordion-01__text">
                              <p>  {faq.content}</p>
                            </div>
                          </div>
                          </div>
                        )
                      })
                    }
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
        <Footer />
      </div>

    );
  }
}

const mapStateToProps = (state) => {
  return {
    messages : state.messages,
    token    : state.auth.token,
    user     : state.auth.user,
    settings : state.settings,
    subjects : state.subjects
  };
};

export default connect(mapStateToProps)(Home);
