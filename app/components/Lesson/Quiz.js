import React from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router';
import Header from '../Header';
import Footer from '../Footer';

class Quiz extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      is_loaded: false,
      item: { course_details: {}, lesson_usage_details: {} },
      current_question: 0,
      correctCount: 0,
      resultOut  : false

    };

    this.questions = [{ Q: '', A: '', C: [] }];
    this.opts = 'abcdefghijklmnopqrstuvwxyz'.split('')
  }

  componentWillUnmount() {
    this.serverRequest && this.serverRequest.abort();
  }

  componentDidMount() {
    this.runQuery(this.props);
  }

  componentWillReceiveProps(newProps) {
    this.runQuery(newProps);
  }

  runQuery(prps) {
    this.serverRequest = $.get('/api/lessons/single/' + prps.params.id+'?withRelated=yes', function (result) {
      if (result.ok) {
        // console.log(result)
        var obj = {
          item: result.item,
          is_loaded: true,
          // questions:
        }
        this.questions = result.item.quiz_data;

      } else {
        var obj = {

          item: { course_details: {}},
          is_loaded: true,
          lesson_usage_details: {},
          is_not_allowed : result.is_not_allowed ? true : false
        }
        this.questions = [{ Q: '', A: '', C: [] }];
      }
      this.setState(obj);
    }.bind(this));
  }

  optionClick(e, choice)
  {
    this.setState({ ch: choice });

    let parent = $(e.target).parent();
    if (parent.find(".ink").length == 0)
      parent.prepend("<span class='ink'></span>");

    let ink = parent.find(".ink");
    ink.removeClass("animate");

    if (!ink.height() && !ink.width()) {
      let d = Math.max(parent.outerWidth(), parent.outerHeight());
      ink.css({ height: "100px", width: "100px" });
    }

    let x = e.pageX - parent.offset().left - ink.width() / 2;
    let y = e.pageY - parent.offset().top - ink.height() / 2;
    let current_question = this.state.current_question;
    ink.css({ top: y + 'px', left: x + 'px' }).addClass("animate");
    this.questions[current_question].UC = choice;
    if (parseInt(choice) == parseInt(this.questions[current_question]['A']) ) {
      this.questions[this.state.current_question].result = "Correct";
      this.setState({ correctCount: this.state.correctCount + 1});
    } else {
      this.questions[this.state.current_question].result = "Incorrect";


    }
    setTimeout(() => {
      if (this.state.current_question + 1 == this.questions.length) {
        this.setState({ resultOut: true });
        this.markComplete();

      } else {
        this.setState({ current_question: this.state.current_question + 1 });
      }
      this.setState({ ch: -1});
    }, 1000);
    $('input:radio').prop('checked', false);
  }

  markComplete() {
    let answers = [];

    let correct_count = 0;
    for (let i = 0; i < this.questions.length; i++)
    {
      let tmp = {};
      tmp.s = this.questions[i].UC;
      tmp.r = this.questions[i].result;
      if (tmp.r) correct_count++;
      answers.push(tmp);
    }
    // if (correct_count == this.questions.length)
    // {
      this.doProgress(answers);
    // }
  }

  doProgress(answers) {
    let completed_on = new Date();
    // let lesson_order = this.state.item.order;
    let lesson_id = this.state.item.id;
    // let course = this.state.item.id;

    fetch('/giveProgress', {
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        lesson_id: lesson_id,
        answers: answers
      })
    })
    .then((response) => {
      return response.json().then((json) => {
        if (response.ok) {
        //  console.log(json)
          this.props.dispatch({
            type: 'UPDATE_PROGRESS',
            progress: json.progress
          });
        }
      });
    });
  }

  doLeft(e){
    e.preventDefault();
    if( this.state.current_question  > 0)
      this.setState({ current_question: this.state.current_question - 1 });
  }

  doQuiz() {
    var options = ["1", '2', "3", '4'];
    return (
      <div className="row">
        <div className="col-sm-8 col-sm-offset-2">
          <div>
            <div id="quiz">
              <div className="question">
                <h3><span className="label label-warning" id="qid">{this.state.current_question + 1}</span>
                  <span id="question">{this.questions[this.state.current_question].Q}</span>
                </h3>
              </div>
              <ul className="options_lst">
                {
                  options.map((option, i) => {
                    return (
                      <li key={i} className="is_single_opp">
                        <input onChange={(e) => { this.optionClick(e, option) }} type="radio" id={"f-option" + i} name="selector" value={option} checked={this.state.ch == option} />
                        <label htmlFor={"f-option" + i} className="element-animation">
                          <span className="label label-warning" id="qid">{this.opts[i]}</span> {this.questions[this.state.current_question].C[option]}
                        </label>
                        <div className="check"></div>
                      </li>
                    )
                  })
                }

              </ul>
            </div>
            <div className="row">
            <div className="col-md-12">
              {
                (this.state.current_question) ? (
                  <button className="btn" onClick={(e)=>this.doLeft(e)} type="button"><i className="fas fa-caret-square-left"></i> Previous Question</button>
                ):false
              }
            </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  doResult() {
    return (
      <div className="row">
        <div className="col-sm-8 col-sm-offset-2">
          <div id="result-of-question" className="pulse animated">
            <span id="totalCorrect" className="pull-right">Total Correct: {this.state.correctCount}</span>
            <table className="table table-hover table-responsive" >
              <thead>
                <tr>
                  <th>Question No.</th>
                  <th>Our answer</th>
                  <th>Your answer</th>
                  <th>Result</th>
                </tr>
              </thead>
              <tbody id="quizResult">
                {
                  this.questions.map((q, i) => {
                    return (
                      <tr key={i}>
                        <td>{i + 1}</td>
                        <td>{q.C[""+q.A]}</td>
                        <td>{q.C[""+q.UC]}</td>
                        <td>{q.result}</td>
                      </tr>
                    )
                  })
                }</tbody>
            </table>
          </div>
        </div>
        <div className="col-sm-12 text-center">
          <Link to={"/course/" + this.state.item.course_details.id + '/' + ((typeof convertToSlug !== "undefined") ? convertToSlug(this.state.item.course_details.title) : 'course')} className="btn-01"> <i className="fas fa-arrow-left"></i> Back to course</Link>
        </div>
      </div>
    )

  }

  render() {
    let item = this.state.item;
    return (
      <div className="wrapp-content">
        <Header padding_less={true} />
        <main className="content-row">
          <div className="content-box-01 padding-top-40 padding-bottom-40">
            <div className="container quiz">
              {
                this.state.resultOut ? this.doResult() : this.doQuiz()
              }
            </div>
          </div>
        </main>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    user: state.auth.user,
    subjects: state.subjects
  };
};

export default connect(mapStateToProps)(Quiz);
