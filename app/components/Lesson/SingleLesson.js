import React from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router';
import Header from '../Header';
import Footer from '../Footer';

class SingleLesson extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      is_loaded: false,
      item: { course_details: {}, lesson_usage_details: {}}
    };
  }


  endsWith(str, search){
    const this_len = str.length
    return str.substring(this_len - search.length, this_len) === search;
  }

  onlyName(item)
  {
    let a = item.substring(item.lastIndexOf('/')+1);
    return a;
  }

  componentWillUnmount() {
    this.serverRequest && this.serverRequest.abort();
  }

  componentDidMount() {
    this.runQuery(this.props);
  }

  componentWillReceiveProps(newProps) {
    this.runQuery(newProps);
  }

  runQuery(prps) {
    this.serverRequest = $.get('/api/lessons/single/' + prps.params.id+'?withRelated=yes', function (result) {
      if (result.ok) {
        var obj = {
          item: result.item,
          is_loaded: true,
        }

      } else {
        var obj = {
          item: { course_details: {}},
          is_loaded: true,
          lesson_usage_details: {},
          is_not_allowed : result.is_not_allowed ? true : false
        }
      }
      this.setState(obj, ()=>{
        setTimeout(()=>{
          $("a.is_fancyBox").fancybox({
        		'hideOnContentClick': true
        	});

        }, 1000);
      });
    }.bind(this));
  }


  render() {
    let item = this.state.item;
    return (
      <div className="wrapp-content">
        <Header padding_less={true} />
        <main className="content-row">

          <div className="content-box-01 padding-top-40 padding-bottom-40">
            <div className="container">


                  <div className="row row_lines">

                    <div className="col-md-3">
                      <h3>Attachements</h3>
                        <div className="video_thumnail">
                          {
                              (item.course_details.image_url && item.course_details.image_url!='') ?
                              (<img src={'/downloads/'+item.course_details.image_url} className="img-responsive" />) :
                              (<img src="/img/imgs/placeholder.png" className="img-responsive" />)
                          }
                        </div>

                        <div className="attachments_list">  <ul>
                          {
                            item.attachment_1 && JSON.parse(item.attachment_1).map((item,i)=>{
                              if(this.endsWith(item, 'pdf')){
                                   return (
                                    <li key={i} style={{marginBottom:'10px'}}>
                                      <a className="is_fancyBox" href={'/downloads/'+item} iframe>
                                        <i className="fa fa-file"></i> {this.onlyName(item)}
                                      </a>
                                    </li>);
                              }
                              return (
                                <li style={{marginBottom:'10px'}}>
                                  <a className="is_fancyBox" href={'/downloads/'+item}>
                                    <i className="fa fa-file"></i> {this.onlyName(item)}
                                  </a>
                                </li>);
                            })
                          }
                          </ul>
                        </div>


                      </div>

                    <div className="col-md-9">
                      <div className="video_details">
                        <div className="vidoe_title">
                          <h2>
                           Lesson {item.order} of {item.course_details.title}  - {item.title}
                          </h2>
                        </div>
                          {
                            (item.course_details.author) ? (
                              <div className="author_video">
                                By <Link className="authorName" to={"/course_detailss/author/" + item.course_details.author_id}>{item.course_details.author.first_name} {item.course_details.author.last_name}</Link>
                              </div>
                            ):false
                          }



                        <div className="video_text">
                        <p dangerouslySetInnerHTML={{ __html: item.description }}></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-12 text-center">
                      <div className="duration_details">
                        <Link to={"/quiz/" + item.id + '/' + (( typeof convertToSlug !== "undefined") ? convertToSlug(item.title) : 'quiz' )} className="btn-01">Proceed to Quiz <i className="fas fa-arrow-right"></i> </Link>
                      </div>
                    </div>
                  </div>
            </div>
          </div>

        </main>

        <Footer />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    user: state.auth.user,
    subjects: state.subjects
  };
};

export default connect(mapStateToProps)(SingleLesson);
