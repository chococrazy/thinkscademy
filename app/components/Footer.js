import React from 'react';
import {Link }     from 'react-router'
import { connect } from 'react-redux'
class Footer extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      pages : []
    };
  }
  componentWillUnmount() {
    this.serverRequest.abort();
  }

  componentDidMount(){
    this.runQuery(this.props);
  }

  runQuery(prps){
    this.serverRequest = $.get('/api/pages/footer_links/', function (result) {
      if(result.ok){
        var obj = {
          pages : result.pages,
        }
      }else{
        var obj = {
          pages : []
        }
      }
      this.setState(obj);
    }.bind(this));
  }

  render() {
    return (
      <footer className="wrapp-footer">
      <div className="footer-box-01">
        <div className="container">
          <div className="row">
            <div className="col-sm-3 col-md-3 col-lg-3">
              <Link to="/" className="footer-logo">
                <img src="/img/logo_white.png" alt="" />
              </Link>
              <ul className="widget-contact">
                <li>
                  <h4 className="widget-contact__title">Location:</h4>
                  <p className="widget-contact__text">{this.props.settings.address}</p>
                </li>
                <li>
                  <h4 className="widget-contact__title">Phone:</h4>
                  <p className="widget-contact__text">{this.props.settings.phone}</p>
                </li>
                <li>
                  <h4 className="widget-contact__title">Email:</h4>
                  <p className="widget-contact__text">
                    <Link className="widget-contact__text-email" to={"mailto:"+this.props.settings.email}>{this.props.settings.email}</Link>
                  </p>
                </li>
              </ul>
            </div>
            <div className="col-sm-3 col-md-3 col-lg-3">
              <div className="widget-link">
                <h3 className="widget-title">Explore</h3>
                <ul className="widget-list">
                  {
                    this.state.pages.map((p,i)=>{
                      return <li key={i}><Link to={"/p/"+p.slug}>{p.title}</Link></li>
                    })
                  }
                </ul>
              </div>
            </div>
            <div className="col-sm-3 col-md-3 col-lg-3">
              <div className="widget-link">
                <h3 className="widget-title">Courses</h3>
                <ul className="widget-list">
                {
                  this.props.subjects['0'].map((o, i)=>{
                    return (

                      <li  key={i}>
                        <Link to={'/courses/'+o.title}>{o.title}</Link>
                      </li>
                    )
                  })
                }

                </ul>
              </div>
            </div>
            <div className="col-sm-3 col-md-3 col-lg-3">
              <div className="widget-link">
                <h3 className="widget-title">Follow Us on </h3>
                <ul className="social-list-01">
                {
                  (this.props.settings.facebook_url) ? (
                    <li>
                      <Link target="_blank" to={this.props.settings.facebook_url}>
                        <i className="fab fa-facebook" aria-hidden="true"></i>
                      </Link>
                    </li>
                  ) : false
                }
                {
                  (this.props.settings.twitter_url) ? (
                    <li>
                      <Link target="_blank" to={this.props.settings.twitter_url}>
                        <i className="fab fa-twitter" aria-hidden="true"></i>
                      </Link>
                    </li>
                  ) : false
                }
                {
                  (this.props.settings.instagram_url) ? (
                    <li>
                      <Link target="_blank" to={this.props.settings.instagram_url}>
                        <i className="fab fa-instagram" aria-hidden="true"></i>
                      </Link>
                    </li>
                  ) : false
                }
                {
                  (this.props.settings.google_plus_url) ? (
                    <li>
                      <Link target="_blank" to={this.props.settings.google_plus_url}>
                        <i className="fab fa-google-plus" aria-hidden="true"></i>
                      </Link>
                    </li>
                  ) : false
                }
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="footer-box-02">
        <div className="container">
          <div className="row">
            <div className="col-sm-12 col-md-12 col-lg-12 text-center">
              <p className="copy-info">&copy; 2018 Onlice Courses. All Rights Reserved</p>
            </div>
          </div>
        </div>
      </div>
      <a href="#" className="back2top" title="Back to Top">Back to Top</a>
      </footer>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    messages : state.messages,
    settings : state.settings,
    subjects : state.subjects,
  };
};

export default connect(mapStateToProps)(Footer);
