import React from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router';

import Header from '../Header';
import Footer from '../Footer';
import Sidebar from './Sidebar';
var moment = require('moment')
class QuizResults extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      is_loaded: false,
      total_items: 0,
      total_pages: 0,
      current_page: 1,
      items: []
    };
  }

  componentWillUnmount() {
    this.serverRequest && this.serverRequest.abort();
  }

  componentDidMount() {
    this.runQuery(this.props);
  }

  componentWillReceiveProps(newProps) {
    this.runQuery(newProps);
  }




  qryStr = [];

  runQuery(prps) {

    var qry = [];
    this.qryStr = [];

    if (prps.params.paged) {
      qry.push('paged=' + prps.params.paged)
      this.qryStr.push(prps.params.paged);
    }

    qry = qry.join('&');

    this.serverRequest = $.get('/api/lessons/single_results/'+this.props.params.lesson_id+'', function (result) {
      if (result.ok) {
        // console.log(result.items);
        var obj = {
          items: result.items,
          is_loaded: true,
        }

      } else {
        var obj = {
          items: [],
          is_loaded: true,
          total_items: 0,
          total_pages: 0,
          current_page: 1,
        }
      }
      this.setState(obj);
    }.bind(this));

  }


  render() {
    return (
      <div className="wrapp-content">
        <Header title="Teacher Dashboard - My Courses - Lessons" />
        <main className="content-row">
          <div className="content-box-01 padding-top-93">
            <div className="container">
              <div className="row">
                    <Sidebar />
                    <div className="col-lg-9">
                       <table className="table" >
                        <thead>
                          <tr>
                            <th>Completed On</th>
                            <th>Completed By</th>
                            <th>Correct Answers</th>
                            <th>Percentage</th>
                          </tr>
                        </thead>
                        <tbody>
                          {
                            this.state.items.map((item, i) => {
                              return (
                                <tr key={item.id}>

                                  <td>{moment(item.created_at).format('llll')}</td>
                                  <td>**** {item.user.last_name} </td>
                                  <td>{item.correct_answers}</td>
                                  <td>{
                                    Math.round((item.correct_answers*100/13) * 100) / 100
                                  }%</td>

                                </tr>
                              )
                            })
                          }
                        </tbody>
                      </table>

                </div>
              </div>
            </div>
          </div>
        </main>
        <Footer />
      </div>


    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    user: state.auth.user,
    messages: state.messages
  };
};

export default connect(mapStateToProps)(QuizResults);
