import React from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router';

import Header from '../Header';
import Footer from '../Footer';
import Sidebar from './Sidebar';

class LessonList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      is_loaded: false,
      total_items: 0,
      total_pages: 0,
      current_page: 1,
      items: []
    };
  }

  componentWillUnmount() {
    this.serverRequest && this.serverRequest.abort();
  }

  componentDidMount() {
    this.runQuery(this.props);
  }

  componentWillReceiveProps(newProps) {
    this.runQuery(newProps);
  }



  doDelete(e, id){
    e.preventDefault();
    var a = confirm('Are you sure you want to delete this lesson?');
    if(a)
    {
      this.serverRequest = $.post('/api/lessons/delete',{id:id}, function (result) {
        this.runQuery(this.props);
      }.bind(this));

    }

  }



  qryStr = [];

  runQuery(prps) {

    var qry = [];
    this.qryStr = [];

    if (prps.params.paged) {
      qry.push('paged=' + prps.params.paged)
      this.qryStr.push(prps.params.paged);
    }

    qry = qry.join('&');
    this.serverRequest = $.get('/api/lessons/list/' + this.props.params.course_id, function (result) {
      if (result.ok) {
        var obj = {
          items: result.items,
          is_loaded: true,
        }

      } else {
        var obj = {
          items: [],
          is_loaded: true,
          total_items: 0,
          total_pages: 0,
          current_page: 1,
        }
      }
      this.setState(obj);
    }.bind(this));

  }


  render() {
    return (
      <div className="wrapp-content">
        <Header title="Teacher Dashboard - My Courses - Lessons" />
        <main className="content-row">
          <div className="content-box-01 padding-top-93">
            <div className="container">
              <div className="row">
                    <Sidebar />
                    <div className="col-lg-9">
                    <Link to={"/dashboard/course/lessons/"+this.props.params.course_id+"/add"} className="btn btn-primary" >Add New Lesson</Link><br /><br />
                      <table className="table" >
                        <thead>
                          <tr>
                            <th>Lesson No</th>
                            <th>Lesson Title</th>
                            <th>Quiz Completed Count</th>
                            <th>Quiz Submit Count</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          {
                            this.state.items.map((item, i) => {
                              return (
                                <tr key={item.id}>
                                  <td>#{item.order}</td>
                                  <td>{item.title}</td>
                                  <td>{item.complete_count}</td>
                                  <td>
                                  {item.start_count} (<a href={"/dashboard/course/lessons/"+this.props.params.course_id+'/quiz/'+item.id+'/results'}>show</a>)
                                  </td>
                                  {/* <td>{item.views}</td> */}
                                  {/* <td>{item.lessons.length}</td> */}
                                  <td>

                                    <Link title="edit" className="btn" to={'/dashboard/course/lessons/'+this.props.params.course_id+'/edit/' + item.id}><i className="fas fa-pencil-alt"></i></Link> <Link title='delete' className="btn" onClick={(e)=>this.doDelete(e, item.id)}><i className="fas fa-trash-alt"></i></Link>
                                  </td>
                                </tr>
                              )
                            })
                          }
                        </tbody>
                      </table>

                </div>
              </div>
            </div>
          </div>
        </main>
        <Footer />
      </div>


    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    user: state.auth.user,
    messages: state.messages
  };
};

export default connect(mapStateToProps)(LessonList);
