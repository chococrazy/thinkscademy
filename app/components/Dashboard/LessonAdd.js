import React from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router';
import { addLesson  } from '../../actions/TeacherPanel';
import Header from '../Header';
import Messages from '../Messages';
import Footer from '../Footer';
import Sidebar from './Sidebar';

class AddLesson extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      is_loaded: false,
      course_id : this.props.params.course_id,
      quiz_data : [{
        Q:'',C:{1:'',2:'',3:'',4:''},A:''},{Q:'',C:{1:'',2:'',3:'',4:''},A:''},{Q:'',C:{1:'',2:'',3:'',4:''},A:''},{  Q:'',C:{1:'',2:'',3:'',4:''},A:''},{Q:'',C:{1:'',2:'',3:'',4:''},A:''},{Q:'',C:{1:'',2:'',3:'',4:''},A:''},{  Q:'',C:{1:'',2:'',3:'',4:''},A:''},{Q:'',C:{1:'',2:'',3:'',4:''},A:''},{Q:'',C:{1:'',2:'',3:'',4:''},A:''},{  Q:'',C:{1:'',2:'',3:'',4:''},A:''},{Q:'',C:{1:'',2:'',3:'',4:''},A:''},{Q:'',C:{1:'',2:'',3:'',4:''},A:''},{  Q:'',C:{1:'',2:'',3:'',4:''},A:''}]
    };
  }


  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleChangeQuiz(event, q,i, k)
  {
    let quiz_data = this.state.quiz_data;
    if(q == 'C'){
      quiz_data[i]['C'][k] = event.target.value;
    }
    else{
      quiz_data[i][q] = event.target.value;
    }
    this.setState({quiz_data : quiz_data});
  }

  handleSaveCallback(){
    jQuery('html, body').animate({scrollTop: 200});
    this.setState({
      title:  '',
      description:  '',
      order:'',
      quiz_data : [{
        Q:'',C:{1:'',2:'',3:'',4:''},A:''},{Q:'',C:{1:'',2:'',3:'',4:''},A:''},{Q:'',C:{1:'',2:'',3:'',4:''},A:''},{  Q:'',C:{1:'',2:'',3:'',4:''},A:''},{Q:'',C:{1:'',2:'',3:'',4:''},A:''},{Q:'',C:{1:'',2:'',3:'',4:''},A:''},{  Q:'',C:{1:'',2:'',3:'',4:''},A:''},{Q:'',C:{1:'',2:'',3:'',4:''},A:''},{Q:'',C:{1:'',2:'',3:'',4:''},A:''},{  Q:'',C:{1:'',2:'',3:'',4:''},A:''},{Q:'',C:{1:'',2:'',3:'',4:''},A:''},{Q:'',C:{1:'',2:'',3:'',4:''},A:''},{  Q:'',C:{1:'',2:'',3:'',4:''},A:''}]
    })
  }

  handleLessonCreate(event) {
    event.preventDefault();
    this.props.dispatch(addLesson('add',this.state, this.props.token, this.handleSaveCallback.bind(this) ));
  }

  componentWillUnmount() {
    this.serverRequest && this.serverRequest.abort();
  }

  componentDidMount() {
    // this.runQuery(this.props);
    // $(document).ready(function() {
      $('#summernote').summernote({
        callbacks: {
          onChange: function(contents, $editable) {
            var event = new Event('input', { bubbles: true });
            jQuery('#summernote')[0].dispatchEvent(event);
            setTimeout(function(){
              var event = new Event('input', { bubbles: true });
              jQuery('#summernote')[0].dispatchEvent(event);
            }, 200);
          }
        }
      });
      setTimeout(function(){
        $('i.note-icon-question').parent().remove();
      },100)
    // });
  }
  //
  // componentWillReceiveProps(newProps) {
  //   this.runQuery(newProps);
  // }
  //
  // qryStr = [];
  //
  // runQuery(prps) {
  //
  //   var qry = [];
  //   this.qryStr = [];
  //
  //   if (prps.params.paged) {
  //     qry.push('paged=' + prps.params.paged)
  //     this.qryStr.push(prps.params.paged);
  //   }
  //
  //   qry = qry.join('&');
  //   this.serverRequest = $.get('/api/lessons/list/' + this.props.params.course_id, function (result) {
  //     if (result.ok) {
  //       var obj = {
  //         items: result.items,
  //         is_loaded: true,
  //       }
  //
  //     } else {
  //       var obj = {
  //         items: [],
  //         is_loaded: true,
  //         total_items: 0,
  //         total_pages: 0,
  //         current_page: 1,
  //       }
  //     }
  //     this.setState(obj);
  //   }.bind(this));
  //
  // }
  quiz_lst = ['','','','','','','','','','','','','']

  render() {
    return (
      <div className="wrapp-content">
        <Header title="Teacher Dashboard - My Courses - Add Lesson" />
        <main className="content-row">
          <div className="content-box-01 padding-top-93">
            <div className="container">
              <div className="row">
                <Sidebar />
                <div className="col-lg-9">


              <form onSubmit={this.handleLessonCreate.bind(this)}>

              <Messages messages={this.props.messages}/>



                      <div className="form-group">
                          <label>Lesson Title</label>
                          <input type="text" value={this.state.title} onChange={this.handleChange.bind(this)}  name="title" placeholder="Enter Lesson Title" className="form-control" />
                      </div>


                      <div className="form-group">
                          <label>Lesson Order / Number</label>
                          <input type="number" value={this.state.order} onChange={this.handleChange.bind(this)}  name="order" placeholder="Enter Lesson Order / Number" className="form-control" />
                      </div>

                  <div className="form-group">
                      <label>Lesson Content</label>
                      <textarea id="summernote" value={this.state.description} onChange={this.handleChange.bind(this)}  name="description"></textarea>



                  </div>

                  <hr />
                  <h3>Quiz Data</h3>


                  <table className="table">
                      <thead>
                          <tr>
                              <th>#</th>
                              <th>Question</th>
                              <th>Choice 1</th>
                              <th>Choice 2</th>
                              <th>Choice 3</th>
                              <th>Choice 4</th>
                              <th style={{width: '107px'}}>Answer</th>
                          </tr>
                      </thead>
                      <tbody>
                      {
                        this.quiz_lst.map( (l,i)=>{
                          return (
                          <tr key={i}>
                              <td>
                                {(i+1)}
                              </td><td>
                                  <textarea onChange={(e)=>this.handleChangeQuiz(e, 'Q', i)} required   value={this.state.quiz_data[i].Q}  className="form-control" ></textarea>
                              </td>
                              <td>
                                  <input  onChange={(e)=>this.handleChangeQuiz(e, 'C',i,1)} type="text" className='form-control' value={this.state.quiz_data[i].C[1]} />
                              </td>
                              <td>
                                  <input  onChange={(e)=>this.handleChangeQuiz(e, 'C', i,2)} type="text" className='form-control' value={this.state.quiz_data[i].C[2]} />
                              </td>
                              <td>
                                  <input  onChange={(e)=>this.handleChangeQuiz(e, 'C',i, 3)} type="text" className='form-control' value={this.state.quiz_data[i].C[3]} />
                              </td>
                              <td>
                                  <input  onChange={(e)=>this.handleChangeQuiz(e, 'C',i, 4)} type="text" className='form-control' value={this.state.quiz_data[i].C[4]} />
                              </td>
                              <td>
                                  <select onChange={(e)=>this.handleChangeQuiz(e, 'A', i)} value={this.state.quiz_data[i].A} required className="form-control"  value={this.state.quiz_data[i].A} >
                                      <option value="">Select</option>
                                      <option value="1">Choice 1</option>
                                      <option value="2">Choice 2</option>
                                      <option value="3">Choice 3</option>
                                      <option value="4">Choice 4</option>
                                  </select>
                              </td>
                          </tr>
                        )
                        })
                      }

                      </tbody>
                  </table>

                  <br />



                      <div className="card-footer ">
                          <button type="submit" className="btn btn-fill btn-info">Submit</button>
                      </div>

                  </form>
                  <br />
                  <br />
                  <br />
                  <br />
<br />


                </div>
              </div>
            </div>
          </div>
        </main>
        <Footer />
      </div>


    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    user: state.auth.user,
    messages: state.messages
  };
};

export default connect(mapStateToProps)(AddLesson);
