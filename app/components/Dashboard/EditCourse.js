import React from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router';
import { addCourse  } from '../../actions/TeacherPanel';
import Header from '../Header';
import Footer from '../Footer';
import Messages from '../Messages';
import Sidebar from './Sidebar';

class EditCourse extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      is_loaded: false,
      // learnings : [],
      // requirements: []
    };
  }


    componentWillUnmount() {
      this.serverRequest && this.serverRequest.abort();
    }

    componentDidMount() {
      this.runQuery(this.props);
    }

    componentWillReceiveProps(newProps) {
      this.runQuery(newProps);
    }


    runQuery(prps) {

      this.serverRequest = $.get('/api/courses/single/'+this.props.params.id, function (result) {
        if (result.ok && result.item.author_id == this.props.user.id) {
          // console.log(result);
          var obj = {
            id : result.item.id,
            title:  result.item.title,
            subject:  result.item.subject,
            topic:  result.item.topic,
            description:  result.item.description,
            learnings:  result.item.learnings.join("\n"),
            requirements:  result.item.requirements.join("\n"),
          }
          this.setState(obj);
        }

      }.bind(this));

    }




  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleCourseEdit(event) {
    event.preventDefault();
    this.props.dispatch(addCourse('edit',this.state, this.props.token, null ));
    jQuery('html, body').animate({scrollTop: 200});
  }

  render() {
    return (
      <div className="wrapp-content">
        <Header title="Teacher Dashboard - My Courses - Edit Course" />
        <main className="content-row">
          <div className="content-box-01 padding-top-93">
            <div className="container">
              <div className="row">
                <Sidebar />
                <div className="col-lg-9">

                  <form onSubmit={this.handleCourseEdit.bind(this)}>

                  <Messages messages={this.props.messages}/>


                    <div className="form-group">
                      <label>Course Title</label>
                      <input type="text" value={this.state.title} onChange={this.handleChange.bind(this)}  name="title" placeholder="Enter Course Title" className="form-control" />
                    </div>


                    <div className="form-group">
                      <label>Course Subject</label>
                      <input type="text" name="subject" value={this.state.subject} onChange={this.handleChange.bind(this)} placeholder="Enter Course Subject" className="form-control" auto-complete="autoCompleteOptions" />
                      <span className="loading" ng-show="loading"></span>
                    </div>



                    <div className="form-group">
                      <label>Course Topic</label>
                      <input type="text" value={this.state.topic} onChange={this.handleChange.bind(this)} name="topic"  placeholder="Enter Course Topic" className="form-control" disabled={!this.state.subject} auto-complete="autoCompleteOptionsTopic" />
                      <span className="loading" ng-show="loading"></span>
                    </div>




                    <div className="form-group">
                      <label>Course Description</label>
                      <textarea type="text" value={this.state.description} onChange={this.handleChange.bind(this)} name="description" placeholder="Enter Course Description" className="form-control">
                      </textarea>
                    </div>


                    <div className="form-group">
                        <label>Course Learnings (1 per line)</label>
                        <textarea value={this.state.learnings } onChange={this.handleChange.bind(this)}  type="text" name="learnings" placeholder="Enter Course Learnings" className="form-control">
                        </textarea>
                    </div>


                    <div className="form-group">
                        <label>Course Requirements (1 per line)</label>
                        <textarea value={this.state.requirements } onChange={this.handleChange.bind(this)} type="text" name="requirements" placeholder="Enter Course Learnings" className="form-control">
                        </textarea>
                    </div>


                    <button type="submit" className="btn btn-fill btn-info">Submit</button>
                  </form>
                  <br />
                  <br />
                  <br />
                  <br />


                </div>
              </div>
            </div>
          </div>
        </main>
        <Footer />
      </div>


    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    user: state.auth.user,
    messages: state.messages
  };
};

export default connect(mapStateToProps)(EditCourse);
