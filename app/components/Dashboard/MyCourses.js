import React from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router';

import ReactPaginate from 'react-paginate';
import { forgotPassword } from '../../actions/auth';
import Messages from '../Messages';
import Header from '../Header';
import Footer from '../Footer';
import Sidebar from './Sidebar';

class MyCourses extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      is_loaded: false,
      total_items: 0,
      total_pages: 0,
      current_page: 1,
      items: []
    };
  }

  componentWillUnmount() {
    this.serverRequest && this.serverRequest.abort();
  }

  componentDidMount() {
    this.runQuery(this.props);
  }

  componentWillReceiveProps(newProps) {
    this.runQuery(newProps);
  }

  doDelete(e, id){
    e.preventDefault();
    var a = confirm('Are you sure you want to delete this course?');
    if(a)
    {
      this.serverRequest = $.post('/api/courses/delete',{id:id}, function (result) {
        this.runQuery(this.props);
      }.bind(this));

    }

  }

  qryStr = [];

  runQuery(prps) {

    var qry = [];
    this.qryStr = [];

    if (prps.params.paged) {
      qry.push('paged=' + prps.params.paged)
      this.qryStr.push(prps.params.paged);
    }

    qry = qry.join('&');
    this.serverRequest = $.get('/api/courses/my?' + qry, function (result) {
      if (result.ok) {
        var obj = {
          items: result.items,
          is_loaded: true,
          total_items: result.pagination.rowCount,
          total_pages: result.pagination.pageCount,
          current_page: result.pagination.page
        }

      } else {
        var obj = {
          items: [],
          is_loaded: true,
          total_items: 0,
          total_pages: 0,
          current_page: 1,
        }
      }
      this.setState(obj);
    }.bind(this));

  }


  render() {
    return (
      <div className="wrapp-content">
        <Header title="Teacher Dashboard - My Courses" />
        <main className="content-row">
          <div className="content-box-01 padding-top-93">
            <div className="container">
                  <div className="row">
                    <Sidebar />
                    <div className="col-lg-9">
                      <table className="table" >
                        <thead>
                          <tr>
                            <th>Course ID</th>
                            <th>Course Name</th>
                            <th>Views</th>
                            <th>Lesson Count</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          {
                            this.state.items.map((item, i)=> {
                              return (
                                <tr key={item.id}>
                                  <td>#{item.id}</td>
                                  <td>{item.title}</td>
                                  <td>{item.views}</td>
                                  <td>
                                    <Link to={'/dashboard/course/lessons/' + item.id} className="btn">View Lessons ({item.lessons.length})</Link>
                                  </td>
                                  <td>
                                    <Link title="edit" className="btn" to={'/dashboard/course/edit/' + item.id}><i className="fas fa-pencil-alt"></i></Link> <Link title='delete' className="btn" onClick={(e)=>this.doDelete(e, item.id)}><i className="fas fa-trash-alt"></i></Link>
                                  </td>
                                </tr>
                              )
                            })
                          }
                        </tbody>
                      </table>
                      <div className="row">
                        <div className="col-lg-12">
                          <div className="courses-pagination">
                            {this.state.is_loaded && this.state.items.length ? (
                              <ReactPaginate previousLabel={"<"}
                                nextLabel={">"}
                                breakLabel={"..."}
                                breakClassName={"break-me"}
                                initialPage={this.state.current_page - 1}
                                disableInitialCallback={true}
                                pageCount={this.state.total_pages}
                                marginPagesDisplayed={2}
                                pageRangeDisplayed={5}
                                onPageChange={(i) => { this.handlePageClick(i) }}
                                containerClassName={"pagination-list"}
                                subContainerClassName={"pages pagination"}
                                activeClassName={"active"} />
                            ) : false}
                          </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
        <Footer />
      </div>


    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    user: state.auth.user,
    messages: state.messages
  };
};

export default connect(mapStateToProps)(MyCourses);
