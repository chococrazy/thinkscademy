import React from 'react';
import { Link } from 'react-router';

class Sidebar extends React.Component {
  render() {
    return (
      <div className="col-lg-3">
        <ul className="list-group">
          <li className={"list-group-item " + ((typeof location !=='undefined' &&location.pathname =='/dashboard') ? ' active ' : ' ' )}>
            <a href="/dashboard">My Courses</a>
          </li>
            <li className={"list-group-item " + ((typeof location !=='undefined' &&location.pathname =='/dashboard/course/add') ? ' active ' : ' ' )}>
              <a href="/dashboard/course/add">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Add New</a>
            </li>
          <li className="list-group-item">
            <a href="/account">Profile</a>
          </li>
        </ul>
      </div>
    );
  }
}

// const mapStateToProps = (state) => {
//   return {
//     token: state.auth.token,
//     user: state.auth.user,
//     messages: state.messages
//   };
// };

export default Sidebar;
