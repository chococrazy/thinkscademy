import React       from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router'
import Messages    from './Messages';

class PlanSingle extends React.Component {
  render() {


    var class_heading = (this.props.isLast) ? '' : 'dull'
    var class_box = (this.props.isLast) ? 'plan_features_ulatra' : 'plan_other_f'

    // console.log("inner", this.props.user.plan)
    var single = this.props.plan.description;

    if(single && single != '')
    {
      single = single.split("\n");
    }else{
      single = [];
    }
    if(this.props.user && this.props.user.id && this.props.user.plan == 'paid')
    {
      // var pl = this.props.user.plan_description;
      // if(!pl)
      //   pl ='{}'
      //   // console.log(pl)
      // // pl = JSON.stringify(pl);
      // // pl = JSON.parse(pl);
      // pl = JSON.parse(pl);
      if (this.props.user.plan_name == this.props.plan.id)
      {
        var planBtn = (<span key={this.props.plan.id} >
          <button className="buy_button dull" >on this plan</button>
        </span>);
      } else {
        let ttl = (this.props.user.max_levels_allowed < this.props.plan.max_level_count) ? 'Upgrade' : 'Downgrade'
        var planBtn = (<button key={this.props.plan.id} className="buy_button" onClick={(e) => { this.props.doPlanChange(this.props.plan.id, this.props.plan.title, e) }} >{ttl}</button>);
      }
    }else{
      var planBtn = (
        <button key={this.props.plan.id}  onClick={ ()=>{this.props.doPlan(this.props.plan.id, this.props.plan.title)}} className={"buy_button "+class_heading} >Buy Now</button>
      )
    }

    return (
      <div className="col-md-3">
        <div className={"plan_heading "+class_heading}>{this.props.plan.title}</div>
        <div className={class_box}>${this.props.plan.cost}</div>
        <div className={class_box}>{this.props.plan.duration}</div>
        <div className={class_box}>{this.props.plan.max_level_count}</div>
        {
          (this.props.user && this.props.user.id) ?  [planBtn] :
          (
            <Link to={"/signup/"+this.props.plan.id} className={"buy_button "+class_heading} >Buy Now</Link>
          )
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    messages : state.messages,
    // token    : state.auth.token,
    user     : state.auth.user,
  };
};

export default connect(mapStateToProps)(PlanSingle);
